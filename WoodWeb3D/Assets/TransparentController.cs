﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransparentController : MonoBehaviour {

    public float transValue = 1;


	// Use this for initialization
	void Start () {
        Renderer[] renderers = GetComponentsInChildren<Renderer>();
        foreach (var r in renderers)
        {
            // Do something with the renderer here...
            //r.enabled = false; // like disable it for example. 
        }
    }
	
	// Update is called once per frame
	void Update () {
        Renderer[] renderers = GetComponentsInChildren<Renderer>();
        foreach (var r in renderers)
        {
            // Do something with the renderer here...
            foreach (Material matt in r.materials)
            {
                matt.color = new Color(matt.color.r, matt.color.g, matt.color.b, transValue);
            }
        }
    }
}
