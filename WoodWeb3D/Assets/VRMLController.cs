﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.IO;
using System;
using System.Text.RegularExpressions;
using System.Text;
using UnityEngine.Networking;

public class VRMLController : MonoBehaviour {

    private string vrmlURL = "file:///G:/Drafting/3DS Max/VR/WebBuilds/Geometry/Bathy/HeidelbergBathy.wrl";

    public GameObject grid, gridPrefab, dotPrefab;

    public Vector3 offset;

    private int xDim, zDim, xIndex, zIndex;
    private float xSpacing, zSpacing;

    private List<float> heightPoints = new List<float>();

    private List<Vector3> terrainPoints = new List<Vector3>();

    private bool isDoingHeight = false;

    private Vector3 parentPosition = Vector3.zero;
 
    // Use this for initialization
    void Start () {
        StartCoroutine(DownloadGeometry());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator DownloadGeometry()
    {
        using (UnityWebRequest www2 = UnityWebRequest.Get(vrmlURL))
        {
            www2.timeout = 0;
            /*
            www.SetRequestHeader("Access-Control-Allow-Credentials", "true");
            www.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
            www.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
            www.SetRequestHeader("Access-Control-Allow-Origin", "*");
            */
            yield return www2.SendWebRequest();
            if (www2.isNetworkError || www2.isHttpError)
            {
                Debug.LogError(www2.error);
            }
            else
            {
                string input = www2.downloadHandler.text;

                string[] stringSeparators1 = new string[] { "Transform" };
                var TransformResult = input.Split(stringSeparators1, StringSplitOptions.None);

                int tIndex = 0;

                foreach(string tran in TransformResult)
                {
                    Debug.Log(tran);
                    if(tIndex > 0)
                    {
                        string[] stringSeparators = new string[] { "Shape" };
                        var result = tran.Split(stringSeparators, StringSplitOptions.None);

                        int x = 0;
                        foreach (string s in result)
                        {
                            if (x == 1)
                            {
                                GameObject newGrid = Instantiate(gridPrefab, parentPosition, Quaternion.identity);
                                xIndex = 0;
                                zIndex = 0;
                                heightPoints.Clear();
                                using (StringReader reader = new StringReader(s))
                                {
                                    string line = string.Empty;
                                    do
                                    {
                                        line = reader.ReadLine();
                                        if (line != null)
                                        {
                                            line = line.Trim();
                                            string[] split = line.Split(' ');
                                            if (split[0].Equals("xDimension"))
                                            {
                                                int t = 0;
                                                foreach (string v in split)
                                                {
                                                    if (t > 0)
                                                    {
                                                        if (!v.Equals(""))
                                                        {
                                                            xDim = int.Parse(v);
                                                        }
                                                    }
                                                    t++;
                                                }
                                            }

                                            if (split[0].Equals("zDimension"))
                                            {
                                                int t = 0;
                                                foreach (string v in split)
                                                {
                                                    if (t > 0)
                                                    {
                                                        if (!v.Equals(""))
                                                        {
                                                            zDim = int.Parse(v);
                                                        }
                                                    }
                                                    t++;
                                                }
                                            }

                                            if (split[0].Equals("xSpacing"))
                                            {
                                                int t = 0;
                                                foreach (string v in split)
                                                {
                                                    if (t > 0)
                                                    {
                                                        if (!v.Equals(""))
                                                        {
                                                            xSpacing = float.Parse(v);
                                                        }
                                                    }
                                                    t++;
                                                }
                                            }

                                            if (split[0].Equals("zSpacing"))
                                            {
                                                int t = 0;
                                                foreach (string v in split)
                                                {
                                                    if (t > 0)
                                                    {
                                                        if (!v.Equals(""))
                                                        {
                                                            zSpacing = float.Parse(v);
                                                        }
                                                    }
                                                    t++;
                                                }
                                            }

                                            if (isDoingHeight)
                                            {
                                                if (split[0].Equals("]"))
                                                {
                                                    isDoingHeight = false;
                                                }
                                                else if (split[0].Equals("["))
                                                {

                                                }
                                                else
                                                {

                                                    foreach (string hValue in split)
                                                    {
                                                        if (!hValue.Equals(""))
                                                        {
                                                            string newValue = hValue.Replace(",", "");
                                                            float xValue = ((float)xIndex) * xSpacing;
                                                            float zValue = ((float)zIndex) * zSpacing;

                                                            Vector3 point = new Vector3(xValue * 0.3048f, float.Parse(newValue) * 0.3048f, zValue * 0.3048f);
                                                            //GameObject g = Instantiate(dotPrefab, point, Quaternion.identity);
                                                            //g.GetComponent<MeshRenderer>().material.color = new Color((float)xIndex/8.0f, 0, 0);
                                                            terrainPoints.Add(point);
                                                            xIndex++;
                                                        }

                                                        if (xIndex >= xDim)
                                                        {
                                                            xIndex = 0;
                                                            zIndex++;
                                                        }

                                                    }

                                                }
                                            }

                                            if (split[0].Equals("height"))
                                            {
                                                isDoingHeight = true;
                                            }
                                        }

                                    } while (line != null);
                                }

                                newGrid.GetComponent<Grid>().terrainVerts = terrainPoints.ToArray();
                                newGrid.GetComponent<Grid>().xSize = xDim;
                                newGrid.GetComponent<Grid>().ySize = zDim;
                                newGrid.GetComponent<Grid>().Generate();
                                terrainPoints.Clear();
                            }
                            else if (x==0)
                            {
                                using (StringReader reader = new StringReader(tran))
                                {
                                    string line = string.Empty;
                                    do
                                    {
                                        line = reader.ReadLine();
                                        if (line != null)
                                        {
                                            line = line.Trim();
                                            string[] split = line.Split(' ');

                                            if (split[0].Equals("translation"))
                                            {
                                                parentPosition.x = float.Parse(split[1]) - offset.x;
                                                parentPosition.y = float.Parse(split[2]) - offset.y;
                                                parentPosition.z = float.Parse(split[3]) - offset.z;
                                            }
                                        }
                                    } while (line != null);
                                }
                            }

                            terrainPoints.Clear();
                            x++;
                        }
                    }
                    
                    tIndex++;
                }

                

                //Debug.Log("xDimension = " + xDim + ". zDimension = " + zDim + ". xSpacing = " + xSpacing + ". zSpacing = " + zSpacing);
            }
        }
    }
}
