﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThirdPersonController : MonoBehaviour {

    //ROV moveSpeed
    float normalMoveSpeed = 2;
    float rotationX;
    Vector3 velocity = new Vector3(0, 0, 0);
    float angularVelocity = 0;
    
    //ROV camera rotation
    float camX;
    float camY;
    
    //toggle between free and pilot mode
    bool freeMotion = false;

    //Reference To PlayerController script, mainly beacuse we want to use the same Camera sensitivity value that script has.
    PlayerController myPlayerScript;

    //Reference to the camera of the ROV
    GameObject myCam;

    //The List of all the different Camera Angles the ROV has.
    //Create as many LOCAL transforms around the ROV to use as camera angles, and put them in this List to cycle through
    public List<Transform> views;

    // index for views List
    int viewIndex = 0, animIndex = 0;

    bool isDpadDown = false, isDpadClicked = false;

    private GameObject Player_MK_UI, IndicatorMenu; 
    private Text IndicatorText;

    Transform lPropellerFront, lPropellerBack, rPropellerFront, rPropellerBack, backPropellerLeft, backPropellerRight;
    float lpfVelocity, lpbVelocity, rpfVelocity, rpbVelocity, bplVelocity, bprVelocity;

	// Use this for initialization
	void Start () {

        // Find our UI
        Player_MK_UI = GameObject.Find("Player_MK_UI");

        IndicatorMenu = Player_MK_UI.transform.Find("IndicatorMenu").gameObject;
        IndicatorText = IndicatorMenu.transform.Find("IndicatorText").GetComponent<Text>();
        IndicatorMenu.SetActive(true);

        myPlayerScript = GameObject.Find("Player_MK").GetComponent<PlayerController>();
        myCam = transform.Find("3rdPersonCam").gameObject;
        camX = myCam.transform.eulerAngles.x;
        camY = myCam.transform.eulerAngles.y;
        rotationX = transform.eulerAngles.y;
        lPropellerFront = transform.Find("ROV 2 Animations/Millenium frame04/Impeller31/Propeller29");
        lPropellerBack = transform.Find("ROV 2 Animations/Millenium frame04/Impeller32/Propeller30");
        rPropellerFront = transform.Find("ROV 2 Animations/Millenium frame04/Impeller33/Propeller33");
        rPropellerBack = transform.Find("ROV 2 Animations/Millenium frame04/Impeller31/Propeller31");
        backPropellerLeft = transform.Find("ROV 2 Animations/Millenium fram04/Impeller35/Propeller35");
        backPropellerRight = transform.Find("ROV 2 Animations/Millenium fram04/Impeller34/Propeller34");
    }
	
	// Update is called once per frame
	void Update () {
        //UpdateThirdPersonMode();
	}

    private void OnCollisionEnter(Collision collision)
    {
        velocity = velocity * -0.4f;
        angularVelocity = angularVelocity * -0.4f;
    }

    public void UpdateThirdPersonMode()
    {
        // forwardDir (forward Direction) of the ROV. Normalize without Y-axis because we only want it move in horizontal axis. Used for movement below
        Vector3 forwardDir = transform.forward;
        forwardDir.y = 0;
        forwardDir.Normalize();

        //rightDir (right Direction) of the ROV. Used for movement below
        Vector3 rightDir = transform.right;
        rightDir.y = 0;
        rightDir.Normalize();

        if (Input.GetKeyDown(KeyCode.K)) { freeMotion = !freeMotion; }
        if (!freeMotion)
        {
            //Realistic ROV movement with collider
            IndicatorText.text = "Pilot Mode";

            //camera fixed on ROV camera, can be rotated with mouse to look around
            camX += Input.GetAxis("Mouse X") * 15 * Time.deltaTime + Input.GetAxis("Xbox_RS_Horizontal") * 15 * Time.deltaTime;
            camY += Input.GetAxis("Mouse Y") * 15 * Time.deltaTime + Input.GetAxis("Xbox_RS_Vertical") * 15 * Time.deltaTime;
            camX = Mathf.Clamp(camX, -60, 60);
            camY = Mathf.Clamp(camY, -50, 50);
            myCam.transform.localRotation = Quaternion.AngleAxis(camX, Vector3.up);
            myCam.transform.localRotation *= Quaternion.AngleAxis(-camY, Vector3.right);

            //Movement of ROV
            //Flat acceleration is defined and velocity and position are updated as a function of time and acceleration/velocity respectively
            Vector3 acceleration = new Vector3(0, 0, 0);
            float angularAcceleration = 0;
            float propellerAcceleration = 2;

            //WASD to move forward, backward, strafe
            //QE to rotate in place, RF to go up or down
            acceleration = forwardDir * 0.5f * Input.GetAxis("Vertical") + rightDir * 0.5f * Input.GetAxis("Horizontal");
            //bplVelocity = propellerAcceleration * Input.GetAxis("Vertical") * Time.deltaTime - bplVelocity * 0.15f;
            //bprVelocity = propellerAcceleration * Input.GetAxis("Vertical") * Time.deltaTime - bprVelocity * 0.15f;
            //lpfVelocity =
            //lpbVelocity =  
            //rpfVelocity = 
            //rpbVelocity = 
            if (Input.GetKey(KeyCode.Q)) {angularAcceleration = -4; }
            if (Input.GetKey(KeyCode.E)) {angularAcceleration = 4;}
            if (Input.GetKey(KeyCode.R)) { acceleration += Vector3.up * 0.5f; }
            if (Input.GetKey(KeyCode.F)) { acceleration += Vector3.up * -0.5f; }
            acceleration = acceleration.normalized;
            velocity += acceleration * Time.deltaTime - velocity * 0.025f;
            angularVelocity += angularAcceleration * Time.deltaTime - angularVelocity * 0.015f;
            transform.position += velocity * Time.deltaTime;
            rotationX += angularVelocity * Time.deltaTime;
            transform.rotation = Quaternion.AngleAxis(rotationX, Vector3.up);
        }
        else
        {
            //Free motion ROV for setting, collider disabled
            IndicatorText.text = "Free Mode";
            myCam.transform.localRotation = Quaternion.identity;

            // update rotationX. Calculates 'Mouse X' every frame and adds to rotation of transform. Uses 'Xbox Right Stick' horizontal axis.
            // ROV mode cannot rotate up or down, so removed 'Mouse Y' and 'Xbox Right Stick' vertical axis.
            rotationX += Input.GetAxis("Mouse X") * myPlayerScript.cameraSensitivity * Time.deltaTime + Input.GetAxis("Xbox_RS_Horizontal") * myPlayerScript.cameraSensitivity * Time.deltaTime;
            transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);

            // Movement of ROV
            // if holding 'Shift' or 'Xbox Right Trigger' use the fastMoveFactor from the playerScript to move faster
            // Xbox trigger deadzone is .2
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift) || (Input.GetAxis("Xbox_RT") > .2f))
            {
                transform.position += forwardDir * (normalMoveSpeed * myPlayerScript.fastMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
                transform.position += rightDir * (normalMoveSpeed * myPlayerScript.fastMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
                transform.position += forwardDir * (normalMoveSpeed * myPlayerScript.fastMoveFactor) * Input.GetAxis("Xbox_LS_Vertical") * Time.deltaTime;
                transform.position += rightDir * (normalMoveSpeed * myPlayerScript.fastMoveFactor) * Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime;

                // use 'Q','E' or 'Dpad Up', 'Dpad Down' to move up and down
                transform.position += transform.up * (normalMoveSpeed * myPlayerScript.fastMoveFactor) * .5f * Input.GetAxis("Xbox_Dpad_Vertical") * Time.deltaTime;

                // use 'Q','E' or 'Dpad Up', 'Dpad Down' to move up and down
                if (Input.GetKey(KeyCode.Q)) { transform.position += transform.up * normalMoveSpeed * myPlayerScript.fastMoveFactor * Time.deltaTime; }
                if (Input.GetKey(KeyCode.E)) { transform.position -= transform.up * normalMoveSpeed * myPlayerScript.fastMoveFactor * Time.deltaTime; }
            }
            // if holding 'Ctrl' or 'Xbox Left Trigger' use the slowMoveFactor from the playerScript to move slower
            // Xbox trigger deadzone is .2
            else if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl) || (Input.GetAxis("Xbox_LT") > .2f))
            {
                transform.position += forwardDir * (normalMoveSpeed * myPlayerScript.slowMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
                transform.position += rightDir * (normalMoveSpeed * myPlayerScript.slowMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
                transform.position += forwardDir * (normalMoveSpeed * myPlayerScript.slowMoveFactor) * Input.GetAxis("Xbox_LS_Vertical") * Time.deltaTime;
                transform.position += rightDir * (normalMoveSpeed * myPlayerScript.slowMoveFactor) * Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime;

                // use 'Q','E' or 'Dpad Up', 'Dpad Down' to move up and down
                transform.position += transform.up * (normalMoveSpeed * myPlayerScript.slowMoveFactor) * .5f * Input.GetAxis("Xbox_Dpad_Vertical") * Time.deltaTime;

                // use 'Q','E' or 'Dpad Up', 'Dpad Down' to move up and down
                if (Input.GetKey(KeyCode.Q)) { transform.position += transform.up * myPlayerScript.slowMoveFactor * normalMoveSpeed * Time.deltaTime; }
                if (Input.GetKey(KeyCode.E)) { transform.position -= transform.up * myPlayerScript.slowMoveFactor * normalMoveSpeed * Time.deltaTime; }
            }
            //move with normal speed factor
            else
            {
                transform.position += forwardDir * normalMoveSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
                transform.position += rightDir * normalMoveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
                transform.position += forwardDir * normalMoveSpeed * Input.GetAxis("Xbox_LS_Vertical") * Time.deltaTime;
                transform.position += rightDir * normalMoveSpeed * Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime;

                // use 'Q','E' or 'Dpad Up', 'Dpad Down' to move up and down
                transform.position += transform.up * normalMoveSpeed * .5f * Input.GetAxis("Xbox_Dpad_Vertical") * Time.deltaTime;

                // use 'Q','E' or 'Dpad Up', 'Dpad Down' to move up and down
                if (Input.GetKey(KeyCode.Q)) { transform.position += transform.up * normalMoveSpeed * Time.deltaTime; }
                if (Input.GetKey(KeyCode.E)) { transform.position -= transform.up * normalMoveSpeed * Time.deltaTime; }
            }
        }


        //Press 'Escape' or 'Xbox button B' to exit Third Person Mode (ROV Mode)
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown("joystick button 1") || Input.GetMouseButtonDown(1))
        {
            //myPlayerScript.ToggleThirdPerson();
        }

        //Press 'Tab' or 'Xbox Button A, X' to cycle through ROV camera positions/angles
        // views is a List of Transforms. viewIndex is the index counter for views.
        if (Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown("joystick button 2") || Input.GetKeyDown("joystick button 0"))
        {
            viewIndex++;
            if(viewIndex >= views.Count)
            {
                viewIndex = 0;
            }

            myCam.transform.position = views[viewIndex].position;
            myCam.transform.rotation = views[viewIndex].rotation;
        }

        updateControls();
    }

    void updateControls()
    {
        #region Animation

        int targetIndex = -1;
        if (Input.GetKeyDown(KeyCode.Alpha1)) { targetIndex = 0; }
        else if (Input.GetKeyDown(KeyCode.Alpha2)) { targetIndex = 1; }
        else if (Input.GetKeyDown(KeyCode.Alpha3)) { targetIndex = 2; }
        else if (Input.GetKeyDown(KeyCode.Alpha4)) { targetIndex = 3; }
        else if (Input.GetKeyDown(KeyCode.Alpha5)) { targetIndex = 4; }

        if (targetIndex >= 0)
        {
            if (GetComponentInChildren<Animation>() != null)
            {
                Animation anim = GetComponentInChildren<Animation>();
                int c = 0;
                foreach (AnimationState state in anim)
                {
                    if (c == targetIndex)
                    {
                        if (anim.IsPlaying(state.clip.name))
                        {
                            if (state.speed > 0)
                            {
                                state.speed = 0;
                            }
                            else
                            {
                                state.speed = 1;
                            }

                        }
                        else
                        {
                            anim.Play(state.clip.name);
                        }
                    }
                    c++;
                }
            }
        }

        #endregion

        #region Animation For Xbox

        if (Input.GetAxis("Xbox_Dpad_Horizontal") != 0)
        {
            if (!isDpadDown)
            {
                isDpadClicked = true;
            }
            else
            {
                isDpadClicked = false;
            }
            isDpadDown = true;
        }
        else
        {
            isDpadDown = false;
            isDpadClicked = false;
        }

        if (isDpadClicked && GetComponentInChildren<Animation>() != null)
        {
            if (Input.GetAxis("Xbox_Dpad_Horizontal") > 0)
            {
                animIndex++;
                Animation anim = GetComponentInChildren<Animation>();
                int c = 0;
                foreach (AnimationState state in anim)
                {
                    c++;
                }

                if (animIndex > (c - 1))
                {
                    animIndex = 0;
                }

                c = 0;
                foreach (AnimationState state in anim)
                {
                    if (c == animIndex)
                    {
                        if (anim.IsPlaying(state.clip.name))
                        {
                            if (state.speed > 0)
                            {
                                state.speed = 0;
                            }
                            else
                            {
                                state.speed = 1;
                            }

                        }
                        else
                        {
                            anim.Play(state.clip.name);
                        }
                    }
                    c++;
                }
            }
            else
            {
                if (GetComponentInChildren<Animation>() != null)
                {
                    Animation anim = GetComponentInChildren<Animation>();
                    int c = 0;
                    foreach (AnimationState state in anim)
                    {
                        if (c == animIndex)
                        {
                            if (anim.IsPlaying(state.clip.name))
                            {
                                if (state.speed > 0)
                                {
                                    state.speed = 0;
                                }
                                else
                                {
                                    state.speed = 1;
                                }

                            }
                            else
                            {
                                anim.Play(state.clip.name);
                            }
                        }
                        c++;
                    }
                }
            }
        }

        #endregion
    }
}
