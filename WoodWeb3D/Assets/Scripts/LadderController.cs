﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderController : MonoBehaviour {

    private GameObject playerObject;

    void Start()
    {
        playerObject = GameObject.Find("Player_MK");
    }

    void OnTriggerEnter(Collider coll)
    {
        if (playerObject != null)
        {
            if (coll.gameObject == playerObject)
            {
                if (playerObject.GetComponent<PlayerController>().ladderTimeout > .5f)
                {
                    //Debug.Log(playerObject.GetComponent<PlayerController>().myCam.transform.forward.normalized.x + transform.up.normalized.x);

                    if (Mathf.Abs(playerObject.GetComponent<PlayerController>().myCam.transform.forward.normalized.x + transform.up.normalized.x) < .5f)
                    {
                        if (Input.GetAxis("Vertical") > .15f)
                        {
                            playerObject.GetComponent<PlayerController>().canClimb = true;
                            playerObject.GetComponent<Rigidbody>().useGravity = false;
                        }
                    }
                }
            }
        }
    }

    void OnTriggerExit(Collider coll2)
    {
        if (playerObject != null)
        {
            if (coll2.gameObject == playerObject)
            {
                playerObject.GetComponent<PlayerController>().canClimb = false;
                playerObject.GetComponent<Rigidbody>().useGravity = true;
            }
        }
    }

}
