﻿/*
 * ****************************************************************
 * MasterController is an object designed to control the scene. The script is usually placed in a GameObject named 'Master'
 * It keeps track of if the game is online, using VPLink, the player's name, or any other values that might be static throughout the scene's lifetime.
 * It controls saving and loading
 * It loads data from .text files that can be used to place information on objects
 * It controls the multiplayer side of things (connecting to Photon NameServer, joining lobbies and rooms)
 ********************************************************************
*/


using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VR;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Formatters.Binary;

public class MasterController : MonoBehaviour
{
    public bool isVPLink, isUnderwaterScene, isGIS;

    // Reference to the Virtual Reality Player GameObject
    public GameObject Player_VR;
    // Reference to the Mouse and Keyboard Player GameObject
    public GameObject Player_MK;
    // Reference to the Mouse and Keyboard Player's UI GameObject
    public GameObject Player_MK_UI;

    // Reference to the Mouse and Keyboard Player's UI GameObject
    //public GameObject DesktopAnimPlayButton;

    // Reference to the Mouse and Keyboard Player's UI GameObject
    //public GameObject DesktopAnimPauseButton;

    // Reference to the Mouse and Keyboard Player's UI GameObject
    //public GameObject VRAnimPlayButton;

    // Reference to the Mouse and Keyboard Player's UI GameObject
    //public GameObject VRAnimPauseButton;

    //public Scrollbar desktopAnimationScrollbar;

    //public VREasy.VRSlider VRAnimationScrollbar;

    // Reference to the Mouse and Keyboard Player's UI GameObject
    //public GameObject[] ROV_Collider_Objects;

    // Reference to the Ultimate(World) Master Controller
    //[HideInInspector]
    //public WorldMasterController worldMasterScript;

    // Reference to Player_VR VR_Tablet_Script
    //[HideInInspector]
    //public VR_Tablet_Script tabletScript;

    // static values inherited from WorldMasterController
    //[HideInInspector]
    //public bool isVR = false, isOnline = false;

    // The Player's name to display if the session is Online.
    //[HideInInspector]
    //public string playerName = "Player";

    // Keeps track of it this player made his own room. If the player made a room, he is the host!
    //private bool isHost = false;

    // List<GameObject> allAssets keep a list of every GameObject with a <VREasy.VRGrabbable> script attatched to it.
    [HideInInspector]
    public List<GameObject> allAssets = new List<GameObject>();

    // List<GameObject> saveObjects is a List of <SaveObject> used to save every GameObject's (with a <VREasy.VRGrabbable> script attatched to it) position and rotation.
    //[HideInInspector]
    //public List<SaveObject> saveObjects = new List<SaveObject>();

    //float disableTimer = 0;
    //bool hasDisabled = false;

    //[HideInInspector]
    //public GameObject myOnlineRightController;

    //[HideInInspector]
    //public GameObject myOnlineDesktopAvatar;


    //public ListedAnimationController selectedAnimationController;
    //public ListedAnimatorController selectedAnimatorController;

    /*
    public struct OnlinePlayer
    {
        public int playerID;
        public string playerName;
    }
    */

    /*
    public struct PlayerAction
    {
        public int type; //0-move, 1-rotate, 2-delete, 3-duplicate
        public GameObject changedObject;
        public GameObject deletedObjPrefab;
        public GameObject duplicatedObject;
        public Quaternion prevRotation;
        public Vector3 prevPosition;
    }
    */

    //[HideInInspector]
    //public List<OnlinePlayer> onlinePlayerList = new List<OnlinePlayer>();

    //[HideInInspector]
    //public Stack<PlayerAction> prevPlayerActions = new Stack<PlayerAction>();

    //[HideInInspector]
    //public PlayerAction currentPlayerAction;

    private void Awake()
    {
        //PhotonNetwork.automaticallySyncScene = true;
    }

    // Use this for initialization
    void Start()
    {

        #region File Reading
        /*

        #region Jumper Data

        TextAsset allLines = Resources.Load("K2_Data") as TextAsset;
        string fs = allLines.text;
        string[] fLines = Regex.Split(fs, "\\r?\\n");
        string[] descriptors = fLines[0].Split('\t');

        int count = 0;
        foreach (string line in fLines)
        {
            if (count != 0)
            {
                string[] fields = line.Split('\t');
                //Debug.Log("count = " + count + ". " + fields[0]);
                if (GameObject.Find(fields[0]))
                {
                    GameObject obj = GameObject.Find(fields[0]);
                    //VREasy.VRGrabbable objScript = obj.GetComponent<VREasy.VRGrabbable>();
                    if (objScript != null)
                    {
                        //objScript.infoValues.Clear();
                        int c = 0;
                        allAssets.Add(obj);
                        foreach (string str in fields)
                        {
                            if (!str.Equals(""))
                            {
                                objScript.infoValues.Add(descriptors[c] + " : " + str);
                            }
                            //Debug.Log(descriptors[c] + " : " + str);
                            c++;
                        }
                    }
                }
            }

            count++;
        }
        #endregion

        #region Manifold Data

        allLines = Resources.Load("Manifold") as TextAsset;
        fs = allLines.text;
        fLines = Regex.Split(fs, "\\r?\\n");
        descriptors = fLines[0].Split('\t');

        count = 0;
        foreach (string line in fLines)
        {
            if (count != 0)
            {
                string[] fields = line.Split('\t');
                //Debug.Log("count = " + count + ". " + fields[0]);
                if (GameObject.Find(fields[0]))
                {
                    GameObject obj = GameObject.Find(fields[0]);
                    VREasy.VRGrabbable objScript = obj.GetComponent<VREasy.VRGrabbable>();
                    if (objScript != null)
                    {
                        //objScript.infoValues.Clear();
                        int c = 0;
                        allAssets.Add(obj);
                        foreach (string str in fields)
                        {
                            if (!str.Equals(""))
                            {
                                objScript.infoValues.Add(descriptors[c] + " : " + str);
                            }
                            //Debug.Log(descriptors[c] + " : " + str);
                            c++;
                        }
                    }
                }
            }

            count++;
        }
        #endregion

        #region Plem_Plet Data

        allLines = Resources.Load("PLEM & PLET") as TextAsset;
        fs = allLines.text;
        fLines = Regex.Split(fs, "\\r?\\n");
        descriptors = fLines[0].Split('\t');

        count = 0;
        foreach (string line in fLines)
        {
            if (count != 0)
            {
                string[] fields = line.Split('\t');
                //Debug.Log("count = " + count + ". " + fields[0]);
                if (GameObject.Find(fields[0]))
                {
                    GameObject obj = GameObject.Find(fields[0]);
                    VREasy.VRGrabbable objScript = obj.GetComponent<VREasy.VRGrabbable>();
                    if (objScript != null)
                    {
                        //objScript.infoValues.Clear();
                        int c = 0;
                        allAssets.Add(obj);
                        foreach (string str in fields)
                        {
                            if (!str.Equals(""))
                            {
                                objScript.infoValues.Add(descriptors[c] + " : " + str);
                            }
                            //Debug.Log(descriptors[c] + " : " + str);
                            c++;
                        }
                    }
                }
            }

            count++;
        }
        #endregion

        #region XT Data

        allLines = Resources.Load("XT") as TextAsset;
        fs = allLines.text;
        fLines = Regex.Split(fs, "\\r?\\n");
        descriptors = fLines[0].Split('\t');

        count = 0;
        foreach (string line in fLines)
        {
            if (count != 0)
            {
                string[] fields = line.Split('\t');
                //Debug.Log("count = " + count + ". " + fields[0]);
                if (GameObject.Find(fields[0]))
                {
                    GameObject obj = GameObject.Find(fields[0]);
                    VREasy.VRGrabbable objScript = obj.GetComponent<VREasy.VRGrabbable>();
                    if (objScript != null)
                    {
                        //objScript.infoValues.Clear();
                        int c = 0;
                        allAssets.Add(obj);
                        foreach (string str in fields)
                        {
                            if (!str.Equals(""))
                            {
                                objScript.infoValues.Add(descriptors[c] + " : " + str);
                            }
                            //Debug.Log(descriptors[c] + " : " + str);
                            c++;
                        }
                    }
                }
            }

            count++;
        }
        #endregion

        #region Other Data

        allLines = Resources.Load("Other") as TextAsset;
        fs = allLines.text;
        fLines = Regex.Split(fs, "\\r?\\n");
        descriptors = fLines[0].Split('\t');

        count = 0;
        foreach (string line in fLines)
        {
            if (count != 0)
            {
                string[] fields = line.Split('\t');
                //Debug.Log("count = " + count + ". " + fields[0]);
                if (GameObject.Find(fields[0]))
                {
                    GameObject obj = GameObject.Find(fields[0]);
                    VREasy.VRGrabbable objScript = obj.GetComponent<VREasy.VRGrabbable>();
                    if (objScript != null)
                    {
                        //objScript.infoValues.Clear();
                        int c = 0;
                        allAssets.Add(obj);
                        foreach (string str in fields)
                        {
                            objScript.infoValues.Add(descriptors[c] + " : " + str);
                            //Debug.Log(descriptors[c] + " : " + str);
                            c++;
                        }
                    }
                }
            }

            count++;
        }
        #endregion

        #region ROV Data

        allLines = Resources.Load("ROV_Clear") as TextAsset;
        fs = allLines.text;
        fLines = Regex.Split(fs, "\\r?\\n");
        descriptors = fLines[0].Split('\t');

        count = 0;
        foreach (string line in fLines)
        {
            if (count != 0)
            {
                string[] fields = line.Split('\t');
                //Debug.Log("count = " + count + ". " + fields[0]);
                if (GameObject.Find(fields[0]))
                {
                    GameObject obj = GameObject.Find(fields[0]);
                    VREasy.VRGrabbable objScript = obj.GetComponent<VREasy.VRGrabbable>();
                    if (objScript != null)
                    {
                        //objScript.infoValues.Clear();
                        int c = 0;
                        allAssets.Add(obj);
                        foreach (string str in fields)
                        {
                            objScript.infoValues.Add(descriptors[c] + " : " + str);
                            //Debug.Log(descriptors[c] + " : " + str);
                            c++;
                        }
                    }
                }
            }

            count++;
        }
        #endregion
        */

        #endregion

        refreshAssetsLists();

        /*
        GameObject.Find("HypotenuseText").GetComponent<MakeBillboard>().cam = Player_MK.transform.Find("Player_Camera").GetComponent<Camera>();
        GameObject.Find("HorizontalText").GetComponent<MakeBillboard>().cam = Player_MK.transform.Find("Player_Camera").GetComponent<Camera>();
        GameObject.Find("VerticalText").GetComponent<MakeBillboard>().cam = Player_MK.transform.Find("Player_Camera").GetComponent<Camera>();
        GameObject.Find("Angle0").GetComponent<MakeBillboard>().cam = Player_MK.transform.Find("Player_Camera").GetComponent<Camera>();
        GameObject.Find("Angle1").GetComponent<MakeBillboard>().cam = Player_MK.transform.Find("Player_Camera").GetComponent<Camera>();
        GameObject.Find("Angle2").GetComponent<MakeBillboard>().cam = Player_MK.transform.Find("Player_Camera").GetComponent<Camera>();
        */
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void reloadVPLink()
    {
        /*
        //Get all Objects of type GameObject and store in local GameObject array "allObjects"
        GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();

        //iterate through newly created allObjects array
        foreach (GameObject go in allObjects)
        {
            // if the object has a VREasy.VRGrabbable> script attached
            if (go.GetComponent<VPLinkEquipment>() != null)
            {
                go.GetComponent<VPLinkEquipment>().reloadConfig();
                if (go.GetComponent<VREasy.VRGrabbable>())
                {
                    go.GetComponent<VREasy.VRGrabbable>().reloadVPLink();
                }
            }
            else if (go.GetComponent<VPLinkTextValue>() != null)
            {
                go.GetComponent<VPLinkTextValue>().reloadConfig();
            }
            else if (go.GetComponent<VPScaler>() != null)
            {
                go.GetComponent<VPScaler>().reloadConfig();
            }
            else if (go.GetComponent<VPButton>() != null)
            {
                go.GetComponent<VPButton>().reloadConfig();
            }
            else if (go.GetComponent<VPColorChanger>() != null)
            {
                go.GetComponent<VPColorChanger>().reloadConfig();
            }
            else if (go.GetComponent<VPLinkRotator>() != null)
            {
                go.GetComponent<VPLinkRotator>().reloadConfig();
            }
            else if (go.GetComponent<VPLight>() != null)
            {
                go.GetComponent<VPLight>().reloadConfig();
            }
        }
        */
    }

    public void refreshAssetsLists()
    {
        #region Setup Desktop Asset List Box

        if (GameObject.Find("AssetListBox"))
        {
            GameObject panel = GameObject.Find("AssetListBox").transform.Find("ScrollRect").transform.Find("Panel").gameObject;

            foreach (GameObject asset in allAssets)
            {
                GameObject newText = Instantiate(Resources.Load("AssetText", typeof(GameObject))) as GameObject;
                newText.GetComponent<ListedAssetController>().myAsset = asset;
                //newText.GetComponent<ListedAssetController>().myVRScript = Player_VR.transform.Find("[CameraRig]").Find("Controller (left)").Find("TabletParent").GetComponent<VR_Tablet_Script>();
                newText.GetComponent<ListedAssetController>().myPlayerScript = Player_MK.GetComponent<PlayerController>();
                newText.GetComponent<ListedAssetController>().myMasterScript = transform.GetComponent<MasterController>();
                newText.GetComponent<Text>().text = asset.name;
                newText.transform.SetParent(panel.transform);
                newText.transform.localScale = new Vector3(1, 1, 1);

                GameObject seperator = Instantiate(Resources.Load("SeperatorImage", typeof(GameObject))) as GameObject;
                seperator.transform.SetParent(panel.transform);
            }
        }

        #endregion
    }
}
