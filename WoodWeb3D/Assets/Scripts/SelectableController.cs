﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectableController : MonoBehaviour {

    [HideInInspector]
    public List<string> infoValues = new List<string>();

    [HideInInspector]
    public float outlineWidth;

    [HideInInspector]
    public bool xAxis, yAxis, zAxis;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
