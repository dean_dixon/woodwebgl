﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform cameraOrbit, target;

    public float rotateSpeed = 5f;

    private float midMouseButtonHeldTime = 0, lerpTime = 0, zoomFactor = 1, zoomMultiplierTimer = 0, prevZoomWay = 1;

    private bool isMidMouseButtonHeld = false;

    private Vector3 lerpTarget = Vector3.zero;

    private PlayerController myPlayerScript;

    private void Awake()
    {
        myPlayerScript = transform.parent.GetComponent<PlayerController>();
    }

    void Start()
    {
        cameraOrbit.position = target.position;
    }

    void Update()
    {
        zoomMultiplierTimer += Time.deltaTime;
        if(lerpTarget != Vector3.zero)
        {
            lerpTime += Time.deltaTime;
            cameraOrbit.transform.parent.position = Vector3.Lerp(cameraOrbit.transform.parent.position, lerpTarget, .1f);

            if(lerpTime >= .5f)
            {
                //cameraOrbit.transform.parent.position = lerpTarget;
                lerpTime = 0;
                lerpTarget = Vector3.zero;
            }
        }

        if (Input.GetMouseButton(2) && Input.GetKey(KeyCode.LeftAlt))
        {
            float rotateMult = 1;
            if(Input.GetKey(KeyCode.LeftControl))
            {
                rotateMult = .1f;
            }

            if (!isMidMouseButtonHeld)
            {
                isMidMouseButtonHeld = true;
            }
            else
            {
                midMouseButtonHeldTime += Time.deltaTime;
            }

            float h = rotateSpeed * Input.GetAxis("Mouse X") * rotateMult;
            float v = rotateSpeed * Input.GetAxis("Mouse Y") * rotateMult;

            if (cameraOrbit.transform.eulerAngles.z + v <= 0.1f || cameraOrbit.transform.eulerAngles.z + v >= 179.9f)
            {
                v = 0;
            }

            cameraOrbit.transform.eulerAngles = new Vector3(cameraOrbit.transform.eulerAngles.x, cameraOrbit.transform.eulerAngles.y + h, cameraOrbit.transform.eulerAngles.z + v);
        }
        else if (Input.GetMouseButton(2))
        {
            if(!isMidMouseButtonHeld)
            {
                isMidMouseButtonHeld = true;
            }
            else
            {
                midMouseButtonHeldTime += Time.deltaTime;
            }
            float h = Input.GetAxis("Mouse X");
            float v = Input.GetAxis("Mouse Y");

            Vector3 upDir = transform.up;
            //forwardDir.y = 0;
            upDir.Normalize();

            Vector3 rightDir = transform.right;
            rightDir.y = 0;
            rightDir.Normalize();

            Vector3 rightMove = rightDir * h;
            Vector3 upMove = upDir * v;

            // good for ortho size 20

            float moveScale = transform.GetComponent<Camera>().orthographicSize / 20.0f;
            cameraOrbit.transform.parent.position -= Input.GetAxis("Mouse X") * rightDir * moveScale;
            cameraOrbit.transform.parent.position -= Input.GetAxis("Mouse Y") * upDir* moveScale;
        }

        if(Input.GetMouseButtonUp(2))
        {
            isMidMouseButtonHeld = false;
            //Debug.Log(midMouseButtonHeldTime);
            if (midMouseButtonHeldTime <= .13)
            {
                int lay = LayerMask.NameToLayer("Detail Object");
                int lay2 = LayerMask.NameToLayer("Detail Object ROV");
                int lay4 = 0;
                int layermask1 = (1 << lay) | (1 << lay2);

                RaycastHit hit;
                Ray ray = new Ray();

                ray = transform.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
                //Debug.Log("yes1");
                if (Physics.Raycast(ray, out hit, 999999999.0f, layermask1))
                {
                    lerpTarget = hit.point;
                    //Debug.Log("yes2");
                }
            }

            midMouseButtonHeldTime = 0;
        }

        float scrollFactor = Input.GetAxis("Mouse ScrollWheel");

        if(scrollFactor != 0)
        {
            if (zoomMultiplierTimer < .5f)
            {
                if (prevZoomWay < 0 && scrollFactor < 0)
                {
                    zoomFactor += 1f;
                }
                else if (prevZoomWay > 0 && scrollFactor > 0)
                {
                    zoomFactor += 1f;
                }               
            }
            else
            {
                zoomFactor = 1;
            }
            zoomMultiplierTimer = 0;
            prevZoomWay = scrollFactor;
        }
        else
        {
            zoomFactor = 1;
        }

        //Vector3 moveFactor 
        float scrollScale = 5;

        if(Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        {
            //scrollScale = 1;
        }
        else if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            //scrollScale = 10;
        }


        if (scrollFactor != 0)
        {
            scrollScale = transform.GetComponent<Camera>().orthographicSize/ 2.0f;

            transform.GetComponent<Camera>().orthographicSize -= scrollFactor * zoomFactor * scrollScale;
            //cameraOrbit.transform.localScale = cameraOrbit.transform.localScale * (1f - scrollFactor);
            if(transform.GetComponent<Camera>().orthographicSize <= .01f)
            {
                transform.GetComponent<Camera>().orthographicSize = .01f;
            }
            
        }


        transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0);

        transform.LookAt(target.position);

        if(myPlayerScript != null && myPlayerScript.LayoutDrawing != null)
        {
            //Debug.Log(Mathf.Abs(cameraOrbit.localEulerAngles.z));
            if(Mathf.Abs(cameraOrbit.localEulerAngles.z) < 45)
            {
                //myPlayerScript.LayoutDrawing.SetActive(true);
                if (myPlayerScript.LayoutDrawing.GetComponent<SpriteRenderer>().color.a < 1)
                {
                    myPlayerScript.LayoutDrawing.GetComponent<DrawingController>().isFadingOut = false;
                    myPlayerScript.LayoutDrawing.GetComponent<DrawingController>().isFadingIn = true;
                }
            }
            else
            {
                //myPlayerScript.LayoutDrawing.SetActive(false);
                if (myPlayerScript.LayoutDrawing.GetComponent<SpriteRenderer>().color.a > 0)
                {
                    myPlayerScript.LayoutDrawing.GetComponent<DrawingController>().isFadingOut = true;
                    myPlayerScript.LayoutDrawing.GetComponent<DrawingController>().isFadingIn = false;
                }
            }
        }
    }

}
