﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointController : MonoBehaviour {

    public Color waypointColor;

    //private GameObject player;
	// Use this for initialization
	void Start () {
        transform.GetComponent<Renderer>().material.color = waypointColor;

        //player = GameObject.Find("Player/[CameraRig]");

	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(new Vector3(0, 0, 1.5f));

        /*
        if(Vector3.Distance(player.transform.position,transform.position) < 3)
        {
            transform.GetComponent<Renderer>().enabled = false;
        }
        else
        {
            transform.GetComponent<Renderer>().enabled = true;
        }
        */
	}
}
