﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Net;
using System.IO;
using System;
using System.Text.RegularExpressions;
using System.Text;

public class GISController : MonoBehaviour
{

    public GameObject GISStructurePrefab, GISLineRendererPrefab;

    public Vector3 offset;

    //public string restURL = "https://www.arcgis.com/sharing/rest/oauth2/token";
    //public string url = "https://wp-app-arcgis-5.woodgroup.com:6443/arcgis/rest/services/GoM/GC903_MapService/MapServer/0/query?where=TagNumber+LIKE+%27%25HB%25%27&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=&returnGeometry=true&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentsOnly=false&datumTransformation=&parameterValues=&rangeValues=&f=pjson";
    private string tokenURL = "http://wp-app-arcgis-5.woodgroup.com:6443/arcgis/tokens/";
    public string tokenURL2 = "https://wp-app-arcgis-5.woodgroup.com:6443/arcgis/tokens/generateToken";
    //public string mapServerURL = "https://wp-app-arcgis-5.woodgroup.com:6443/arcgis/rest/services/GoM/GC903_MapService/MapServer/0";
    //string cookie = null;

    private MasterController myMasterScript;

    private float resetMasterAssetsTimer = 0;
    private bool startMasterAssetsTimer = false;

    public Material orangeMat, yellowMat, steelMat, whiteMat, greenMat;

    [HideInInspector]
    public string token = "";

    private void Awake()
    {
        if (GameObject.Find("WorldMaster"))
        {
           token = GameObject.Find("WorldMaster").transform.GetComponent<WorldMasterController>().GISToken;
        }

        if (GameObject.Find("Master"))
        {
            myMasterScript = GameObject.Find("Master").transform.GetComponent<MasterController>();
        }

        if (myMasterScript != null && myMasterScript.isGIS)
        {
            StartCoroutine(Upload());
        }
    }

    void Start()
    {
        Application.OpenURL("file:///G:/Drafting/3DS Max/VR/WebBuilds/Geometry/GC903.pdf");
    }

    private void Update()
    {

    }

    IEnumerator Upload()
    {
        /*
        WWWForm form = new WWWForm();
        form.AddField("username", "Wood_VR_Team");
        form.AddField("password", "WoodVRTeam");
        form.AddField("slbxClient", "requestip");
        form.AddField("slbxClientUi", "requestip");
        form.AddField("f", "json");


        using ( UnityWebRequest www = UnityWebRequest.Post(tokenURL2, form))
        {
            //www.SetRequestHeader("Access-Control-Allow-Credentials", "true");
            //www.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
            //www.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
            //www.SetRequestHeader("Access-Control-Allow-Origin", "*");
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                Debug.Log(www.GetResponseHeaders());
            }
        }
        */

        WWWForm form2 = new WWWForm();
        form2.AddField("token", token);

        using (UnityWebRequest www2 = UnityWebRequest.Post("https://wp-app-arcgis-5.woodgroup.com:6443/arcgis/rest/services/GoM/GC903_MapService/MapServer/0/query?where=TagNumber+LIKE+%27%25%25%27&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=true&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentsOnly=false&datumTransformation=&parameterValues=&rangeValues=&f=pjson", form2))
        {
            yield return www2.SendWebRequest();
            if (www2.isNetworkError || www2.isHttpError)
            {
                //Debug.Log(www2.error);
            }
            else
            {
                //Debug.Log("Form upload complete 3!");
                //Debug.Log(www2.downloadHandler.text);
                string input = www2.downloadHandler.text;
                string splitter = "attributes";
                char[] split = splitter.ToCharArray();
                string[] strings = input.Split(split, StringSplitOptions.None);

                string[] stringSeparators = new string[] { "attributes" };
                var result = input.Split(stringSeparators, StringSplitOptions.None);

                int x = 0;
                foreach (string s in result)
                {
                    if (x > 0)
                    {
                        String regex = "\\s+(?=((\\\\[\\\\\"]|[^\\\\\"])*\"(\\\\[\\\\\"]|[^\\\\\"])*\")*(\\\\[\\\\\"]|[^\\\\\"])*$)";

                        string s22 = s;
                        s22 = s22.Replace("\n", String.Empty);
                        s22 = s22.Replace("\r", String.Empty);
                        s22 = s22.Replace("\t", String.Empty);
                        s22 = s22.Remove(0, 3);
                        s22 = s22.Remove(s22.Length - 4, 4);
                        s22 = Regex.Replace(s22, regex, "");
                        s22 = s22.Replace("{", "");
                        s22 = s22.Replace("}", "");
                        s22 = s22.Replace("\"", "");
                        s22 = s22.Replace("geometry:", "");
                        //Debug.Log(s22);

                        string[] stringSeparators2 = new string[] { "," };
                        var result2 = s22.Split(stringSeparators2, StringSplitOptions.None);

                        GameObject gisObject = Instantiate(GISStructurePrefab, transform.position, Quaternion.identity);

                        foreach (string s3 in result2)
                        {
                            //Debug.Log(s3);
                            string[] stringSeparators3 = new string[] { ":" };
                            var result3 = s3.Split(stringSeparators3, StringSplitOptions.None);
                            if (result3[0].Equals("TagNumber"))
                            {
                                gisObject.name = result3[1];
                            }

                            if (result3[0].Equals("x"))
                            {
                                gisObject.transform.position = new Vector3((float.Parse(result3[1]) - offset.x) * .3048f, gisObject.transform.position.y, gisObject.transform.position.z);
                            }

                            if (result3[0].Equals("y"))
                            {
                                gisObject.transform.position = new Vector3(gisObject.transform.position.x, gisObject.transform.position.y, (float.Parse(result3[1]) - offset.z) * .3048f);
                            }

                            if (result3[0].Equals("WaterDepth"))
                            {
                                gisObject.transform.position = new Vector3(gisObject.transform.position.x, (float.Parse(result3[1]) - offset.y) * .3048f, gisObject.transform.position.z);
                            }

                            if (result3[0].Equals("Orientation"))
                            {
                                gisObject.transform.Rotate(0, float.Parse(result3[1]), 0);
                            }
                        }

                        foreach (string s3 in result2)
                        {
                            //Debug.Log(s3);
                            string[] stringSeparators3 = new string[] { ":" };
                            var result3 = s3.Split(stringSeparators3, StringSplitOptions.None);
                            if (result3[0].Equals("TagNumber"))
                            {
                                if (GameObject.Find(result3[1]))
                                {
                                    //Debug.Log("Found object " + result3[1]);
                                    GameObject obj = GameObject.Find(result3[1]);
                                    SelectableController objScript = obj.GetComponent<SelectableController>();
                                    if (objScript != null)
                                    {
                                        if (!myMasterScript.allAssets.Contains(obj))
                                        {
                                            myMasterScript.allAssets.Add(obj);
                                        }

                                        foreach (string s4 in result2)
                                        {
                                            string[] stringSeparators4 = new string[] { ":" };
                                            var result4 = s4.Split(stringSeparators3, StringSplitOptions.None);

                                            if (result4[0].Equals("Orientation") || result4[0].Equals("WaterDepth") || result4[0].Equals("x") || result4[0].Equals("y"))
                                            {
                                                if (result4[0].Equals("Orientation"))
                                                {
                                                    objScript.infoValues.Add(result4[0] + " : " + float.Parse(result4[1]).ToString("F") + "°");
                                                }
                                                else if (result4[0].Equals("WaterDepth"))
                                                {
                                                    objScript.infoValues.Add(result4[0] + " : " + float.Parse(result4[1]).ToString("F") + " (ft)");
                                                }
                                                else
                                                {
                                                    objScript.infoValues.Add(result4[0] + " : " + float.Parse(result4[1]).ToString("F"));
                                                }
                                                
                                            }
                                            else
                                            {
                                                objScript.infoValues.Add(result4[0] + " : " + result4[1]);
                                            }
                                        }
                                    }
                                }
                            }

                            if (result3[0].Equals("Name"))
                            {
                                gisObject.AddComponent<OBJLoader>();
                                if (result3[1].Contains("Tree"))
                                {
                                    gisObject.GetComponent<OBJLoader>().structureName = "Tree";
                                }
                                else if (result3[1].Contains("Well"))
                                {
                                    gisObject.GetComponent<OBJLoader>().structureName = "AbandonedWellhead";
                                }
                                else if (result3[1].Contains("Manifold"))
                                {
                                    gisObject.GetComponent<OBJLoader>().structureName = "Manifold";
                                }
                                else if (result3[1].Contains("Umbilical Subsea Distribution Unit"))
                                {
                                    gisObject.GetComponent<OBJLoader>().structureName = "SDU";
                                }
                                else if (result3[1].Contains("FMC Connector"))
                                {
                                    gisObject.GetComponent<OBJLoader>().structureName = "PLEM";
                                }
                                else if (result3[1].Contains("Intermediate Skid"))
                                {
                                    gisObject.GetComponent<OBJLoader>().structureName = "IntermediateSkid";
                                }

                                gisObject.GetComponent<OBJLoader>().loadGeometry();

                                //gisObject.transform.Rotate(0,180,0);

                                //geom.transform.parent = gisObject.transform;
                                //geom.transform.localPosition = Vector3.zero;
                            }
                        }
                    }
                    x++;
                }
            }
        }

        using (UnityWebRequest www3 = UnityWebRequest.Post("https://wp-app-arcgis-5.woodgroup.com:6443/arcgis/rest/services/GoM/GC903_MapService/MapServer/1/query?where=TagNumber+LIKE+%27%25%25%27&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=true&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentsOnly=false&datumTransformation=&parameterValues=&rangeValues=&f=pjson", form2))
        {
            yield return www3.SendWebRequest();
            if (www3.isNetworkError || www3.isHttpError)
            {
                //Debug.Log(www3.error);
            }
            else
            {
                //Debug.Log("Form upload complete 4!");
                //Debug.Log(www3.downloadHandler.text);
                string input = www3.downloadHandler.text;
                string splitter = "attributes";
                char[] split = splitter.ToCharArray();
                string[] strings = input.Split(split, StringSplitOptions.None);

                string[] stringSeparators = new string[] { "attributes" };
                var result = input.Split(stringSeparators, StringSplitOptions.None);

                int x = 0;
                foreach (string s in result)
                {
                    if (x > 0)
                    {
                        //Material mat = new Material();
                        float tubeRadius = 0;
                        String regex = "\\s+(?=((\\\\[\\\\\"]|[^\\\\\"])*\"(\\\\[\\\\\"]|[^\\\\\"])*\")*(\\\\[\\\\\"]|[^\\\\\"])*$)";

                        string s22 = s;
                        s22 = s22.Replace("\n", String.Empty);
                        s22 = s22.Replace("\r", String.Empty);
                        s22 = s22.Replace("\t", String.Empty);
                        s22 = s22.Remove(0, 3);
                        s22 = s22.Remove(s22.Length - 4, 4);
                        s22 = Regex.Replace(s22, regex, "");
                        s22 = s22.Replace("{", "");
                        s22 = s22.Replace("}", "");
                        s22 = s22.Replace("\"", "");
                        s22 = s22.Replace("geometry:", "");
                        //Debug.Log(s22);

                        string[] seperatePaths = new string[] { ",paths:" };
                        var resultOrig = s22.Split(seperatePaths, StringSplitOptions.None);

                        string[] stringSeparators2 = new string[] { "," };
                        var result2 = s22.Split(stringSeparators2, StringSplitOptions.None);
                        var result5 = resultOrig[0].Split(stringSeparators2, StringSplitOptions.None);
                        GameObject gisObject = Instantiate(GISLineRendererPrefab, transform.position, Quaternion.identity);

                        foreach (string s3 in result2)
                        {
                            //Debug.Log(s3);
                            string[] stringSeparators3 = new string[] { ":" };
                            var result3 = s3.Split(stringSeparators3, StringSplitOptions.None);
                            if (result3[0].Equals("TagNumber"))
                            {
                                gisObject.name = result3[1];

                                if (GameObject.Find(result3[1]))
                                {
                                    //Debug.Log("Found object " + result3[1]);
                                    GameObject obj = GameObject.Find(result3[1]);
                                    SelectableController objScript = obj.GetComponent<SelectableController>();
                                    if (objScript != null)
                                    {
                                        if (!myMasterScript.allAssets.Contains(obj))
                                        {
                                            myMasterScript.allAssets.Add(obj);
                                        }

                                        foreach (string s4 in result5)
                                        {
                                            //Debug.Log("String 4 = " + s4);
                                            string[] stringSeparators4 = new string[] { ":" };
                                            var result4 = s4.Split(stringSeparators3, StringSplitOptions.None);
                                            //objScript.infoValues.Add(result4[0] + " : " + result4[1]);

                                            if (result4[0].Equals("SHAPE_Length"))
                                            {
                                                objScript.infoValues.Add(result4[0] + " : " + float.Parse(result4[1]).ToString("F") + " (ft)");
                                            }
                                            else
                                            {
                                                objScript.infoValues.Add(result4[0] + " : " + result4[1]);
                                            }
                                        }
                                    }
                                }
                            }

                            if (result3[0].Equals("Name"))
                            {
                                

                                if (result3[1].Contains("Steel"))
                                {
                                    gisObject.GetComponent<LineRenderer>().SetColors(new Color(.5f, .5f, .5f), new Color(.5f, .5f, .5f));
                                    gisObject.GetComponent<LineRenderer>().SetWidth(.08f, .08f);
                                    tubeRadius = .08f;
                                    Material[] mat = new Material[1];
                                    mat[0] = new Material(steelMat);
                                    gisObject.GetComponent<MeshRenderer>().materials = mat;
                                    gisObject.GetComponent<PipeMaker>().pipeRadius = .08f;
                                }
                                else if (result3[1].Contains("EFL"))
                                {
                                    gisObject.GetComponent<LineRenderer>().SetColors(new Color(1, .25f, 0), new Color(1, .25f, 0));
                                    gisObject.GetComponent<LineRenderer>().SetWidth(.04f, .04f);
                                    tubeRadius = .04f;
                                    Material[] mat = new Material[1];
                                    mat[0] = new Material(orangeMat);
                                    gisObject.GetComponent<MeshRenderer>().materials = mat;
                                    gisObject.GetComponent<PipeMaker>().pipeRadius = .04f;
                                }
                                else if (result3[1].Contains("Jumper"))
                                {
                                    gisObject.GetComponent<LineRenderer>().SetColors(new Color(1, 1, 1), new Color(1, 1, 1));
                                    gisObject.GetComponent<LineRenderer>().SetWidth(.2f, .2f);
                                    tubeRadius = .2f;
                                    Material[] mat = new Material[1];
                                    mat[0] = new Material(whiteMat);
                                    gisObject.GetComponent<MeshRenderer>().materials = mat;
                                    gisObject.GetComponent<PipeMaker>().pipeRadius = .2f;
                                }
                                else if (result3[1].Contains("Flowline"))
                                {
                                    gisObject.GetComponent<LineRenderer>().SetColors(new Color(0, 1, 0), new Color(0, 1, 0));
                                    gisObject.GetComponent<LineRenderer>().SetWidth(.2f, .2f);
                                    tubeRadius = .2f;
                                    Material[] mat = new Material[1];
                                    mat[0] = new Material(greenMat);
                                    gisObject.GetComponent<MeshRenderer>().materials = mat;
                                    gisObject.GetComponent<PipeMaker>().pipeRadius = .2f;
                                }
                                else if (result3[1].Contains("Umbilical"))
                                {
                                    gisObject.GetComponent<LineRenderer>().SetColors(new Color(1, 1, 0), new Color(1, 1, 0));
                                    gisObject.GetComponent<LineRenderer>().SetWidth(.2f, .2f);
                                    tubeRadius = .2f;
                                    Material[] mat = new Material[1];
                                    mat[0] = new Material(yellowMat);
                                    gisObject.GetComponent<MeshRenderer>().materials = mat;
                                    gisObject.GetComponent<PipeMaker>().pipeRadius = .2f;
                                }

                                //Debug.Log("Found name column");
                                //Debug.Log(result3[1]);

                                

                            }
                        }

                        string[] stringSeparatorsPath = new string[] { ",paths:" };
                        var resultPaths = s22.Split(stringSeparatorsPath, StringSplitOptions.None);
                        string pathsString = resultPaths[1];
                        pathsString = pathsString.Remove(0, 2);
                        //Debug.Log(pathsString);

                        string[] valueSeparator = new string[] { "],[" };
                        var values = pathsString.Split(valueSeparator, StringSplitOptions.None);

                        gisObject.GetComponent<LineRenderer>().numPositions = values.Length;
                        int lineIndex = 0;

                        

                        foreach (string value in values)
                        {
                            string newValue = value.Replace("[", String.Empty);
                            newValue = newValue.Replace("]", String.Empty);

                            string[] positionSeperator = new string[] { "," };
                            var positions = newValue.Split(positionSeperator, StringSplitOptions.None);

                            gisObject.GetComponent<LineRenderer>().SetPosition(lineIndex, new Vector3((float.Parse(positions[0]) - offset.x) * .3048f, 0, (float.Parse(positions[1]) - offset.z) * .3048f));
                
                            lineIndex++;
                        }

                        Vector3[] points = new Vector3[gisObject.GetComponent<LineRenderer>().positionCount];
                        gisObject.GetComponent<LineRenderer>().GetPositions(points);

                        Vector3[] newLine = Curver.MakeSmoothCurve(points, 1);
                        //Vector3[] newLine = LineSmoother.SmoothLine(points, 10);

                        gisObject.GetComponent<LineRenderer>().SetPositions(newLine);

                        
                        for(int n = 0; n < gisObject.GetComponent<LineRenderer>().positionCount; n++)
                        {
                            if (n > 0)
                            {
                                AddColliderToLine(gisObject.GetComponent<LineRenderer>(), gisObject.GetComponent<LineRenderer>().GetPosition(n - 1), gisObject.GetComponent<LineRenderer>().GetPosition(n), true);
                                //AddColliderToLine(gisObject.GetComponent<LineRenderer>(), gisObject.GetComponent<LineRenderer>().GetPosition(n - 1), gisObject.GetComponent<LineRenderer>().GetPosition(n), false);
                            }
                        }

                        //calculateMesh(gisObject.GetComponent<LineRenderer>());
                        GameObject pipeCollider = new GameObject("PipeMeshCollider");

                        pipeCollider.AddComponent<MeshFilter>();
                        pipeCollider.AddComponent<MeshRenderer>();
                        pipeCollider.GetComponent<MeshRenderer>().enabled = false;

                        pipeCollider.AddComponent<PipeMaker>();
                        pipeCollider.GetComponent<PipeMaker>().pipeRadius = gisObject.GetComponent<PipeMaker>().pipeRadius;
                        pipeCollider.GetComponent<PipeMaker>().pipeSegmentCount = gisObject.GetComponent<PipeMaker>().pipeSegmentCount;
                        pipeCollider.GetComponent<PipeMaker>().dotPrefab = gisObject.GetComponent<PipeMaker>().dotPrefab;

                        pipeCollider.AddComponent<LineRenderer>();
                        pipeCollider.GetComponent<LineRenderer>().numPositions = values.Length;
                        pipeCollider.GetComponent<LineRenderer>().SetPositions(newLine);
                        pipeCollider.GetComponent<LineRenderer>().enabled = false;
                        pipeCollider.transform.SetParent(gisObject.transform);
                        pipeCollider.transform.position = Vector3.zero;
                        pipeCollider.transform.rotation = Quaternion.identity;
                        pipeCollider.layer = 8;
                        gisObject.GetComponent<PipeMaker>().makeMesh();
                        pipeCollider.GetComponent<PipeMaker>().makeMesh();
                        pipeCollider.AddComponent<MeshCollider>();
                    }

                    x++;
                    
                }
            }
        }
        GameObject.Find("Master").GetComponent<MasterController>().refreshAssetsLists();
        
    }

    private void AddColliderToLine(LineRenderer line, Vector3 startPoint, Vector3 endPoint, bool isSelectable)
    {
        //create the collider for the line
        BoxCollider lineCollider = new GameObject("ProxyCube").AddComponent<BoxCollider>();
        //set the collider as a child of your line
        lineCollider.transform.parent = line.transform;
        // get width of collider from line 
        float lineWidth = line.endWidth;
        // get the length of the line using the Distance method
        float lineLength = Vector3.Distance(startPoint, endPoint);
        // size of collider is set where X is length of line, Y is width of line
        //z will be how far the collider reaches to the sky
        lineCollider.size = new Vector3(lineLength, lineWidth, lineWidth);
        // get the midPoint
        Vector3 midPoint = (startPoint + endPoint) / 2;
        // move the created collider to the midPoint
        lineCollider.transform.localPosition = midPoint;


        //heres the beef of the function, Mathf.Atan2 wants the slope, be careful however because it wants it in a weird form
        //it will divide for you so just plug in your (y2-y1),(x2,x1)
        float angle = Mathf.Atan2((endPoint.z - startPoint.z), (endPoint.x - startPoint.x));

        // angle now holds our answer but it's in radians, we want degrees
        // Mathf.Rad2Deg is just a constant equal to 57.2958 that we multiply by to change radians to degrees
        angle *= Mathf.Rad2Deg;

        //were interested in the inverse so multiply by -1
        angle *= -1;
        // now apply the rotation to the collider's transform, carful where you put the angle variable
        // in 3d space you don't wan't to rotate on your y axis
        lineCollider.transform.Rotate(0, angle, 0);

        lineCollider.isTrigger = true;

        if (isSelectable)
        {
            lineCollider.transform.gameObject.layer = LayerMask.NameToLayer("Selectable");
        }
        else
        {
            lineCollider.transform.gameObject.layer = LayerMask.NameToLayer("Detail Object");
        }
    }

    private void calculateMesh(LineRenderer line)
    {
        List<Vector3> points = new List<Vector3>();
        points.Clear();
        GameObject caret = null;
        caret = new GameObject("Lines");

        Vector3 left, right; // A position to the left of the current line

        // For all but the last point
        for (var i = 0; i < line.positionCount - 1; i++)
        {
            caret.transform.position = line.GetPosition(i);
            caret.transform.LookAt(line.GetPosition(i + 1));
            right = caret.transform.position + transform.right * line.startWidth / 2;
            left = caret.transform.position - transform.right * line.startWidth / 2;
            points.Add(left);
            points.Add(right);
        }

        // Last point looks backwards and reverses
        caret.transform.position = line.GetPosition(line.positionCount - 1);
        caret.transform.LookAt(line.GetPosition(line.positionCount - 2));
        right = caret.transform.position + transform.right * line.startWidth / 2;
        left = caret.transform.position - transform.right * line.startWidth / 2;
        points.Add(left);
        points.Add(right);
        Destroy(caret);
        Vector3[] verticies = new Vector3[points.Count];

        for (int i = 0; i < verticies.Length; i++)
        {
            verticies[i] = points[i];
        }

        int[] triangles = new int[((points.Count / 2) - 1) * 6];

        //Works on linear patterns tn = bn+c
        int position = 6;
        for (int i = 0; i < (triangles.Length / 6); i++)
        {
            triangles[i * position] = 2 * i;
            triangles[i * position + 3] = 2 * i;

            triangles[i * position + 1] = 2 * i + 3;
            triangles[i * position + 4] = (2 * i + 3) - 1;

            triangles[i * position + 2] = 2 * i + 1;
            triangles[i * position + 5] = (2 * i + 1) + 2;
        }


        Mesh mesh = line.transform.GetComponent<MeshFilter>().mesh;
        mesh.Clear();
        mesh.vertices = verticies;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();

    }

    private void makeMesh()
    {
        MeshFilter filter = gameObject.AddComponent<MeshFilter>();
        Mesh mesh = filter.mesh;
        mesh.Clear();

        float radius1 = 1f;
        float radius2 = .3f;
        int nbRadSeg = 24;
        int nbSides = 18;

        #region Vertices		
        Vector3[] vertices = new Vector3[(nbRadSeg + 1) * (nbSides + 1)];
        float _2pi = Mathf.PI * 2f;
        for (int seg = 0; seg <= nbRadSeg; seg++)
        {
            int currSeg = seg == nbRadSeg ? 0 : seg;

            float t1 = (float)currSeg / nbRadSeg * _2pi;
            Vector3 r1 = new Vector3(Mathf.Cos(t1) * radius1, 0f, Mathf.Sin(t1) * radius1);

            for (int side = 0; side <= nbSides; side++)
            {
                int currSide = side == nbSides ? 0 : side;

                Vector3 normale = Vector3.Cross(r1, Vector3.up);
                float t2 = (float)currSide / nbSides * _2pi;
                Vector3 r2 = Quaternion.AngleAxis(-t1 * Mathf.Rad2Deg, Vector3.up) * new Vector3(Mathf.Sin(t2) * radius2, Mathf.Cos(t2) * radius2);

                vertices[side + seg * (nbSides + 1)] = r1 + r2;
            }
        }
        #endregion

        #region Normales		
        Vector3[] normales = new Vector3[vertices.Length];
        for (int seg = 0; seg <= nbRadSeg; seg++)
        {
            int currSeg = seg == nbRadSeg ? 0 : seg;

            float t1 = (float)currSeg / nbRadSeg * _2pi;
            Vector3 r1 = new Vector3(Mathf.Cos(t1) * radius1, 0f, Mathf.Sin(t1) * radius1);

            for (int side = 0; side <= nbSides; side++)
            {
                normales[side + seg * (nbSides + 1)] = (vertices[side + seg * (nbSides + 1)] - r1).normalized;
            }
        }
        #endregion

        #region UVs
        Vector2[] uvs = new Vector2[vertices.Length];
        for (int seg = 0; seg <= nbRadSeg; seg++)
            for (int side = 0; side <= nbSides; side++)
                uvs[side + seg * (nbSides + 1)] = new Vector2((float)seg / nbRadSeg, (float)side / nbSides);
        #endregion

        #region Triangles
        int nbFaces = vertices.Length;
        int nbTriangles = nbFaces * 2;
        int nbIndexes = nbTriangles * 3;
        int[] triangles = new int[nbIndexes];

        int i = 0;
        for (int seg = 0; seg <= nbRadSeg; seg++)
        {
            for (int side = 0; side <= nbSides - 1; side++)
            {
                int current = side + seg * (nbSides + 1);
                int next = side + (seg < (nbRadSeg) ? (seg + 1) * (nbSides + 1) : 0);

                if (i < triangles.Length - 6)
                {
                    triangles[i++] = current;
                    triangles[i++] = next;
                    triangles[i++] = next + 1;

                    triangles[i++] = current;
                    triangles[i++] = next + 1;
                    triangles[i++] = current + 1;
                }
            }
        }
        #endregion

        mesh.vertices = vertices;
        mesh.normals = normales;
        mesh.uv = uvs;
        mesh.triangles = triangles;

        mesh.RecalculateBounds();
        //mesh.
    }

    public void setToken(InputField input)
    {
        token = input.text;
    }

}
