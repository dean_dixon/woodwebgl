﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingDotController : MonoBehaviour {

    public float offset;

    float timer;

    public bool isGoingDown = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if(GetComponent<Image>().color.a >= 1.0f)
        {
            GetComponent<Image>().color = new Color(1, 1, 1, 1.0f);
            isGoingDown = true;
        }

        if (GetComponent<Image>().color.a <= 0.0f)
        {
            GetComponent<Image>().color = new Color(1, 1, 1, 0.0f);
            isGoingDown = false;
        }

        if (!isGoingDown)
        {
            GetComponent<Image>().color = new Color(1, 1, 1, GetComponent<Image>().color.a + ((255.0f/8.0f)/255.0f/5.0f)*Time.deltaTime*100.0f);
        }
        else
        {
            GetComponent<Image>().color = new Color(1, 1, 1, GetComponent<Image>().color.a - ((255.0f / 8.0f)/255.0f/5.0f)*Time.deltaTime*100.0f);
        }
	}
}
