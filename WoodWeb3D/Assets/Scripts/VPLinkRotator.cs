﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using System;
using System.Text.RegularExpressions;

public class VPLinkRotator : MonoBehaviour {

    /*
    public bool isX;
    public bool isY;
    public bool isZ;

    // flowrate tag
    public string equipmentName;


    private VPDatabase vpd;

    string tagName;
    [HideInInspector]
    public string value;

    private void Awake()
    {
        StreamReader reader;

        string defaultPath = "Assets/Resources/VPValves.txt";
        string path = "C:/ProgramData/CapeSoftware/VPLink3/VR/VPValves.txt";
        string configPath = "C:/ProgramData/CapeSoftware/VPLink3/VR/VR_Config.txt";
        string tagFileName = "";
        string line;

        bool exists = System.IO.File.Exists(configPath);
        if (exists)
        {
            reader = new StreamReader(configPath);
            while ((line = reader.ReadLine()) != null)
            {
                //line = line.Replace("\t", string.Empty);
                string[] fields = line.Split(',');
                //Debug.Log("Line=" + line + "; looking for " + equipmentName);

                char[] chars = fields[0].ToCharArray();
                if (!chars[0].Equals('#'))
                {
                    tagFileName = fields[0];
                }
            }

            path = "C:/ProgramData/CapeSoftware/VPLink3/VR/" + tagFileName;
        }

        exists = System.IO.File.Exists(path);

        if (exists)
        {
            reader = new StreamReader(path);
        }
        else
        {
            reader = new StreamReader(defaultPath);
        }
        while ((line = reader.ReadLine()) != null)
        {
            //line = line.Replace("\t", string.Empty);
            string[] fields = line.Split(',');
            //Debug.Log("Line=" + line + "; looking for " + equipmentName);

            char[] chars = fields[0].ToCharArray();
            if ((!chars[0].Equals('#')) && (fields[0].Length > 0))
            {
                if (String.Equals(fields[0], equipmentName, StringComparison.CurrentCultureIgnoreCase))
                {
                    //Debug.Log("Fields[0]=" + fields[0]);
                    tagName = fields[1];
                    break;
                }
                //objScript.isValve = true;
            }
        }

        reader.Close();
    }
    // Use this for initialization
    void Start()
    {
        vpd = FindObjectOfType<VPDatabase>();
        if (vpd == null)
        {
            Debug.LogError("Cannot find VP Link database in the VerMovement object");
        }
        else
        {
            if (tagName.Length > 0)
            {
                value = vpd.PVEStr(tagName, "VPLinkTag ???");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (vpd != null)
        {
            if (tagName.Length > 0)
            {
                value = vpd.PVEStr(tagName, value);
            }
        }

        //RPM = (degrees / sec) * 0.166666666667

        float speed = float.Parse(ExtractNumber(value)) * (.104719755f) * Time.deltaTime;

        speed = Mathf.Rad2Deg*speed;

        if (isX)
        {
            transform.Rotate(new Vector3(speed, 0, 0));
        }
        else if (isY)
        {
            transform.Rotate(new Vector3(0, speed, 0));
        }
        else if (isZ)
        {
            transform.Rotate(new Vector3(0, 0, speed));
        }

    }

    void setVPValue(float newValue)
    {
        if (vpd != null)
        {
            if (tagName.Length > 0)
            {
                vpd.setPVE(tagName, newValue);
            }
        }
    }

    public string ExtractNumber(string original)
    {
        return Regex.Split(original, @"\D+")[0];
    }

    public void reloadConfig()
    {
        StreamReader reader;

        string defaultPath = "Assets/Resources/VPValves.txt";
        string path = "C:/ProgramData/CapeSoftware/VPLink3/VR/VPValves.txt";
        string configPath = "C:/ProgramData/CapeSoftware/VPLink3/VR/VR_Config.txt";
        string tagFileName = "";
        string line;

        bool exists = System.IO.File.Exists(configPath);
        if (exists)
        {
            reader = new StreamReader(configPath);
            while ((line = reader.ReadLine()) != null)
            {
                //line = line.Replace("\t", string.Empty);
                string[] fields = line.Split(',');
                //Debug.Log("Line=" + line + "; looking for " + equipmentName);

                char[] chars = fields[0].ToCharArray();
                if (!chars[0].Equals('#'))
                {
                    tagFileName = fields[0];
                }
            }

            path = "C:/ProgramData/CapeSoftware/VPLink3/VR/" + tagFileName;
        }

        exists = System.IO.File.Exists(path);

        if (exists)
        {
            reader = new StreamReader(path);
        }
        else
        {
            reader = new StreamReader(defaultPath);
        }
        while ((line = reader.ReadLine()) != null)
        {
            //line = line.Replace("\t", string.Empty);
            string[] fields = line.Split(',');
            //Debug.Log("Line=" + line + "; looking for " + equipmentName);

            char[] chars = fields[0].ToCharArray();
            if ((!chars[0].Equals('#')) && (fields[0].Length > 0))
            {
                if (String.Equals(fields[0], equipmentName, StringComparison.CurrentCultureIgnoreCase))
                {
                    //Debug.Log("Fields[0]=" + fields[0]);
                    tagName = fields[1];
                    break;
                }
                //objScript.isValve = true;
            }
        }

        reader.Close();
    }
    */
}
