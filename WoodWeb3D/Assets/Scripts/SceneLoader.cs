﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class SceneLoader : MonoBehaviour
{

    private bool loadScene = false;

    [SerializeField]
    private int scene;
    [SerializeField]
    private Text loadingText;
    AsyncOperation sceneAO;

    public Text loadingTextVR;

    // the actual percentage while scene is fully loaded
    private const float LOAD_READY_PERCENTAGE = 0.9f;

    public void Start()
    {
        //float progress = Mathf.Clamp01(sceneAO.progress / 0.9f);
        loadingText.text = "0%";
        StartCoroutine(LoadingSceneRealProgress(scene));
    }

    IEnumerator LoadingSceneRealProgress(int sceneNum)
    {
        yield return new WaitForSeconds(1);
        sceneAO = SceneManager.LoadSceneAsync(sceneNum);

        // disable scene activation while loading to prevent auto load
        sceneAO.allowSceneActivation = false;

        while (!sceneAO.isDone)
        {
            if (sceneAO.progress >= LOAD_READY_PERCENTAGE)
            {
                loadingText.text = "Activating Scene";
                //if (Input.GetKeyDown(KeyCode.Space))
                //{
                    sceneAO.allowSceneActivation = true;
                //}
            }
            else
            {
                float progress = Mathf.Clamp01(sceneAO.progress / 0.9f)*100.0f;
                loadingText.text = "Loading..." + "\n" + progress.ToString("F0") + "%";
                loadingTextVR.text = loadingText.text;
            }
            Debug.Log(sceneAO.progress);
            yield return null;
        }
    }

}