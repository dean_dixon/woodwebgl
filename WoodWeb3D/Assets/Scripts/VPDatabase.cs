﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using VP2KCLIENTLib;
using UnityEngine.UI;
using System.IO;

public class VPDatabase : MonoBehaviour
{
    
    //private VPClient vp;
    public string theDatabase;
    public bool isConnected = false;
    public string knownDatabases;

    public GameObject isConnectedIndicator;
    public GameObject isConnectedIndicatorVR;

    private Dictionary<string, string> gTagList = new Dictionary<string, string>();

    /*
    private void Awake()
    {
        vp = new VPClient();
    }

    public bool IsConnected()
    {
        return isConnected;
    }

    public float PVE(string tagname, float defaultValue)
    {
        float ans = defaultValue;
        float n;
        bool isNumeric = float.TryParse(tagname, out n);
        if (isNumeric)
        {
            ans = float.Parse(tagname);
        }
        else
        {
            if (isConnected)
            {
                ans = (float)vp.get_PVE(tagname);
                isConnectedIndicator.GetComponent<Text>().text = "VPLink Connected: Yes";
            }
            else
            {
                isConnectedIndicator.GetComponent<Text>().text = "VPLink Connected: No";
            }
        }
        return ans;
    }

    public float PV(string tagname, float defaultValue)
    {
        float ans = defaultValue;
        float n;
        bool isNumeric = float.TryParse(tagname, out n);
        if (isNumeric)
        {
            ans = float.Parse(tagname);
        }
        else
        {
            
            if (isConnected)
            {
                ans = (float)vp.get_PV(tagname);
            }
        }
        return ans;
    }

    public string PVEStr(string tagname, string defaultValue)
    {
        string ans = defaultValue;
        float n;
        bool isNumeric = float.TryParse(tagname, out n);
        if (isNumeric)
        {
            ans = tagname;
        }
        else
        {
            if (isConnected)
            {
                try
                {
                    ans = vp.get_PVEStr(tagname);
                }
                catch
                {
                    Debug.Log("Couldn't find tag " + tagname);
                }
            }
        }
        return ans;
    }

    public int setPVE(string tagname, float defaultValue)
    {
        int ans = 0;
        if (isConnected)
        {
            ans = vp.get_SetPVE(tagname, defaultValue);
        }
        return ans;
    }

    public int setPV(string tagname, float defaultValue)
    {
        int ans = 0;
        if (isConnected)
        {
            ans = vp.get_SetPV(tagname, defaultValue);
        }
        return ans;
    }

    public string TagType(string tagname)
    {
        string ans = "";
        if (isConnected)
        {
            ans = vp.get_TagType(tagname);
        }
        return ans;
    }

    private void OnDestroy()
    {
        vp = null;
    }

    private void OnEnable()
    {
        StreamReader reader;

        string defaultPath = "Assets/Resources/VPValves.txt";
        string path = "C:/ProgramData/CapeSoftware/VPLink3/VR/VPValves.txt";
        string configPath = "C:/ProgramData/CapeSoftware/VPLink3/VR/VR_Config.txt";
        string tagFileName = "";
        string line;
        bool exists;
        
        exists = System.IO.File.Exists(configPath);
        if (exists)
        {
            reader = new StreamReader(configPath);
            while ((line = reader.ReadLine()) != null)
            {
                //line = line.Replace("\t", string.Empty);
                string[] fields = line.Split(',');
                //Debug.Log("Line=" + line + "; looking for " + equipmentName);

                char[] chars = fields[0].ToCharArray();
                if (!chars[0].Equals('#'))
                {
                    tagFileName = fields[0];
                }
            }

            path = "C:/ProgramData/CapeSoftware/VPLink3/VR/" + tagFileName;
        }
        
        exists = System.IO.File.Exists(path);
        if (exists)
        {
            reader = new StreamReader(path);
        }
        else
        {
            reader = new StreamReader(defaultPath);
        }

        while ((line = reader.ReadLine()) != null)
        {
            //line = line.Replace("\t", string.Empty);
            string[] fields = line.Split(',');
            //Debug.Log("Line=" + line + "; looking for " + equipmentName);

            char[] chars = fields[0].ToCharArray();
            if ((chars[0].Equals('#')) && (chars[1].Equals('!')))
            {
                theDatabase = fields[1];
            }
        }
        reader.Close();

        //#endif

        try
        {
            vp.Initialize("");
            knownDatabases = vp.ListVPDatabases();
            if (knownDatabases.Contains(theDatabase) || (theDatabase.Length == 0))
            {
                vp.Connect(theDatabase);
                isConnected = true;
                vp.EnableEvents(0);
            }
        }
        catch (System.Runtime.InteropServices.COMException ex)
        {
            Debug.LogError("Error during VP initialization: " + ex.Message);
        }
        catch
        {

        }

        BuildTagListDatabase();
    }

    // Use this for initialization
    void Start()
    {
        if (isConnected)
        {
            isConnectedIndicator.GetComponent<Text>().text = "VPLink Connected: Yes";
            //isConnectedIndicator.GetComponent<Text>().color = Color.green;
        }
        else
        {
            isConnectedIndicator.GetComponent<Text>().text = "VPLink Connected: No";
            //isConnectedIndicator.GetComponent<Text>().color = Color.red;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isConnected)
        {
            isConnectedIndicator.GetComponent<Text>().text = "VPLink Connected: Yes";
            isConnectedIndicatorVR.GetComponent<TextMesh>().text = "VPLink Connected: Yes";
            //isConnectedIndicator.GetComponent<Text>().color = Color.green;
        }
        else
        {
            isConnectedIndicator.GetComponent<Text>().text = "VPLink Connected: No";
            isConnectedIndicatorVR.GetComponent<TextMesh>().text = "VPLink Connected: No";
            //isConnectedIndicator.GetComponent<Text>().color = Color.red;
        }
    }

    void BuildTagListDatabase()
    {
        string tagList = vp.SearchTags("LFN_Flow", "algorithm");

        string[] tags = tagList.Split(',');

        //clean first space
        foreach(string tag in tags)
        {
            string tag1 = tag;
            if(tag[0] == ' ')
            {
                 tag1 = tag.Remove(0, 1);
            }

            string def = vp.Upload(tag1);

            string[] tokens = def.Split('\t');

            string tagParams = tokens[15];

            if(tagParams.Length > 0)
            {

                string[] theParams = tagParams.Split(',');

                string theVal = tag1 + "," + StripSpaces(theParams[0]) + "," + StripSpaces(theParams[1]) + ",";
                int n;
                bool isNumeric = int.TryParse(StripSpaces(theParams[2]), out n);

                if (isNumeric)
                {
                    int i = 6;
                    while (i < theParams.Length && StripSpaces(theParams[i])[0].Equals('$'))
                    {
                        i++;
                    }

                    if(i<theParams.Length)
                    {
                        theVal += StripSpaces(theParams[i]);
                    }
                }
                else
                {
                    theVal += StripSpaces(theParams[2]);
                }

                gTagList[tag1.ToUpper()] = theVal;
            }
        }


    }

    public string GetTagListEntry(string tag)
    {
        string ans = "";
        tag = tag.ToUpper();
        if(gTagList.ContainsKey(tag))
        {
            ans = gTagList[tag];
        }

        return ans;
    }

    private string StripSpaces(string s)
    {
        return s.Trim(' ');
    }
    */
    
}
