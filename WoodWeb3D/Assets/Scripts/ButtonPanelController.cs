﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonPanelController : MonoBehaviour {

    //public int[] numButtonsPerRow;
    public float topStopValue, bottomStopValue;
    
    private bool isMinimized = true;
    private bool isChanging = false;

    [HideInInspector]
    public PlayerController myPlayerScript;

    private GameObject pcButtonImage;
    private GameObject xboxButtonImage;

    private List<GameObject> myButtons = new List<GameObject>();

    private int xboxButtonIndex = 0;

    [HideInInspector]
    public bool isActive = false;

    private float menuStickTimeout = 0;

    // Use this for initialization
    void Start () {

        myPlayerScript = GameObject.Find("Player_MK").GetComponent<PlayerController>();
        pcButtonImage = transform.Find("TabImage").Find("KeyImage").gameObject;
        xboxButtonImage = transform.Find("TabImage").Find("XboxImage").gameObject;

        xboxButtonImage.SetActive(false);


        foreach (Transform child in transform)
        {
            if(child.tag.Equals("ActionButton"))
            {
                myButtons.Add(child.gameObject);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {

        UpdateTimers(Time.deltaTime);

        if(myPlayerScript.isUsingController)
        {
            pcButtonImage.SetActive(false);
            xboxButtonImage.SetActive(true);
        }
        else
        {
            pcButtonImage.SetActive(true);
            xboxButtonImage.SetActive(false);
        }
        

        if((!myPlayerScript.isUsingController && Input.GetKeyDown(KeyCode.R)) || (myPlayerScript.isUsingController && (Input.GetKeyDown("joystick button 3") || (Input.GetKeyDown("joystick button 1") && myPlayerScript.inActionMenu))))
        {
            if (!myPlayerScript.inCursorMode /*&& !myPlayerScript.inActionMenu*/ && !myPlayerScript.inPauseMenu && !myPlayerScript.inOptionsMenu && !myPlayerScript.isThirdPerson && !myPlayerScript.inControlsMenu)
            {
                isChanging = true;
                if (isMinimized)
                {
                    myPlayerScript.inActionMenu = true;
                    isActive = true;
                }
                else
                {
                    myPlayerScript.inActionMenu = false;
                    isActive = false;
                }
            }
        }

        if(isChanging)
        {
            if(isMinimized)
            {
                transform.GetComponent<RectTransform>().position = new Vector3(transform.GetComponent<RectTransform>().position.x, transform.GetComponent<RectTransform>().position.y + 50f, transform.GetComponent<RectTransform>().position.z);
                //Debug.Log(transform.GetComponent<RectTransform>().sizeDelta.y);
                if (transform.GetComponent<RectTransform>().position.y >= topStopValue)
                {
                    isChanging = false;
                    isMinimized = false;
                    transform.GetComponent<RectTransform>().position = new Vector3(transform.GetComponent<RectTransform>().position.x,  topStopValue, transform.GetComponent<RectTransform>().position.z);
                    //isActive = true;
                }
            }
            else
            {
                transform.GetComponent<RectTransform>().position = new Vector3(transform.GetComponent<RectTransform>().position.x, transform.GetComponent<RectTransform>().position.y - 50f, transform.GetComponent<RectTransform>().position.z);
                if (transform.GetComponent<RectTransform>().position.y <= bottomStopValue)
                {
                    isChanging = false;
                    isMinimized = true;
                    transform.GetComponent<RectTransform>().position = new Vector3(transform.GetComponent<RectTransform>().position.x, bottomStopValue, transform.GetComponent<RectTransform>().position.z);
                    //isActive = false;
                }
            }
        }

        if(isActive)
        {
            if (myPlayerScript.isUsingController)
            {
                if(((Input.GetAxis("Xbox_Dpad_Horizontal") < 0) || (Input.GetAxis("Xbox_LS_Horizontal") < 0)))
                {
                    if (menuStickTimeout > .2f)
                    {
                        xboxButtonIndex--;
                        if (xboxButtonIndex < 0)
                        {
                            xboxButtonIndex = myButtons.Count - 1;
                        }
                        menuStickTimeout = 0;
                        GetComponent<AudioSource>().Play();
                    }
                }
                else if ((Input.GetAxis("Xbox_Dpad_Horizontal") > 0) || (Input.GetAxis("Xbox_LS_Horizontal") > 0))
                {
                    if (menuStickTimeout > .2f)
                    {
                        xboxButtonIndex++;
                        if (xboxButtonIndex >= myButtons.Count)
                        {
                            xboxButtonIndex = 0;
                        }
                        menuStickTimeout = 0;
                        GetComponent<AudioSource>().Play();
                    }
                }
                else
                {
                    menuStickTimeout = 1;
                }

                for (int x = 0; x < myButtons.Count; x++)
                {
                    if(x == xboxButtonIndex)
                    {
                        myButtons[x].transform.GetComponent<Image>().color = new Color(1,1,0);
                    }
                    else
                    {
                        myButtons[x].transform.GetComponent<Image>().color = new Color(1, 1, 1);
                    }
                }
                
                if(Input.GetKeyDown("joystick button 0"))
                {
                    myButtons[xboxButtonIndex].GetComponent<Button>().onClick.Invoke();
                    isMinimized = false;
                    isChanging = true;
                    isActive = false;
                    myPlayerScript.inActionMenu = false;
                }

            }
            else
            {
                for (int x = 0; x < myButtons.Count; x++)
                {
                    myButtons[x].transform.GetComponent<Image>().color = new Color(1, 1, 1);
                }
            }
        }
        else
        {

        }

    }

    public void MinWindow()
    {
        if (!isMinimized)
        {
            isChanging = true;
        }
    }

    void UpdateTimers(float deltaT)
    {
        menuStickTimeout += deltaT;
    }
}
