﻿/*
 * ****************************************************************
 * PlayerController is a script to control the Non-Virtual-Reality Player
 * Uses Mouse, Keyboard, and Xbox input to control player
 * Has all methods invoked by the buttons in the UI to control the scene
 ********************************************************************
*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.IO;


public class PlayerController : MonoBehaviour
{
    // This region contains all variables that are public and SET IN THE INSPECTOR
    #region Public Inpector Values

    // isFlying will change the way we do our locomotion whether it's true or false. False uses gravity, true doesn't
    public bool isFlying, startAsDay;

    // These 5 floats control our locomotion speed, and our camera rotation sensitivity
    public float cameraSensitivity = 90;
    public float climbSpeed = 5;
    public float normalMoveSpeed = 10;
    public float slowMoveFactor = 0.25f;
    public float fastMoveFactor = 3;

    public Vector2 coordinateOffset;

    public float depthOffset;
    // references our Lights that are on at day to toggle between day and night
    public List<GameObject> Day_Lights_List;

    // references our Lights that are on at night to toggle between day and night
    public List<GameObject> Night_Lights_List;

    // references our ROV object to toggle between 'ROV Third Person' Mode
    public GameObject ROV;

    // reference to our measure points material that we use to measure from - to. Measure from measurePointObj_0 to measurePointObj_1
    public GameObject measureDistancePrefab;
    public List<GameObject> measureDistancePrefabs;
    public GameObject measureAnglePrefab;
    public List<GameObject> measureAnglePrefabs;

    // reference to our outline material that we use to highlight objects when they are selected. Set in inpector
    public GameObject coordinateRefObject;

    // reference to our outline material that we use to highlight objects when they are selected. Set in inpector
    public Material outlineMat;

    public GameObject UnderwaterFogVolume, DayPP_Profile, NightPP_Profile, ROVPP_Profile, LayoutDrawing;

    #endregion

    // This region contains all variables that are public but HIDDEN FROM THE INSPECTOR
    // These are usually objects that have this as a parent, but I also want them public for others to manipulate
    #region Public Hidden Values

    // the selectedObject variable keeps a reference to the object we currently have selected. NULL if no object is selected
    [HideInInspector]
    public GameObject selectedObject;

    // the selectedScrollbar variable keeps a reference to the scrollbar we currently have selected.
    // Needed to allow xbox controller to scroll
    [HideInInspector]
    public GameObject selectedScrollbar;

    [HideInInspector]
    public GameObject ObjectDataBox;

    [HideInInspector]
    public GameObject AssetListBox;

    [HideInInspector]
    public GameObject VPLinkDataBox;

    [HideInInspector]
    public bool canClimb;

    [HideInInspector]
    public float ladderTimeout;

    [HideInInspector]
    public Camera myCam;

    [HideInInspector]
    public bool isOrthoMode = false, isThirdPerson = false, inControlsMenu = false, inCursorMode = false, inPauseMenu = false, inActionMenu = false, inOptionsMenu = false, isIndicating = false, isUsingController = false;

    [HideInInspector]
    public float mouseHeldTimer = 0;

    [HideInInspector]
    public int qualityIndex = 0;
    #endregion

    // This region contains all variables that are private
    #region Private Variables

    private Text xCoordinateText, yCoordinateText, depthText, zText, IndicatorText;

    private Button selectedButton;

    private float rotationX = 0.0f, rotationY = 0.0f, cameraTimer = 100.0f, vibrateTimer = 5, vibrateTime = 0, prevAngle, angle, depth, hasSelectedTimer,
        vplinkUpdateTimer, changeSceneTimer = 0;

    private int animIndex = 0;

    private GameObject Player_MK_UI, HUD, HoveredPanel, RadialDial, ControlsMenu, PauseMenu, ButtonPanel, vpLink_AnalogValve_ButtonPanel, vpLink_DigitalValve_ButtonPanel,
        vpLink_DigitalPump_ButtonPanel, IndicatorMenu, OptionsMenu, myMaster, HeadingText,
        LeftPanel, RightPanel, CursorModeImage, waterSurfaceObj, prevObjectPrefab;


    private bool isDay = false, isIndicatingVisible, isScrollingVert, isScrollingHoriz, isMeasuringDistance1stPoint = false, isMeasuringDistance2ndPoint = false, 
        hasMeasuredDistance = false, isMeasuringDistance = false, isMeasuringAngle1stPoint = false, isMeasuringAngle2ndPoint = false,
        isMeasuringAngle3rdPoint = false, isMeasuringAngle4thPoint = false, isMeasuringAngle = false, hasMeasuredAngle = false, isTakingPicture = false, 
        hasSelected = false, isDpadDown = false, isDpadClicked;

    private Vector3 lastMousePos, measurePoint0, measurePoint1, prevMousePos, prevXboxKnobPos, prevObjectPosition, originalRigidBodyRot;

    private VPDatabase vpd;

    private Quaternion prevObjectRotation;

    private Vector2 startingScreenSize;

    #endregion

    #region Constant Variables

    const int KUnityNameCol = 0;
    const int KFlowCol = 1;
    const int KUpstreamPressureCol = 2;
    const int KDownstreamPressureCol = 3;
    const int KValvePosCol = 4;

    #endregion

    List<GameObject> vpLinkValues = new List<GameObject>();

    void Start()
    {

        // set our rotationX variable to what it is in the scene to start so that it doesn't start at 0 automatically
        //rotationX = transform.eulerAngles.y;

        // Find our master object
        myMaster = GameObject.Find("Master").gameObject;

        // Keep our rigidBody's original rotation in originalRigidBodyRot so that we can keep the rigidBody's rotation from rotating with parent.
        originalRigidBodyRot = transform.GetComponent<Rigidbody>().transform.eulerAngles;

        // no vertical sync, fps = 60, lock our cursor
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
        Screen.lockCursor = true;

        // Find our VPLink Database script
        vpd = FindObjectOfType<VPDatabase>();
        if (vpd == null)
        {
            Debug.LogError("Cannot find VP Link database in the VerMovement object");
        }

        // Find our UI
        Player_MK_UI = GameObject.Find("Player_MK_UI");

        // Find/Set our menu objects from Player_MK_UI object
        PauseMenu = Player_MK_UI.transform.Find("PauseMenu").gameObject;
        OptionsMenu = Player_MK_UI.transform.Find("OptionsMenu").gameObject;
        ControlsMenu = Player_MK_UI.transform.Find("ControlsMenu").gameObject;
        IndicatorMenu = Player_MK_UI.transform.Find("IndicatorMenu").gameObject;
        IndicatorText = IndicatorMenu.transform.Find("IndicatorText").GetComponent<Text>();

        // Find our cursor mode background dim image
        CursorModeImage = Player_MK_UI.transform.Find("CursorModeImage").gameObject;
        CursorModeImage.SetActive(false);

        // Find/Set our HUD from the Player_MK_UI object
        HUD = Player_MK_UI.transform.Find("HUD").gameObject;

        // Find/Set our panels and texts from the HUD object
        HoveredPanel = HUD.transform.Find("HoveredPanel").gameObject;
        LeftPanel = HUD.transform.Find("LeftPanel").gameObject;
        RightPanel = HUD.transform.Find("RightPanel").gameObject;
        ButtonPanel = HUD.transform.Find("ButtonPanel").gameObject;
        vpLink_AnalogValve_ButtonPanel = HUD.transform.Find("VPLink_AnalogValve_ButtonPanel").gameObject;
        vpLink_DigitalValve_ButtonPanel = HUD.transform.Find("VPLink_DigitalValve_ButtonPanel").gameObject;
        vpLink_DigitalPump_ButtonPanel = HUD.transform.Find("VPLink_DigitalPump_ButtonPanel").gameObject;
        RadialDial = LeftPanel.transform.Find("RadialDial").gameObject;
        HeadingText = HUD.transform.Find("LeftPanel").Find("HeadingText").gameObject;
        xCoordinateText = HUD.transform.Find("LeftPanel").Find("xText").GetComponent<Text>();
        yCoordinateText = HUD.transform.Find("LeftPanel").Find("yText").GetComponent<Text>();
        zText = HUD.transform.Find("LeftPanel").Find("zText").GetComponent<Text>();

        // Find/Set our player position texts from the LeftPanel object
        xCoordinateText = LeftPanel.transform.Find("xText").GetComponent<Text>();
        yCoordinateText = LeftPanel.transform.Find("yText").GetComponent<Text>();
        depthText = LeftPanel.transform.Find("dText").GetComponent<Text>();
        ObjectDataBox = LeftPanel.transform.Find("AssetInfoPanel").Find("ObjectDataBox").gameObject;
        AssetListBox = LeftPanel.transform.Find("AssetInfoPanel").Find("AssetListBox").gameObject;
        VPLinkDataBox = LeftPanel.transform.Find("VPLink_DataBox").gameObject;


        // Set all menus to OFF
        OptionsMenu.SetActive(false);
        PauseMenu.SetActive(false);
        IndicatorMenu.SetActive(false);
        isIndicating = false;
        isIndicatingVisible = true;

        // Find/Set my Camera
        myCam = transform.Find("Player_Camera").GetComponent<Camera>();

        // Find/Set Water Surface Object
        waterSurfaceObj = GameObject.Find("WaterSurface").gameObject;

        // initialize our lights
        isDay = !startAsDay;
        ToggleDayNight();

        startingScreenSize = new Vector2(Screen.width, Screen.height);

        if (myMaster.GetComponent<MasterController>().isVPLink)
        {
            LeftPanel.transform.Find("AssetInfoPanel").gameObject.SetActive(false);
            //LeftPanel.transform.Find("FrameImage").gameObject.SetActive(false);
            //LeftPanel.transform.Find("Viewer Coordinates Text").gameObject.SetActive(false);
            RadialDial.SetActive(false);
            HeadingText.SetActive(false);
            xCoordinateText.gameObject.SetActive(false);
            yCoordinateText.gameObject.SetActive(false);
            zText.gameObject.SetActive(false);
            xCoordinateText.gameObject.SetActive(false);
            yCoordinateText.gameObject.SetActive(false);
            depthText.gameObject.SetActive(false);
            ButtonPanel.SetActive(false);
            vpLink_DigitalPump_ButtonPanel.SetActive(false);
            vpLink_DigitalValve_ButtonPanel.SetActive(false);
        }
        else
        {
            LeftPanel.transform.Find("Cape_Logo").gameObject.SetActive(false);
            VPLinkDataBox.SetActive(false);
            vpLink_AnalogValve_ButtonPanel.SetActive(false);
            vpLink_DigitalPump_ButtonPanel.SetActive(false);
            vpLink_DigitalValve_ButtonPanel.SetActive(false);
        }

        qualityIndex = QualitySettings.GetQualityLevel();
    }

    void Update()
    {
        // Invoke our Always Update At Beginning Method
        AlwaysUpdateAtBeginning();

        // This if/else region controls what update method I invoke every frame
        // based off booleans I know what state the game is currently in, therefor knowing what to update
        // if the player isn't in any menus, invoke the in game method
        if (!inCursorMode && !inActionMenu && !inPauseMenu && !inOptionsMenu && !isThirdPerson && !inControlsMenu)
        {
            //Debug.Log("In Game Mode" + Time.deltaTime);

            // update our player
            UpdateInGamePlayer();

            if (isOrthoMode)
            {
                // hide and lock the cursor in place
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                // hide and lock the cursor in place
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }


            // make sure our image (that dims the background when in cursor mode) is OFF.
            CursorModeImage.SetActive(false);

            // Hide our HUD's XboxKnob because we are not in cursor mode
            HUD.transform.Find("XboxKnob").gameObject.SetActive(false);

            #region Update Cursor Mode

            if (Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown("joystick button 6"))
            {
                inCursorMode = !inCursorMode;
            }

            #endregion
        }
        else if (inControlsMenu)
        {
            //Debug.Log("In Controls Menu" + Time.deltaTime);
            UpdateControlsMenu();

            // show and un-lock the cursor so that it can move
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else if (isThirdPerson)
        {
            //Debug.Log("In Third Person" + Time.deltaTime);
            // invoke our ROV's update method here in our player' script
            ROV.GetComponent<ThirdPersonController>().UpdateThirdPersonMode();
        }
        else if (inPauseMenu)
        {
            //Debug.Log("In Pause Menu" + Time.deltaTime);
            UpdatePauseMenu();

            // show and un-lock the cursor so that it can move
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else if (inOptionsMenu)
        {
            //Debug.Log("In Options Menu" + Time.deltaTime);
            UpdateOptionsMenu();

            // show and un-lock the cursor so that it can move
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else if (inActionMenu)
        {
            if (!isUsingController)
            {
                // show and un-lock the cursor so that it can move
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                // show and un-lock the cursor so that it can move
                //Cursor.visible = false;
                //Cursor.visible = true;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
        else if (inCursorMode)
        {
            //Debug.Log("In Cursor Menu" + Time.deltaTime);
            UpdateCursorMode();

            // show and un-lock the cursor so that it can move
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

            // make sure our image (that dims the background when in cursor mode) is ON.
            CursorModeImage.SetActive(true);

            #region Update Cursor Mode

            if (Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown("joystick button 6") || Input.GetKeyDown("joystick button 1"))
            {
                inCursorMode = !inCursorMode;
            }

            #endregion
        }

        // Invoke our Always Update At Beginning Method
        AlwaysUpdateAtEnd();



    }

    #region Update Methods

    void AlwaysUpdateAtBeginning()
    {

        #region Update Compass

        // Controlling the Compass Bar
        Vector3 dir = myCam.transform.forward;

        //set our y variable to 0, because we are only interested in our horizontal direction
        dir.y = 0;
        dir.Normalize();

        // get our angle based off the inverse tangent of our horizontal vectors. Convert to Degrees
        angle = Mathf.Atan2(dir.x, dir.z) * 57.2958f;

        // if the angle is below zero, add 360 to it to make it equal it's positive counterpart
        if (angle < 0)
        {
            angle += 360;
        }

        // Update our heading Text
        HeadingText.GetComponent<Text>().text = angle.ToString("N0") + "°";

        //update our prevAngle variable
        prevAngle = angle;

        #endregion

        #region Update Depth

        // the depth is just the difference between the y positions of our water surface object (set in inspector), and the players y position
        // the water surface object should be placed on an object in the scene in which we know that object's depth
        depth = waterSurfaceObj.transform.position.y - myCam.transform.position.y;
        depth *= 3.28084f;

        //depth offset(basically if we put the water surface object on a subsea tree, we want this to equal that tree's depth)
        depth += depthOffset;
        if (depth <= 0)
        {
            depth = depth * -1;
            string dString = depth.ToString("0");
            depthText.text = "D = (+)" + dString + " ft";

            // if we're using the fog volume, then turn it off because we are above the water
            if (UnderwaterFogVolume != null)
            {
                if (UnderwaterFogVolume.activeSelf)
                {
                    UnderwaterFogVolume.SetActive(false);
                }
            }
        }
        else
        {
            string dString = depth.ToString("0");
            depthText.text = "D = (-)" + dString + " ft";

            // if we're using the fog volume, then turn it on because we are below the water
            if (UnderwaterFogVolume != null)
            {
                if (!UnderwaterFogVolume.activeSelf)
                {
                    UnderwaterFogVolume.SetActive(true);
                }
            }
        }

        #endregion

        #region Update Zoom

        //this updates the text(named zText) in the upper left corner that displays our zoom %
        int zoomAmount = (int)(6000 / myCam.fieldOfView);

        zText.GetComponent<Text>().text = "Zoom = " + zoomAmount + "%";

        #endregion

        #region Update if player is using Keyboard or Xbox
        //these methods constantly check for inputs from keyboard/mouse or xbox
        isMouseKeyboard();

        isControlerInput();

        #endregion

        #region Update Northing/Easting Text
        //to update our easting and northing, we ujst subtract our position from the coordinate reference object position, then add our offset that we get from a drawing.
        // the coordinate reference object should be placed on an object in the scene in which we know that object's real world coordinates.
        Vector2 offset = new Vector2(transform.position.x - coordinateRefObject.transform.position.x, transform.position.z - coordinateRefObject.transform.position.z);
        xCoordinateText.text = "X = " + (coordinateOffset.x + ((offset.x) * 3.28084f));
        yCoordinateText.text = "Y = " + (coordinateOffset.y + ((offset.y) * 3.28084f));

        #endregion

        #region Update Xbox Controller Vibration
        /*
        //if the vibate timer has surpassed the virbrate time, turn off vibration
        if (vibrateTimer > vibrateTime)
        {
            GamePad.SetVibration(0, 0, 0);
        }
        */
        #endregion

        #region Update Picture Taking

        if (isTakingPicture)
        {
            //if the cameraTimer is below 5 sceonds then all we do is update the text
            if (cameraTimer < 4)
            {
                IndicatorText.text = "Picture in : " + (int)(5 - cameraTimer);
            }
            else
            {
                //Take the picture
                isTakingPicture = false;
                isIndicating = false;
                IndicatorMenu.SetActive(false);
            }

        }

        #endregion

        #region Update Action Indication

        //isIndicating is true if we're in a current action(for example moving, rotating, measureing, etc.)
        //It's indicating to tell what the user is currently doing
        //The text is set in methods where the action is invoked
        if (isIndicating)
        {
            //if the indicator menu is off, turn it on
            if (!IndicatorMenu.activeSelf)
            {
                IndicatorMenu.SetActive(true);
            }

            // this if/else statement makes the image/text blink basically
            // if isIndicatingVisible is true, we decrease the alpha. otherwise we increase it.
            // isIndicatingVisible will basically toggle when our alpa value is bloew .1 or above .96
            if (isIndicatingVisible)
            {
                Color myColor = IndicatorMenu.GetComponent<Image>().color;
                IndicatorMenu.GetComponent<Image>().color = new Color(1, 1, 1, myColor.a - .033f);
                //IndicatorText.color = new Color(0, 0, 0, myColor.a - .02f);

                if (IndicatorMenu.GetComponent<Image>().color.a < .1f)
                {
                    isIndicatingVisible = false;
                }
            }
            else
            {
                Color myColor = IndicatorMenu.GetComponent<Image>().color;
                IndicatorMenu.GetComponent<Image>().color = new Color(myColor.r, myColor.g, myColor.b, myColor.a + .033f);
                //IndicatorText.color = new Color(0, 0, 0, myColor.a + .02f);
                if (IndicatorMenu.GetComponent<Image>().color.a > .96f)
                {
                    isIndicatingVisible = true;
                }
            }
        }
        else
        {
            if (IndicatorMenu.activeSelf)
            {
                IndicatorMenu.SetActive(false);
            }
        }

        #endregion

        #region Measure Line Texts Control

        


        float zoom = (60.0f / myCam.fieldOfView);

        #region Control Text Size

        //these if else statements just control the size of the measure lines' texts
        //hypotenuseTextMesh.characterSize = 12 * (Vector3.Distance(hypotenuseTextMesh.transform.position, myCam.transform.position) / 30) / zoom;

        //if (hypotenuseTextMesh.characterSize > 10f)
        //{
        //    hypotenuseTextMesh.characterSize = 10f;
        //}

        //verticalTextMesh.characterSize = 12 * (Vector3.Distance(verticalTextMesh.transform.position, myCam.transform.position) / 30) / zoom;

        //if (verticalTextMesh.characterSize > 10f)
        //{
        //    verticalTextMesh.characterSize = 10f;
        //}

        //horizontalTextMesh.characterSize = 12 * (Vector3.Distance(horizontalTextMesh.transform.position, myCam.transform.position) / 30) / zoom;

        //if (horizontalTextMesh.characterSize > 10f)
        //{
        //    horizontalTextMesh.characterSize = 10f;
        //}
        #endregion

        #region Control Text Visibility

        //these if/else statement will turn off the veritcal and horizontal lines if there is suspected to be overlap
        //the hypotenuse will always stay on
        //if (Vector3.Distance(hypotenuseTextMesh.transform.position, horizontalTextMesh.transform.position) < hypotenuseTextMesh.characterSize/5.0f)
        //{
        //    horizontalTextMesh.transform.GetComponent<MeshRenderer>().enabled = false;
        //}
        //else
        //{
        //    horizontalTextMesh.transform.GetComponent<MeshRenderer>().enabled = true;
        //}

        //if (Vector3.Distance(hypotenuseTextMesh.transform.position, verticalTextMesh.transform.position) < hypotenuseTextMesh.characterSize / 5.0f)
        //{
        //    verticalTextMesh.transform.GetComponent<MeshRenderer>().enabled = false;
        //}
        //else
        //{
        //    verticalTextMesh.transform.GetComponent<MeshRenderer>().enabled = true;
        //}

        #endregion

        #endregion

        #region Vplink Position Control

        vplinkUpdateTimer += Time.deltaTime;

        // if it's time to update to vplink
        if (vplinkUpdateTimer >= 1)
        {
            if (vpd != null)
            {
                string player = "Player1";

                /*
                //Check to see if the tagtype exists
                if (vpd.TagType("VRLocation_" + player + "_X").Length > 0)
                {
                    // Update the vplink player position using our camera position
                    // "VRLocation_Player1_X" is a vplink tagname for example
                    vpd.setPVE("VRLocation_" + player + "_X", myCam.transform.position.x);
                    vpd.setPVE("VRLocation_" + player + "_Y", myCam.transform.position.y);
                    vpd.setPVE("VRLocation_" + player + "_Z", myCam.transform.position.z);
                    vpd.setPVE("VRLocation_" + player + "_Heading", myCam.transform.eulerAngles.y);
                }
                */
            }
        }

        #endregion

        #region Update Valve/Pump Info
        /*
        // if the file is a vplink file
        if (myMaster.GetComponent<MasterController>().isVPLink)
        {
            // if we have an object selected
            if (selectedObject != null)
            {
                // if the object has correct tagStrings
                if (selectedObject.GetComponent<SelectableController>().tagStrings.Length >= 4)
                {
                    //if the object is a valve
                    if (selectedObject.GetComponent<SelectableController>().isValve)
                    {
                        //if the object is a Digital Valve
                        if (selectedObject.GetComponent<SelectableController>().myValve.GetComponent<ValveLerper>().isLever)
                        {
                            //activate our Digital Valve button panel
                            //deactivate all other button panels
                            if (!vpLink_DigitalValve_ButtonPanel.activeSelf)
                            {
                                vpLink_DigitalValve_ButtonPanel.SetActive(true);
                                vpLink_AnalogValve_ButtonPanel.SetActive(false);
                                vpLink_DigitalPump_ButtonPanel.SetActive(false);
                                ButtonPanel.SetActive(false);
                            }
                            // update our vplink values from the vplink database
                            vpLinkValues[0].GetComponent<Text>().text = "Flow Through Valve: " + vpd.PVEStr(selectedObject.GetComponent<SelectableController>().tagStrings[KFlowCol], "?? " + selectedObject.GetComponent<SelectableController>().tagStrings[KFlowCol] + " ??");
                            vpLinkValues[1].GetComponent<Text>().text = "Upstream Pressure: " + vpd.PVEStr(selectedObject.GetComponent<SelectableController>().tagStrings[KUpstreamPressureCol], "?? " + selectedObject.GetComponent<SelectableController>().tagStrings[KUpstreamPressureCol] + " ??");
                            vpLinkValues[2].GetComponent<Text>().text = "Downstream Pressure: " + vpd.PVEStr(selectedObject.GetComponent<SelectableController>().tagStrings[KDownstreamPressureCol], "?? " + selectedObject.GetComponent<SelectableController>().tagStrings[KDownstreamPressureCol] + " ??");
                            vpLinkValues[3].GetComponent<Text>().text = "Valve Status:       " + vpd.PVEStr(selectedObject.GetComponent<SelectableController>().tagStrings[KValvePosCol], "?? " + selectedObject.GetComponent<SelectableController>().tagStrings[KValvePosCol] + " ??");
                        }
                        else
                        {
                            //activate our Analog Valve button panel
                            //deactivate all other button panels
                            if (!vpLink_AnalogValve_ButtonPanel.activeSelf)
                            {
                                vpLink_DigitalValve_ButtonPanel.SetActive(false);
                                vpLink_AnalogValve_ButtonPanel.SetActive(true);
                                vpLink_DigitalPump_ButtonPanel.SetActive(false);
                                ButtonPanel.SetActive(false);
                            }
                            // update our vplink values from the vplink database
                            vpLinkValues[0].GetComponent<Text>().text = "Flow Through Valve: " + vpd.PVEStr(selectedObject.GetComponent<SelectableController>().tagStrings[KFlowCol], "?? " + selectedObject.GetComponent<SelectableController>().tagStrings[KFlowCol] + " ??");
                            vpLinkValues[1].GetComponent<Text>().text = "Upstream Pressure: " + vpd.PVEStr(selectedObject.GetComponent<SelectableController>().tagStrings[KUpstreamPressureCol], "?? " + selectedObject.GetComponent<SelectableController>().tagStrings[KUpstreamPressureCol] + " ??");
                            vpLinkValues[2].GetComponent<Text>().text = "Downstream Pressure: " + vpd.PVEStr(selectedObject.GetComponent<SelectableController>().tagStrings[KDownstreamPressureCol], "?? " + selectedObject.GetComponent<SelectableController>().tagStrings[KDownstreamPressureCol] + " ??");
                            vpLinkValues[3].GetComponent<Text>().text = "Percent Open: " + vpd.PVEStr(selectedObject.GetComponent<SelectableController>().tagStrings[KValvePosCol], "?? " + selectedObject.GetComponent<SelectableController>().tagStrings[KValvePosCol] + " ??");
                        }
                    }
                    else
                    {
                        //activate our Digital Pump button panel
                        //deactivate all other button panels
                        if (!vpLink_DigitalPump_ButtonPanel.activeSelf)
                        {
                            vpLink_DigitalValve_ButtonPanel.SetActive(false);
                            vpLink_AnalogValve_ButtonPanel.SetActive(false);
                            vpLink_DigitalPump_ButtonPanel.SetActive(true);
                            ButtonPanel.SetActive(false);
                        }
                        // update our vplink values from the vplink database
                        vpLinkValues[0].GetComponent<Text>().text = "Flow Through Pump: " + vpd.PVEStr(selectedObject.GetComponent<SelectableController>().tagStrings[KFlowCol], "?? " + selectedObject.GetComponent<SelectableController>().tagStrings[KFlowCol] + " ??");
                        vpLinkValues[1].GetComponent<Text>().text = "Inlet Pressure: " + vpd.PVEStr(selectedObject.GetComponent<SelectableController>().tagStrings[KUpstreamPressureCol], "?? " + selectedObject.GetComponent<SelectableController>().tagStrings[KUpstreamPressureCol] + " ??");
                        vpLinkValues[2].GetComponent<Text>().text = "Discharge Pressure: " + vpd.PVEStr(selectedObject.GetComponent<SelectableController>().tagStrings[KDownstreamPressureCol], "?? " + selectedObject.GetComponent<SelectableController>().tagStrings[KDownstreamPressureCol] + " ??");
                        vpLinkValues[3].GetComponent<Text>().text = "Pump Status:      " + vpd.PVEStr(selectedObject.GetComponent<SelectableController>().tagStrings[KValvePosCol], "?? " + selectedObject.GetComponent<SelectableController>().tagStrings[KValvePosCol] + " ??");
                    }
                }
            }
            else
            {
                //if no object is selected, we default to our normal button panel
                vpLink_DigitalValve_ButtonPanel.SetActive(false);
                vpLink_AnalogValve_ButtonPanel.SetActive(false);
                vpLink_DigitalPump_ButtonPanel.SetActive(false);
                ButtonPanel.SetActive(true);
            }
        }
        */
        #endregion

        //lock our rigidbody rotation
        transform.GetComponent<Rigidbody>().transform.eulerAngles = originalRigidBodyRot;

        //update all of our timers
        UpdateTimers(Time.deltaTime);
    }

    void AlwaysUpdateAtEnd()
    {
        //update our mouse pos
        prevMousePos = Input.mousePosition;

        //rotate my measurePointObjs (just for looks)
        foreach (GameObject measurement in measureDistancePrefabs)
        {
            if (measureDistancePrefabs.Count > 0)
            {
                measurement.transform.Find("MeasurePointIcon0").Rotate(2, 0, 0);
                measurement.transform.Find("MeasurePointIcon1").Rotate(2, 0, 0);
            }
        }
        foreach (GameObject measurement in measureAnglePrefabs)
        {
            if (measureAnglePrefabs.Count > 0)
            {
                measurement.transform.Find("MeasurePointIcon0").Rotate(2, 0, 0);
                measurement.transform.Find("MeasurePointIcon1").Rotate(2, 0, 0);
                measurement.transform.Find("MeasurePointIcon2").Rotate(2, 0, 0);
                measurement.transform.Find("MeasurePointIcon3").Rotate(2, 0, 0);
                measurement.transform.Find("ProjectionTrace0").GetComponent<LineRenderer>().material.mainTextureOffset = new Vector2(measurement.transform.Find("ProjectionTrace0").GetComponent<LineRenderer>().material.mainTextureOffset.x + -4 * Time.deltaTime, 0);
                measurement.transform.Find("ProjectionTrace1").GetComponent<LineRenderer>().material.mainTextureOffset = new Vector2(measurement.transform.Find("ProjectionTrace1").GetComponent<LineRenderer>().material.mainTextureOffset.x + -4 * Time.deltaTime, 0);
                measurement.transform.Find("ProjectionTrace2").GetComponent<LineRenderer>().material.mainTextureOffset = new Vector2(measurement.transform.Find("ProjectionTrace2").GetComponent<LineRenderer>().material.mainTextureOffset.x + -4 * Time.deltaTime, 0);
                measurement.transform.Find("ProjectionTrace3").GetComponent<LineRenderer>().material.mainTextureOffset = new Vector2(measurement.transform.Find("ProjectionTrace3").GetComponent<LineRenderer>().material.mainTextureOffset.x + -4 * Time.deltaTime, 0);
            }
        }
    }

    void UpdateTimers(float deltaTime)
    {
        //update all of our timers
        cameraTimer += deltaTime;
        ladderTimeout += deltaTime;
        vibrateTimer += deltaTime;
        hasSelectedTimer += deltaTime;
    }

    void UpdateInGamePlayer()
    {
        #region Selecting

        // the only layer from which we can select from is layer 10, a.k.a "Selectable"
        int mask = 10;
        int layermask = 1 << mask;

        RaycastHit rayHit;
        Ray hoverRay = new Ray();
        if (isOrthoMode)
        {
            hoverRay = myCam.ScreenPointToRay(Input.mousePosition);
        }
        else
        {
            hoverRay = new Ray(transform.position, myCam.transform.forward);
        }



        //if we hit an object
        if (Physics.Raycast(hoverRay, out rayHit, 9999999.0f, layermask))
        {
            // update our hovered panel to tell us what we're looking at
            HoveredPanel.SetActive(true);
            HoveredPanel.transform.Find("HoveredText").GetComponent<Text>().text = rayHit.transform.name;
        }
        else
        {
            HoveredPanel.SetActive(false);
        }

        // if user pressed left mouse button
        if (Input.GetMouseButton(0))
        {
            //increase our mouse held timer
            mouseHeldTimer += Time.deltaTime;
        }
        else
        {
            //reset our mouse held timer
            mouseHeldTimer = 0.0f;
        }

        // if user pressed left mouse button(xbox a button) AND isn't measuring
        if ((Input.GetMouseButtonDown(0) || Input.GetKeyDown("joystick button 0")) && !isMeasuringDistance)
        {
            // the only layer from which we can select from is layer 10, a.k.a "Selectable"
            int lay = 10;
            int layermask1 = 1 << lay;

            RaycastHit hit;
            Ray ray = new Ray();
            if (isOrthoMode)
            {
                ray = myCam.ScreenPointToRay(Input.mousePosition);
            }
            else
            {
                ray = new Ray(transform.position, myCam.transform.forward);
            }

            //if we clicked on an object in the "Selectable" layer
            if (Physics.Raycast(ray, out hit, 9999999.0f, layermask1))
            {
                //if the object isn't already selected
                if (selectedObject != hit.transform.gameObject)
                {
                    // un-highlight old selected object if there is one
                    if (selectedObject != null)
                    {
                        paintColours(true);
                    }

                    hasSelected = true;
                    hasSelectedTimer = 0;

                    //if the object itself is VRGRabbable
                    if (hit.transform.GetComponent<SelectableController>() != null)
                    {
                        selectedObject = hit.transform.gameObject;
                    }
                    //if the object is a collider proxy and it's parent is VRGRabbable(this is used so that we can have multiple collider boxes to outline anything)
                    else if (hit.transform.parent.GetComponent<SelectableController>() != null && hit.transform.name.Equals("ProxyCube"))
                    {
                        selectedObject = hit.transform.parent.gameObject;
                    }
                    //set our outline material's width based off of the selected objects SelectableController outlineWidth variable
                    outlineMat.SetFloat("_Outline", selectedObject.transform.GetComponent<SelectableController>().outlineWidth);

                    // highlight new selected object
                    paintColours(false);

                    //vibrate our controller
                    //GamePad.SetVibration(0, .5f, .5f);
                    vibrateTimer = 0;
                    vibrateTime = .05f;

                    //play select sound
                    transform.GetComponent<AudioSource>().Play();

                    //if this is a vplink file
                    if (myMaster.GetComponent<MasterController>().isVPLink)
                    {
                        //clear our vplink values list
                        vpLinkValues.Clear();

                        //clear our VPLinkDataBox texts
                        foreach (Transform child in VPLinkDataBox.transform.Find("ScrollRect").Find("Panel"))
                        {
                            Destroy(child.gameObject);
                        }
                        //if the object selected is a vplink equipment
                        if (selectedObject.GetComponent<VPLinkEquipment>() != null)
                        {
                            /*
                            //add a new line to the VPLinkDataBox that displays the name of the object
                            GameObject newText = Instantiate(Resources.Load("InfoText", typeof(GameObject))) as GameObject;
                            newText.GetComponent<Text>().text = "Name: " + selectedObject.GetComponent<VPLinkEquipment>().equipmentName;
                            newText.transform.SetParent(VPLinkDataBox.transform.Find("ScrollRect").Find("Panel").transform);
                            newText.transform.localScale = new Vector3(1, 1, 1);

                            //add a seperator image
                            GameObject seperator = Instantiate(Resources.Load("SeperatorImage", typeof(GameObject))) as GameObject;
                            seperator.transform.SetParent(VPLinkDataBox.transform.Find("ScrollRect").Find("Panel").transform);
                            */
                        }

                        //for each tag in the selected object
                        for (int x = 0; x < 4; x++)
                        {
                            //add a new linke to the VPLinkDataBox that displays the vplink data
                            GameObject newText = Instantiate(Resources.Load("InfoText", typeof(GameObject))) as GameObject;
                            newText.GetComponent<Text>().text = "";
                            newText.transform.SetParent(VPLinkDataBox.transform.Find("ScrollRect").Find("Panel").transform);
                            newText.transform.localScale = new Vector3(1, 1, 1);

                            //add a seperator image
                            GameObject seperator = Instantiate(Resources.Load("SeperatorImage", typeof(GameObject))) as GameObject;
                            seperator.transform.SetParent(VPLinkDataBox.transform.Find("ScrollRect").Find("Panel").transform);


                            //add to our vp link values list so we know what to update later
                            vpLinkValues.Add(newText);
                        }
                    }
                    //if this is NOT a vplink file
                    else
                    {
                        //if the selected object has a vrgrabbable
                        if (selectedObject.GetComponent<SelectableController>() != null)
                        {
                            //clear our ObjectDataBox of all texts
                            foreach (Transform child in ObjectDataBox.transform.Find("ScrollRect").Find("Panel"))
                            {
                                Destroy(child.gameObject);
                            }

                            foreach (string infoValue in selectedObject.GetComponent<SelectableController>().infoValues)
                            {
                                //populate our ObjectDataBox with new texts based on the object's VRGrabbale infoValues list
                                GameObject newText = Instantiate(Resources.Load("InfoText", typeof(GameObject))) as GameObject;
                                newText.GetComponent<Text>().text = infoValue;
                                newText.transform.SetParent(ObjectDataBox.transform.Find("ScrollRect").Find("Panel").transform);
                                newText.transform.localScale = new Vector3(1, 1, 1);

                                //ad seperator image between each value
                                GameObject seperator = Instantiate(Resources.Load("SeperatorImage", typeof(GameObject))) as GameObject;
                                seperator.transform.SetParent(ObjectDataBox.transform.Find("ScrollRect").Find("Panel").transform);
                            }
                        }
                    }
                }
            }
        }
        else
        {

        }

        #endregion

        if (!isOrthoMode)
        {
            if (myCam.GetComponent<CameraController>() != null)
            {
                if (myCam.GetComponent<CameraController>().enabled)
                {
                    myCam.GetComponent<CameraController>().enabled = false;
                    myCam.transform.parent = this.transform;
                    myCam.transform.localPosition = new Vector3(0, 0, 0);
                    myCam.transform.localEulerAngles = new Vector3(0, 0, 0);
                    myCam.GetComponent<Camera>().orthographic = false;
                    myCam.GetComponent<Camera>().farClipPlane = 2500;
                }
            }
            
            #region Locomotion

            Vector3 forwardDir = myCam.transform.forward;
            forwardDir.y = 0;
            forwardDir.Normalize();

            Vector3 rightDir = myCam.transform.right;
            rightDir.y = 0;
            rightDir.Normalize();

            float scaleSensitivity = myCam.fieldOfView / 60.0f;


            if (!isOrthoMode)
            {
                rotationX += Input.GetAxis("Mouse X") * cameraSensitivity * scaleSensitivity * Time.deltaTime + Input.GetAxis("Xbox_RS_Horizontal") * cameraSensitivity * scaleSensitivity * Time.deltaTime;
                rotationY += Input.GetAxis("Mouse Y") * cameraSensitivity * scaleSensitivity * Time.deltaTime + Input.GetAxis("Xbox_RS_Vertical") * cameraSensitivity * scaleSensitivity * Time.deltaTime;
                rotationY = Mathf.Clamp(rotationY, -90, 90);


                myCam.transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);
                myCam.transform.localRotation *= Quaternion.AngleAxis(rotationY, Vector3.left);
                RadialDial.transform.eulerAngles = new Vector3(0, 0, rotationX);
            }

            if (!canClimb)
            {

                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift) || (Input.GetAxis("Xbox_RT") > .2f))
                {
                    if (!isFlying)
                    {
                        transform.position += forwardDir * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
                        transform.position += rightDir * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
                        transform.position += forwardDir * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Xbox_LS_Vertical") * Time.deltaTime;
                        transform.position += rightDir * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime;
                        transform.position += transform.up * (normalMoveSpeed * fastMoveFactor) * .5f * Input.GetAxis("Xbox_Dpad_Vertical") * Time.deltaTime;
                    }
                    else
                    {
                        transform.position += myCam.transform.forward * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
                        transform.position += myCam.transform.right * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
                        transform.position += myCam.transform.forward * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Xbox_LS_Vertical") * Time.deltaTime;
                        transform.position += myCam.transform.right * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime;
                        transform.position += transform.up * (normalMoveSpeed * fastMoveFactor) * .5f * Input.GetAxis("Xbox_Dpad_Vertical") * Time.deltaTime;
                        if (Input.GetKey(KeyCode.Q)) { transform.position += transform.up * climbSpeed * fastMoveFactor * Time.deltaTime; }
                        if (Input.GetKey(KeyCode.E)) { transform.position -= transform.up * climbSpeed * fastMoveFactor * Time.deltaTime; }
                    }
                }
                else if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl) || (Input.GetAxis("Xbox_LT") > .2f))
                {
                    if (!isFlying)
                    {
                        transform.position += forwardDir * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
                        transform.position += rightDir * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
                        transform.position += forwardDir * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
                        transform.position += rightDir * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
                        transform.position += transform.up * (normalMoveSpeed * slowMoveFactor) * .5f * Input.GetAxis("Xbox_Dpad_Vertical") * Time.deltaTime;
                    }
                    else
                    {
                        transform.position += myCam.transform.forward * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
                        transform.position += myCam.transform.right * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
                        transform.position += myCam.transform.forward * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Xbox_LS_Vertical") * Time.deltaTime;
                        transform.position += myCam.transform.right * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime;
                        transform.position += transform.up * (normalMoveSpeed * slowMoveFactor) * .5f * Input.GetAxis("Xbox_Dpad_Vertical") * Time.deltaTime;
                        if (Input.GetKey(KeyCode.Q)) { transform.position += transform.up * slowMoveFactor * climbSpeed * Time.deltaTime; }
                        if (Input.GetKey(KeyCode.E)) { transform.position -= transform.up * slowMoveFactor * climbSpeed * Time.deltaTime; }
                    }
                }
                else
                {
                    if (!isFlying)
                    {
                        transform.position += forwardDir * normalMoveSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
                        transform.position += rightDir * normalMoveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
                        transform.position += forwardDir * normalMoveSpeed * Input.GetAxis("Xbox_LS_Vertical") * Time.deltaTime;
                        transform.position += rightDir * normalMoveSpeed * Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime;
                        transform.position += transform.up * normalMoveSpeed * .5f * Input.GetAxis("Xbox_Dpad_Vertical") * Time.deltaTime;
                    }
                    else
                    {
                        transform.position += myCam.transform.forward * normalMoveSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
                        transform.position += myCam.transform.right * normalMoveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
                        transform.position += myCam.transform.forward * normalMoveSpeed * Input.GetAxis("Xbox_LS_Vertical") * Time.deltaTime;
                        transform.position += myCam.transform.right * normalMoveSpeed * Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime;
                        transform.position += transform.up * normalMoveSpeed * .5f * Input.GetAxis("Xbox_Dpad_Vertical") * Time.deltaTime;
                        if (Input.GetKey(KeyCode.Q)) { transform.position += transform.up * climbSpeed * Time.deltaTime; }
                        if (Input.GetKey(KeyCode.E)) { transform.position -= transform.up * climbSpeed * Time.deltaTime; }
                    }
                }
            }
            else
            {
                transform.position += Vector3.up * normalMoveSpeed * .5f * Input.GetAxis("Vertical") * Time.deltaTime;
            }

            if (isFlying)
            {
                if (Input.GetKey(KeyCode.Q)) { transform.position += transform.up * climbSpeed * Time.deltaTime; }
                if (Input.GetKey(KeyCode.E)) { transform.position -= transform.up * climbSpeed * Time.deltaTime; }
            }


            if (Input.GetKeyDown(KeyCode.End))
            {
                Screen.lockCursor = (Screen.lockCursor == false) ? true : false;
            }

            #endregion

            #region Measuring

            if (isMeasuringDistance && (isMeasuringDistance1stPoint || isMeasuringDistance2ndPoint))
            {

                GameObject measurement = measureDistancePrefabs[measureDistancePrefabs.Count - 1];
                LineRenderer hypLine = measurement.transform.Find("HypotenuseText").GetComponent<LineRenderer>();
                TextMesh hypotenuseTextMesh = measurement.transform.Find("HypotenuseText").GetComponent<TextMesh>();
                BoxCollider hypotenuseTextBox = measurement.transform.Find("HypotenuseText").GetComponent<BoxCollider>();
                LineRenderer horLine = measurement.transform.Find("HorizontalText").GetComponent<LineRenderer>();
                TextMesh horizontalTextMesh = measurement.transform.Find("HorizontalText").GetComponent<TextMesh>();
                BoxCollider horizontalTextBox = measurement.transform.Find("HorizontalText").GetComponent<BoxCollider>();
                LineRenderer verLine = measurement.transform.Find("VerticalText").GetComponent<LineRenderer>();
                TextMesh verticalTextMesh = measurement.transform.Find("VerticalText").GetComponent<TextMesh>();
                BoxCollider verticalTextBox = measurement.transform.Find("VerticalText").GetComponent<BoxCollider>();
                measurement.transform.Find("HypotenuseText").GetComponent<MakeBillboard>().cam = myCam;
                measurement.transform.Find("HorizontalText").GetComponent<MakeBillboard>().cam = myCam;
                measurement.transform.Find("VerticalText").GetComponent<MakeBillboard>().cam = myCam;
                hypotenuseTextBox.enabled = false;
                horizontalTextBox.enabled = false;
                verticalTextBox.enabled = false;

                if (isMeasuringDistance1stPoint)
                {
                    int lay = LayerMask.NameToLayer("Detail Object");
                    int lay2 = LayerMask.NameToLayer("Detail Object ROV");
                    int layermask1 = (1 << lay) | (1 << lay2);

                    RaycastHit hit;
                    Ray ray = new Ray(transform.position, myCam.transform.forward);
                    if (Physics.Raycast(ray, out hit, 100.0f, layermask1))
                    {
                        measurement.SetActive(true);
                        if ((hit.transform.name == "MeasurePointIconMesh0" || hit.transform.name == "MeasurePointIconMesh1"))
                        {
                            measurePoint0 = hit.transform.parent.Find("MeasureLinePoint").position;
                            measurement.transform.Find("MeasurePointIcon0").GetChild(0).GetComponent<MeshRenderer>().enabled = false;
                        }
                        else
                        {
                            measurePoint0 = hit.point;
                            measurement.transform.Find("MeasurePointIcon0").GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                            measurement.transform.Find("MeasurePointIcon0").transform.position = measurePoint0;
                            measurement.transform.Find("MeasurePointIcon0").transform.LookAt(transform.position + hit.normal * 5);
                            measurement.transform.Find("MeasurePointIcon0").transform.rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);

                            measurePoint0 += hit.normal / 100;
                            measurement.transform.Find("MeasurePointIcon0").Find("MeasureLinePoint").position = measurePoint0;
                        }
                    }
                    else
                    {
                        measurement.SetActive(false);
                    }
                        
                }
                else if (isMeasuringDistance2ndPoint)
                {
                    int lay = LayerMask.NameToLayer("Detail Object");
                    int lay2 = LayerMask.NameToLayer("Detail Object ROV");
                    int layermask1 = (1 << lay) | (1 << lay2);

                    RaycastHit hit;
                    Ray ray = new Ray(transform.position, myCam.transform.forward);
                    if (Physics.Raycast(ray, out hit, 100.0f, layermask1))
                    {

                        measurement.transform.Find("MeasurePointIcon1").gameObject.SetActive(true);
                        measurement.transform.Find("HypotenuseText").gameObject.SetActive(true);
                        measurement.transform.Find("HorizontalText").gameObject.SetActive(true);
                        measurement.transform.Find("VerticalText").gameObject.SetActive(true);

                        measurePoint1 = hit.point;
                        
                        measurement.transform.Find("MeasurePointIcon1").position = hit.point;
                        measurement.transform.Find("MeasurePointIcon1").LookAt(transform.position + hit.normal * 5);
                        measurement.transform.Find("MeasurePointIcon1").rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);

                        measurePoint1 += hit.normal / 100;
                        measurement.transform.Find("MeasurePointIcon1").Find("MeasureLinePoint").position = measurePoint1;

                        if (hit.transform.name == "MeasurePointIconMesh0" || hit.transform.name == "MeasurePointIconMesh1")
                        {
                            measurePoint1 = hit.transform.parent.position;
                            measurePoint1 += hit.normal / 100;
                            hypLine.SetPosition(0, measurePoint0);
                            hypLine.SetPosition(1, measurePoint1);
                            horLine.SetPosition(0, new Vector3(measurePoint0.x, measurePoint0.y + (measurePoint1.y - measurePoint0.y), measurePoint0.z));
                            horLine.SetPosition(1, measurePoint1);
                            verLine.SetPosition(0, measurePoint0);
                            verLine.SetPosition(1, new Vector3(measurePoint0.x, measurePoint0.y + (measurePoint1.y - measurePoint0.y), measurePoint0.z));
                            measurement.transform.Find("MeasurePointIcon1").gameObject.SetActive(false);
                        }
                        else
                        {
                            if (Vector3.Distance(measurePoint1, new Vector3(measurePoint0.x, measurePoint1.y, measurePoint0.z)) < 0.125)
                            {
                                hypLine.transform.GetComponent<LineRenderer>().enabled = true;
                                hypotenuseTextMesh.transform.GetComponent<MeshRenderer>().enabled = true;
                                hypLine.SetPosition(0, measurePoint0);
                                hypLine.SetPosition(1, new Vector3(measurePoint0.x, measurePoint1.y, measurePoint0.z));
                                hypotenuseTextMesh.transform.position = (hypLine.GetPosition(0) + hypLine.GetPosition(1)) / 2;
                                hypotenuseTextMesh.text = "" + (Vector3.Distance(measurePoint0, new Vector3(measurePoint0.x, measurePoint1.y, measurePoint0.z)) * 3.28084f).ToString("F2") + "(ft)";
                                horLine.transform.GetComponent<LineRenderer>().enabled = false;
                                verLine.transform.GetComponent<LineRenderer>().enabled = false;
                                horizontalTextMesh.transform.GetComponent<MeshRenderer>().enabled = false;
                                verticalTextMesh.transform.GetComponent<MeshRenderer>().enabled = false;
                            }
                            else if (Vector3.Distance(measurePoint0, new Vector3(measurePoint0.x, measurePoint1.y, measurePoint0.z)) < 0.125)
                            {
                                hypLine.transform.GetComponent<LineRenderer>().enabled = true;
                                hypotenuseTextMesh.transform.GetComponent<MeshRenderer>().enabled = true;
                                hypLine.SetPosition(0, measurePoint0);
                                hypLine.SetPosition(1, measurePoint1);
                                hypotenuseTextMesh.transform.position = (hypLine.GetPosition(0) + hypLine.GetPosition(1)) / 2;
                                hypotenuseTextMesh.text = "" + (Vector3.Distance(measurePoint0, measurePoint1) * 3.28084f).ToString("F2") + "(ft)";
                                horLine.transform.GetComponent<LineRenderer>().enabled = false;
                                verLine.transform.GetComponent<LineRenderer>().enabled = false;
                                horizontalTextMesh.transform.GetComponent<MeshRenderer>().enabled = false;
                                verticalTextMesh.transform.GetComponent<MeshRenderer>().enabled = false;
                            }
                            else
                            {
                                horLine.transform.GetComponent<LineRenderer>().enabled = true;
                                verLine.transform.GetComponent<LineRenderer>().enabled = true;
                                hypLine.transform.GetComponent<LineRenderer>().enabled = true;
                                horizontalTextMesh.transform.GetComponent<MeshRenderer>().enabled = true;
                                verticalTextMesh.transform.GetComponent<MeshRenderer>().enabled = true;
                                hypotenuseTextMesh.transform.GetComponent<MeshRenderer>().enabled = true;

                                hypLine.SetPosition(0, measurePoint0);
                                hypLine.SetPosition(1, measurePoint1);
                                hypotenuseTextMesh.transform.position = (hypLine.GetPosition(0) + hypLine.GetPosition(1)) / 2;
                                hypotenuseTextMesh.text = "" + (Vector3.Distance(measurePoint0, measurePoint1) * 3.28084f).ToString("F2") + "(ft)";

                                verLine.SetPosition(0, measurePoint0);
                                verLine.SetPosition(1, new Vector3(measurePoint0.x, measurePoint0.y + (measurePoint1.y - measurePoint0.y), measurePoint0.z));
                                verticalTextMesh.transform.position = (verLine.GetPosition(0) + verLine.GetPosition(1)) / 2;
                                verticalTextMesh.text = "" + (Vector3.Distance(verLine.GetPosition(0), verLine.GetPosition(1)) * 3.28084f).ToString("F2") + "(ft)";
                                
                                horLine.SetPosition(0, new Vector3(measurePoint0.x, measurePoint0.y + (measurePoint1.y - measurePoint0.y), measurePoint0.z));
                                horLine.SetPosition(1, measurePoint1);
                                horizontalTextMesh.transform.position = (horLine.GetPosition(0) + horLine.GetPosition(1)) / 2;
                                horizontalTextMesh.text = "" + (Vector3.Distance(horLine.GetPosition(0), horLine.GetPosition(1)) * 3.28084f).ToString("F2") + "(ft)";

                                measurement.transform.Find("MeasurePointIcon1").position = hit.point;
                                measurement.transform.Find("MeasurePointIcon1").LookAt(transform.position + hit.normal * 5);
                                measurement.transform.Find("MeasurePointIcon1").rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);
                            }
                        }
                    }
                    else
                    {
                        measurement.transform.Find("MeasurePointIcon1").gameObject.SetActive(false);
                        measurement.transform.Find("HypotenuseText").gameObject.SetActive(false);
                        measurement.transform.Find("HorizontalText").gameObject.SetActive(false);
                        measurement.transform.Find("VerticalText").gameObject.SetActive(false);
                    }
                }
            }

            if ((Input.GetMouseButtonDown(0) || Input.GetKeyDown("joystick button 0")) && isMeasuringDistance)
            {
                if (isMeasuringDistance1stPoint)
                {
                    isMeasuringDistance2ndPoint = true;
                    isMeasuringDistance1stPoint = false;
                    measureDistancePrefabs[measureDistancePrefabs.Count - 1].transform.Find("MeasurePointIcon0").GetChild(0).GetComponent<MeshCollider>().enabled = true;
                }
                else if (isMeasuringDistance2ndPoint)
                {
                    isMeasuringDistance2ndPoint = false;
                    hasMeasuredDistance = true;
                    isMeasuringDistance = false;
                    isIndicating = false;
                    IndicatorMenu.SetActive(false);
                    measureDistancePrefabs[measureDistancePrefabs.Count - 1].transform.Find("HypotenuseText").GetComponent<BoxCollider>().enabled = true;
                    measureDistancePrefabs[measureDistancePrefabs.Count - 1].transform.Find("HorizontalText").GetComponent<BoxCollider>().enabled = true;
                    measureDistancePrefabs[measureDistancePrefabs.Count - 1].transform.Find("VerticalText").GetComponent<BoxCollider>().enabled = true;
                    measureDistancePrefabs[measureDistancePrefabs.Count - 1].transform.Find("MeasurePointIcon1").GetChild(0).GetComponent<MeshCollider>().enabled = true;
                }

            }

            #endregion

            #region MeasuringAngle


            if (isMeasuringAngle && (isMeasuringAngle1stPoint || isMeasuringAngle2ndPoint || isMeasuringAngle3rdPoint || isMeasuringAngle4thPoint))
            {
                //Assign components of angle measure prefab
                GameObject measurement = measureAnglePrefabs[measureAnglePrefabs.Count - 1];
                LineRenderer AngleLine0 = measurement.transform.Find("AngleLine0").GetComponent<LineRenderer>();
                LineRenderer AngleLine1 = measurement.transform.Find("AngleLine1").GetComponent<LineRenderer>();
                LineRenderer FlatLineProjection0 = measurement.transform.Find("FlatProjection0").GetComponent<LineRenderer>();
                LineRenderer FlatLineProjection1 = measurement.transform.Find("FlatProjection1").GetComponent<LineRenderer>();
                LineRenderer AngleLineProjection0 = measurement.transform.Find("AngleLineProjection0").GetComponent<LineRenderer>();
                LineRenderer AngleLineProjection1 = measurement.transform.Find("AngleLineProjection1").GetComponent<LineRenderer>();
                LineRenderer ProjectionTrace0 = measurement.transform.Find("ProjectionTrace0").GetComponent<LineRenderer>();
                LineRenderer ProjectionTrace1 = measurement.transform.Find("ProjectionTrace1").GetComponent<LineRenderer>();
                LineRenderer ProjectionTrace2 = measurement.transform.Find("ProjectionTrace2").GetComponent<LineRenderer>();
                LineRenderer ProjectionTrace3 = measurement.transform.Find("ProjectionTrace3").GetComponent<LineRenderer>();
                LineRenderer Angle0 = measurement.transform.Find("Angle0").GetComponent<LineRenderer>();
                TextMesh AngleText0 = measurement.transform.Find("Angle0").GetComponent<TextMesh>();
                LineRenderer Angle1 = measurement.transform.Find("Angle1").GetComponent<LineRenderer>();
                TextMesh AngleText1 = measurement.transform.Find("Angle1").GetComponent<TextMesh>();

                //Define first point
                if (isMeasuringAngle1stPoint)
                {
                    int lay = LayerMask.NameToLayer("Detail Object");
                    int lay2 = LayerMask.NameToLayer("Detail Object ROV");
                    int layermask1 = (1 << lay) | (1 << lay2);

                    RaycastHit hit;
                    Ray ray = new Ray(transform.position, myCam.transform.forward);
                    if (Physics.Raycast(ray, out hit, 100.0f, layermask1))
                    {
                        measurement.SetActive(true);
                        measurePoint0 = hit.point;
                        measurement.transform.Find("MeasurePointIcon0").GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                        measurement.transform.Find("MeasurePointIcon0").transform.position = measurePoint0;
                        measurement.transform.Find("MeasurePointIcon0").transform.LookAt(transform.position + hit.normal * 5);
                        measurement.transform.Find("MeasurePointIcon0").transform.rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);
                        measurePoint0 += hit.normal / 100;
                        measurement.transform.Find("MeasurePointIcon0").Find("MeasureLinePoint").position = measurePoint0;

                    }
                    else
                    {
                        measurement.SetActive(false);
                    }
                }
                //Define second point
                else if (isMeasuringAngle2ndPoint)
                {
                    int lay = LayerMask.NameToLayer("Detail Object");
                    int lay2 = LayerMask.NameToLayer("Detail Object ROV");
                    int layermask1 = (1 << lay) | (1 << lay2);

                    RaycastHit hit;
                    Ray ray = new Ray(transform.position, myCam.transform.forward);
                    if (Physics.Raycast(ray, out hit, 100.0f, layermask1))
                    {
                        measurement.transform.Find("MeasurePointIcon1").gameObject.SetActive(true);
                        measurement.transform.Find("AngleLine0").gameObject.SetActive(true);
                        //hold Ctrl to make a flat or vertical line
                        if (Input.GetKey(KeyCode.LeftControl))
                        {
                            Vector3 flat = new Vector3(hit.point.x, measurePoint0.y, hit.point.z);
                            Vector3 vert = new Vector3(measurePoint0.x, hit.point.y, measurePoint0.z);
                            //Make line flat if angle between hit point and flat line is less than 45 deg, else vertical
                            if (Vector3.Angle((hit.point - measurePoint0), (flat - measurePoint0)) < 45.0f)
                            {
                                measurePoint1 = flat;
                                measurement.transform.Find("MeasurePointIcon1").GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                                measurement.transform.Find("MeasurePointIcon1").transform.position = measurePoint1;
                                measurement.transform.Find("MeasurePointIcon1").transform.LookAt(transform.position + hit.normal * 5);
                                measurement.transform.Find("MeasurePointIcon1").transform.rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);
                                measurePoint1 += hit.normal / 100;
                                measurement.transform.Find("MeasurePointIcon1").Find("MeasureLinePoint").position = measurePoint1;

                                measurement.transform.Find("MeasurePointIcon1").GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                                AngleLine0.SetPosition(0, measurePoint0);
                                AngleLine0.SetPosition(1, measurePoint1);
                            }
                            else
                            {
                                measurePoint1 = vert;
                                measurement.transform.Find("MeasurePointIcon1").GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                                measurement.transform.Find("MeasurePointIcon1").transform.position = measurePoint1;
                                measurement.transform.Find("MeasurePointIcon1").transform.LookAt(transform.position + hit.normal * 5);
                                measurement.transform.Find("MeasurePointIcon1").transform.rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);
                                measurePoint1 += hit.normal / 100;
                                measurement.transform.Find("MeasurePointIcon1").Find("MeasureLinePoint").position = measurePoint1;

                                measurement.transform.Find("MeasurePointIcon1").GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                                AngleLine0.SetPosition(0, measurePoint0);
                                AngleLine0.SetPosition(1, measurePoint1);
                            }
                        }
                        else
                        {
                            measurePoint1 = hit.point;
                            measurement.transform.Find("MeasurePointIcon1").GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                            measurement.transform.Find("MeasurePointIcon1").transform.position = measurePoint1;
                            measurement.transform.Find("MeasurePointIcon1").transform.LookAt(transform.position + hit.normal * 5);
                            measurement.transform.Find("MeasurePointIcon1").transform.rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);
                            measurePoint1 += hit.normal / 100;
                            measurement.transform.Find("MeasurePointIcon1").Find("MeasureLinePoint").position = measurePoint1;

                            measurement.transform.Find("MeasurePointIcon1").GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                            AngleLine0.SetPosition(0, measurePoint0);
                            AngleLine0.SetPosition(1, measurePoint1);
                        }  
                    }
                    else
                    {
                        measurement.transform.Find("MeasurePointIcon1").gameObject.SetActive(false);
                        measurement.transform.Find("AngleLine0").gameObject.SetActive(false);
                    }
                }
                //Same as above for second line
                else if (isMeasuringAngle3rdPoint)
                {
                    int lay = LayerMask.NameToLayer("Detail Object");
                    int lay2 = LayerMask.NameToLayer("Detail Object ROV");
                    int layermask1 = (1 << lay) | (1 << lay2);

                    RaycastHit hit;
                    Ray ray = new Ray(transform.position, myCam.transform.forward);
                    if (Physics.Raycast(ray, out hit, 100.0f, layermask1))
                    {
                        measurement.transform.Find("MeasurePointIcon2").gameObject.SetActive(true);
                        measurePoint0 = hit.point;
                        measurement.transform.Find("MeasurePointIcon2").GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                        measurement.transform.Find("MeasurePointIcon2").transform.position = measurePoint0;
                        measurement.transform.Find("MeasurePointIcon2").transform.LookAt(transform.position + hit.normal * 5);
                        measurement.transform.Find("MeasurePointIcon2").transform.rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);
                        measurePoint0 += hit.normal / 100;
                        measurement.transform.Find("MeasurePointIcon2").Find("MeasureLinePoint").position = measurePoint0;
                    }
                    else
                    {
                        measurement.transform.Find("MeasurePointIcon2").gameObject.SetActive(false);
                    }
                }
                else if (isMeasuringAngle4thPoint)
                {
                    int lay = LayerMask.NameToLayer("Detail Object");
                    int lay2 = LayerMask.NameToLayer("Detail Object ROV");
                    int layermask1 = (1 << lay) | (1 << lay2);

                    RaycastHit hit;
                    Ray ray = new Ray(transform.position, myCam.transform.forward);
                    if (Physics.Raycast(ray, out hit, 100.0f, layermask1))
                    {
                        measurement.transform.Find("MeasurePointIcon3").gameObject.SetActive(true);
                        measurement.transform.Find("AngleLine1").gameObject.SetActive(true);
                        if (Input.GetKey(KeyCode.LeftControl))
                        {
                            Vector3 flat = new Vector3(hit.point.x, measurePoint0.y, hit.point.z);
                            Vector3 vert = new Vector3(measurePoint0.x, hit.point.y, measurePoint0.z);
                            if (Vector3.Angle((hit.point - measurePoint0), (flat - measurePoint0)) < 45.0f)
                            {
                                measurePoint1 = flat;
                                measurement.transform.Find("MeasurePointIcon3").GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                                measurement.transform.Find("MeasurePointIcon3").transform.position = measurePoint1;
                                measurement.transform.Find("MeasurePointIcon3").transform.LookAt(transform.position + hit.normal * 5);
                                measurement.transform.Find("MeasurePointIcon3").transform.rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);
                                measurePoint1 += hit.normal / 100;
                                measurement.transform.Find("MeasurePointIcon3").Find("MeasureLinePoint").position = measurePoint1;

                                measurement.transform.Find("MeasurePointIcon3").GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                                AngleLine1.SetPosition(0, measurePoint0);
                                AngleLine1.SetPosition(1, measurePoint1);
                            }
                            else
                            {
                                measurePoint1 = vert;
                                measurement.transform.Find("MeasurePointIcon3").GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                                measurement.transform.Find("MeasurePointIcon3").transform.position = measurePoint1;
                                measurement.transform.Find("MeasurePointIcon3").transform.LookAt(transform.position + hit.normal * 5);
                                measurement.transform.Find("MeasurePointIcon3").transform.rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);
                                measurePoint1 += hit.normal / 100;
                                measurement.transform.Find("MeasurePointIcon3").Find("MeasureLinePoint").position = measurePoint1;

                                measurement.transform.Find("MeasurePointIcon3").GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                                AngleLine1.SetPosition(0, measurePoint0);
                                AngleLine1.SetPosition(1, measurePoint1);
                            }
                        }
                        else
                        {
                            measurePoint1 = hit.point;
                            measurement.transform.Find("MeasurePointIcon3").GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                            measurement.transform.Find("MeasurePointIcon3").transform.position = measurePoint1;
                            measurement.transform.Find("MeasurePointIcon3").transform.LookAt(transform.position + hit.normal * 5);
                            measurement.transform.Find("MeasurePointIcon3").transform.rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);
                            measurePoint1 += hit.normal / 100;
                            measurement.transform.Find("MeasurePointIcon3").Find("MeasureLinePoint").position = measurePoint1;

                            measurement.transform.Find("MeasurePointIcon3").GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                            AngleLine1.SetPosition(0, measurePoint0);
                            AngleLine1.SetPosition(1, measurePoint1);
                        }
                    }
                    else
                    {
                        measurement.transform.Find("MeasurePointIcon3").gameObject.SetActive(false);
                        measurement.transform.Find("AngleLine1").gameObject.SetActive(false);
                    }
                }
                //Cycle through defining points with mouse click
                if ((Input.GetMouseButtonDown(0) || Input.GetKeyDown("joystick button 0")))
                {
                    if (isMeasuringAngle1stPoint)
                    {
                        isMeasuringAngle2ndPoint = true;
                        isMeasuringAngle1stPoint = false;
                    }
                    else if (isMeasuringAngle2ndPoint)
                    {
                        isMeasuringAngle2ndPoint = false;
                        isMeasuringAngle3rdPoint = true;
                    }
                    else if (isMeasuringAngle3rdPoint)
                    {
                        isMeasuringAngle3rdPoint = false;
                        isMeasuringAngle4thPoint = true;
                    }
                    else if (isMeasuringAngle4thPoint)
                    {
                        isMeasuringAngle4thPoint = false;
                        hasMeasuredAngle = true;
                        isMeasuringAngle = false;
                        isIndicating = false;
                        IndicatorMenu.SetActive(false);
                        
                        //Once all points defined, make projections and measure angles
                        //Direction from point 0 to 1
                        Vector3 AngleLineDirection0 = (AngleLine0.GetPosition(1) - AngleLine0.GetPosition(0)).normalized;
                        //From point 1 to 0
                        Vector3 AngleLineDirection1 = (AngleLine0.GetPosition(0) - AngleLine0.GetPosition(1)).normalized;
                        Vector3 AngleLineDirection2 = (AngleLine1.GetPosition(1) - AngleLine1.GetPosition(0)).normalized;
                        Vector3 AngleLineDirection3 = (AngleLine1.GetPosition(0) - AngleLine1.GetPosition(1)).normalized;

                        //Vertical plane formed by line 1
                        Plane AnglePlane0 = new Plane(AngleLine1.GetPosition(0), AngleLine1.GetPosition(1), AngleLine1.GetPosition(0) + Vector3.up);
                        //Vertical plane formed by line 0
                        Plane AnglePlane1 = new Plane(AngleLine0.GetPosition(0), AngleLine0.GetPosition(1), AngleLine0.GetPosition(0) + Vector3.up);

                        //Ray pointing away from point 0, line 0
                        Ray planeRay0 = new Ray(AngleLine0.GetPosition(0), AngleLineDirection1);
                        //Ray pointing away from point 1, line 0
                        Ray planeRay1 = new Ray(AngleLine0.GetPosition(1), AngleLineDirection0);
                        Ray planeRay2 = new Ray(AngleLine1.GetPosition(0), AngleLineDirection3);
                        Ray planeRay3 = new Ray(AngleLine1.GetPosition(1), AngleLineDirection2);

                        //Distance from point to plane
                        float rayDistance;

                        //Normals for angle calculation and drawing angles
                        Vector3 FlatNormal0 = Vector3.zero;
                        Vector3 ProjectionNormal0 = Vector3.zero;
                        Vector3 AngleNormal0 = Vector3.zero;
                        Vector3 FlatNormal1 = Vector3.zero;
                        Vector3 ProjectionNormal1 = Vector3.zero;
                        Vector3 AngleNormal1 = Vector3.zero;

                        //Angles
                        float flatToProjection0 = 0;
                        float flatToAngle0 = 0;
                        float flatToProjection1 = 0;
                        float flatToAngle1 = 0;
                        float flatToFlat = 0;

                        //Number of generated projections
                        int projections = 0;

                        //Layer projections on their source, for consistency's sake
                        AngleLineProjection0.SetPosition(0, AngleLine0.GetPosition(0));
                        AngleLineProjection0.SetPosition(1, AngleLine0.GetPosition(1));
                        AngleLineProjection1.SetPosition(0, AngleLine1.GetPosition(0));
                        AngleLineProjection1.SetPosition(1, AngleLine1.GetPosition(1));

                        //Check which lines intersect with which planes
                        AnglePlane0.Raycast(planeRay0, out rayDistance);
                        if (rayDistance > 0)
                        {
                            //Extend line to plane
                            AngleLine0.SetPosition(0, AngleLine0.GetPosition(0) + AngleLineDirection1 * Mathf.Abs(rayDistance));
                            
                            //Calculate y-offset for projection
                            float yDiff = AngleLine0.GetPosition(0).y - (((AngleLine0.GetPosition(0).x - AngleLine1.GetPosition(0).x) / AngleLineDirection2.x) * AngleLineDirection2.y + AngleLine1.GetPosition(0).y);
                            
                            //Set projection to intersect second line
                            AngleLineProjection0.SetPosition(0, new Vector3(AngleLine0.GetPosition(0).x, AngleLine0.GetPosition(0).y - yDiff, AngleLine0.GetPosition(0).z));
                            AngleLineProjection0.SetPosition(1, new Vector3(AngleLine0.GetPosition(1).x, AngleLine0.GetPosition(1).y - yDiff, AngleLine0.GetPosition(1).z));
                            
                            //Line up a flat projection as a reference
                            FlatLineProjection0.SetPosition(0, new Vector3(AngleLine0.GetPosition(0).x, AngleLine0.GetPosition(0).y - yDiff, AngleLine0.GetPosition(0).z));
                            FlatLineProjection0.SetPosition(1, new Vector3(AngleLine0.GetPosition(1).x, AngleLine0.GetPosition(0).y - yDiff, AngleLine0.GetPosition(1).z)); 
                            
                            //Set position of projection traces between angle line and projection
                            ProjectionTrace0.SetPosition(0, AngleLine0.GetPosition(0));
                            ProjectionTrace0.SetPosition(1, AngleLineProjection0.GetPosition(0));
                            ProjectionTrace1.SetPosition(0, AngleLine0.GetPosition(1));
                            ProjectionTrace1.SetPosition(1, AngleLineProjection0.GetPosition(1));

                            //Calculate normals
                            FlatNormal0 = (FlatLineProjection0.GetPosition(1) - FlatLineProjection0.GetPosition(0)).normalized / 2;
                            ProjectionNormal0 = (AngleLineProjection0.GetPosition(1) - AngleLineProjection0.GetPosition(0)).normalized / 2;
                            if(Vector3.Distance(FlatLineProjection0.GetPosition(0), AngleLine1.GetPosition(0)) > 0.5f)
                            {
                                AngleNormal0 = (AngleLine1.GetPosition(0) - FlatLineProjection0.GetPosition(0)).normalized / 2;
                            }
                            else
                            {
                                AngleNormal0 = (AngleLine1.GetPosition(1) - FlatLineProjection0.GetPosition(0)).normalized / 2;
                            }

                            //Calculate angles
                            flatToProjection0 = Vector3.Angle(FlatNormal0, ProjectionNormal0);
                            flatToAngle0 = Vector3.Angle(FlatNormal0, AngleNormal0);
                            projections++;
                        }
                        AnglePlane0.Raycast(planeRay1, out rayDistance);
                        if (rayDistance > 0)
                        {
                            AngleLine0.SetPosition(1, AngleLine0.GetPosition(1) + AngleLineDirection0 * Mathf.Abs(rayDistance));
                            float yDiff = AngleLine0.GetPosition(1).y - (((AngleLine0.GetPosition(1).x - AngleLine1.GetPosition(0).x) / AngleLineDirection2.x) * AngleLineDirection2.y + AngleLine1.GetPosition(0).y);
                            AngleLineProjection0.SetPosition(0, new Vector3(AngleLine0.GetPosition(1).x, AngleLine0.GetPosition(1).y - yDiff, AngleLine0.GetPosition(1).z));
                            AngleLineProjection0.SetPosition(1, new Vector3(AngleLine0.GetPosition(0).x, AngleLine0.GetPosition(0).y - yDiff, AngleLine0.GetPosition(0).z));
                            FlatLineProjection0.SetPosition(0, new Vector3(AngleLine0.GetPosition(1).x, AngleLine0.GetPosition(1).y - yDiff, AngleLine0.GetPosition(1).z));
                            FlatLineProjection0.SetPosition(1, new Vector3(AngleLine0.GetPosition(0).x, AngleLine0.GetPosition(1).y - yDiff, AngleLine0.GetPosition(0).z));
                            ProjectionTrace0.SetPosition(0, AngleLine0.GetPosition(1));
                            ProjectionTrace0.SetPosition(1, AngleLineProjection0.GetPosition(0));
                            ProjectionTrace1.SetPosition(0, AngleLine0.GetPosition(0));
                            ProjectionTrace1.SetPosition(1, AngleLineProjection0.GetPosition(1));
                            FlatNormal0 = (FlatLineProjection0.GetPosition(1) - FlatLineProjection0.GetPosition(0)).normalized / 2;
                            ProjectionNormal0 = (AngleLineProjection0.GetPosition(1) - AngleLineProjection0.GetPosition(0)).normalized / 2;
                            if (Vector3.Distance(FlatLineProjection0.GetPosition(0), AngleLine1.GetPosition(0)) > 0.5f)
                            {
                                AngleNormal0 = (AngleLine1.GetPosition(0) - FlatLineProjection0.GetPosition(0)).normalized / 2;
                            }
                            else
                            {
                                AngleNormal0 = (AngleLine1.GetPosition(1) - FlatLineProjection0.GetPosition(0)).normalized / 2;
                            }
                            flatToProjection0 = Vector3.Angle(FlatNormal0, ProjectionNormal0);
                            flatToAngle0 = Vector3.Angle(FlatNormal0, AngleNormal0);
                            projections++;
                        }
                        //Same as above, but from second line to first
                        AnglePlane1.Raycast(planeRay2, out rayDistance);
                        if (rayDistance > 0)
                        {
                            AngleLine1.SetPosition(0, AngleLine1.GetPosition(0) + AngleLineDirection3 * Mathf.Abs(rayDistance));
                            float yDiff = AngleLine1.GetPosition(0).y - (((AngleLine1.GetPosition(0).x - AngleLine0.GetPosition(0).x) / AngleLineDirection0.x) * AngleLineDirection0.y + AngleLine0.GetPosition(0).y);
                            AngleLineProjection1.SetPosition(0, new Vector3(AngleLine1.GetPosition(0).x, AngleLine1.GetPosition(0).y - yDiff, AngleLine1.GetPosition(0).z));
                            AngleLineProjection1.SetPosition(1, new Vector3(AngleLine1.GetPosition(1).x, AngleLine1.GetPosition(1).y - yDiff, AngleLine1.GetPosition(1).z));
                            FlatLineProjection1.SetPosition(0, new Vector3(AngleLine1.GetPosition(0).x, AngleLine1.GetPosition(0).y - yDiff, AngleLine1.GetPosition(0).z));
                            FlatLineProjection1.SetPosition(1, new Vector3(AngleLine1.GetPosition(1).x, AngleLine1.GetPosition(0).y - yDiff, AngleLine1.GetPosition(1).z));
                            ProjectionTrace2.SetPosition(0, AngleLine1.GetPosition(0));
                            ProjectionTrace2.SetPosition(1, AngleLineProjection1.GetPosition(0));
                            ProjectionTrace3.SetPosition(0, AngleLine1.GetPosition(1));
                            ProjectionTrace3.SetPosition(1, AngleLineProjection1.GetPosition(1));
                            FlatNormal1 = (FlatLineProjection1.GetPosition(1) - FlatLineProjection1.GetPosition(0)).normalized / 2;
                            ProjectionNormal1 = (AngleLineProjection1.GetPosition(1) - AngleLineProjection1.GetPosition(0)).normalized / 2;
                            if (Vector3.Distance(FlatLineProjection1.GetPosition(0), AngleLine0.GetPosition(0)) > 0.5f)
                            {
                                AngleNormal1 = (AngleLine0.GetPosition(0) - FlatLineProjection1.GetPosition(0)).normalized / 2;
                            }
                            else
                            {
                                AngleNormal1 = (AngleLine0.GetPosition(1) - FlatLineProjection1.GetPosition(0)).normalized / 2;
                            }
                            flatToProjection1 = Vector3.Angle(FlatNormal1, ProjectionNormal1);
                            flatToAngle1 = Vector3.Angle(FlatNormal1, AngleNormal1);
                            projections++;
                            //If this is the first projection, redefine projections, normals, and angles, for consistency
                            if (projections == 1)
                            {
                                AngleLineProjection0 = AngleLineProjection1;
                                FlatLineProjection0 = FlatLineProjection1;
                                FlatNormal0 = FlatNormal1;
                                AngleNormal0 = AngleNormal1;
                                ProjectionNormal0 = ProjectionNormal1;
                                flatToProjection0 = flatToProjection1;
                                flatToAngle0 = flatToAngle1;
                            }
                            //For multiple projections, align flat projections, and calculate angle between them
                            else
                            {
                                flatToFlat = Vector3.Angle(FlatNormal0, FlatNormal1);
                                FlatLineProjection0.SetPosition(0, FlatLineProjection1.GetPosition(0));
                                FlatLineProjection0.SetPosition(1, new Vector3(FlatLineProjection0.GetPosition(1).x, FlatLineProjection1.GetPosition(1).y, FlatLineProjection0.GetPosition(1).z));
                                ProjectionTrace0.SetPosition(1, FlatLineProjection0.GetPosition(0));
                                ProjectionTrace1.SetPosition(1, FlatLineProjection0.GetPosition(1));
                                ProjectionTrace2.SetPosition(1, FlatLineProjection1.GetPosition(0));
                                ProjectionTrace3.SetPosition(1, FlatLineProjection1.GetPosition(1));
                                //Hide extra projections
                                AngleLineProjection0.enabled = false;
                                AngleLineProjection1.enabled = false;
                            }
                        }
                        AnglePlane1.Raycast(planeRay3, out rayDistance);
                        if (rayDistance > 0)
                        {
                            AngleLine1.SetPosition(1, AngleLine1.GetPosition(1) + AngleLineDirection2 * Mathf.Abs(rayDistance));
                            float yDiff = AngleLine1.GetPosition(1).y - (((AngleLine1.GetPosition(1).x - AngleLine0.GetPosition(0).x) / AngleLineDirection0.x) * AngleLineDirection0.y + AngleLine0.GetPosition(0).y);
                            AngleLineProjection1.SetPosition(0, new Vector3(AngleLine1.GetPosition(1).x, AngleLine1.GetPosition(1).y - yDiff, AngleLine1.GetPosition(1).z));
                            AngleLineProjection1.SetPosition(1, new Vector3(AngleLine1.GetPosition(0).x, AngleLine1.GetPosition(0).y - yDiff, AngleLine1.GetPosition(0).z));
                            FlatLineProjection1.SetPosition(0, new Vector3(AngleLine1.GetPosition(1).x, AngleLine1.GetPosition(1).y - yDiff, AngleLine1.GetPosition(1).z));
                            FlatLineProjection1.SetPosition(1, new Vector3(AngleLine1.GetPosition(0).x, AngleLine1.GetPosition(1).y - yDiff, AngleLine1.GetPosition(0).z));
                            ProjectionTrace2.SetPosition(0, AngleLine1.GetPosition(1));
                            ProjectionTrace2.SetPosition(1, AngleLineProjection1.GetPosition(0));
                            ProjectionTrace3.SetPosition(0, AngleLine1.GetPosition(0));
                            ProjectionTrace3.SetPosition(1, AngleLineProjection1.GetPosition(1));
                            FlatNormal1 = (FlatLineProjection1.GetPosition(1) - FlatLineProjection1.GetPosition(0)).normalized / 2;
                            ProjectionNormal1 = (AngleLineProjection1.GetPosition(1) - AngleLineProjection1.GetPosition(0)).normalized / 2;
                            if (Vector3.Distance(FlatLineProjection1.GetPosition(0), AngleLine0.GetPosition(0)) > 0.5f)
                            {
                                AngleNormal1 = (AngleLine0.GetPosition(0) - FlatLineProjection1.GetPosition(0)).normalized / 2;
                            }
                            else
                            {
                                AngleNormal1 = (AngleLine0.GetPosition(1) - FlatLineProjection1.GetPosition(0)).normalized / 2;
                            }
                            flatToProjection1 = Vector3.Angle(FlatNormal1, ProjectionNormal1);
                            flatToAngle1 = Vector3.Angle(FlatNormal1, AngleNormal1);
                            projections++;
                            if(projections == 1)
                            {
                                AngleLineProjection0 = AngleLineProjection1;
                                FlatLineProjection0 = FlatLineProjection1;
                                FlatNormal0 = FlatNormal1;
                                AngleNormal0 = AngleNormal1;
                                ProjectionNormal0 = ProjectionNormal1;
                                flatToProjection0 = flatToProjection1;
                                flatToAngle0 = flatToAngle1;
                            }
                            else
                            {
                                flatToFlat = Vector3.Angle(FlatNormal0, FlatNormal1);
                                FlatLineProjection0.SetPosition(0, FlatLineProjection1.GetPosition(0));
                                FlatLineProjection0.SetPosition(1, new Vector3(FlatLineProjection0.GetPosition(1).x, FlatLineProjection1.GetPosition(1).y, FlatLineProjection0.GetPosition(1).z));
                                ProjectionTrace0.SetPosition(1, FlatLineProjection0.GetPosition(0));
                                ProjectionTrace1.SetPosition(1, FlatLineProjection0.GetPosition(1));
                                ProjectionTrace2.SetPosition(1, FlatLineProjection1.GetPosition(0));
                                ProjectionTrace3.SetPosition(1, FlatLineProjection1.GetPosition(1));
                                AngleLineProjection0.enabled = false;
                                AngleLineProjection1.enabled = false;
                            }
                        }

                        //Extend reference lines to fit angle measurement
                        if(Vector3.Distance(FlatLineProjection0.GetPosition(0), FlatLineProjection0.GetPosition(1)) < 0.5f)
                        {
                            FlatLineProjection0.SetPosition(1, ((FlatLineProjection0.GetPosition(1) - FlatLineProjection0.GetPosition(0)).normalized / 1.75f) + FlatLineProjection0.GetPosition(0));
                        }
                        if (Vector3.Distance(FlatLineProjection1.GetPosition(0), FlatLineProjection1.GetPosition(1)) < 0.5f)
                        {
                            FlatLineProjection1.SetPosition(1, ((FlatLineProjection1.GetPosition(1) - FlatLineProjection1.GetPosition(0)).normalized / 1.75f) + FlatLineProjection1.GetPosition(0));
                        }

                        //Draw angles for angle measurement with only one projection
                        if (projections == 1)
                        {
                            if (flatToProjection0 > 3.0f)
                            {
                                Vector3 AngleDirection = ProjectionNormal0 - FlatNormal0;
                                Angle0.SetPosition(0, FlatNormal0 + FlatLineProjection0.GetPosition(0));
                                for (int i = 1; i < 10; i++)
                                {
                                    Angle0.SetPosition(i, ((FlatNormal0 + (AngleDirection / 10) * i).normalized / 2) + FlatLineProjection0.GetPosition(0));
                                }
                                Angle0.SetPosition(10, ProjectionNormal0 + FlatLineProjection0.GetPosition(0));
                                AngleText0.text = flatToProjection0.ToString("F2") + "°";
                                AngleText0.transform.position = Angle0.GetPosition(5);
                            }
                            else
                            {
                                AngleLineProjection0.enabled = false;
                            }
                            if (flatToAngle0 > 3.0f)
                            {
                                Vector3 AngleDirection = AngleNormal0 - FlatNormal0;
                                Angle1.SetPosition(0, FlatNormal0 + FlatLineProjection0.GetPosition(0));
                                for (int i = 1; i < 10; i++)
                                {
                                    Angle1.SetPosition(i, ((FlatNormal0 + (AngleDirection / 10) * i).normalized / 2) + FlatLineProjection0.GetPosition(0));
                                }
                                Angle1.SetPosition(10, AngleNormal0 + FlatLineProjection0.GetPosition(0));
                                AngleText1.text = flatToAngle0.ToString("F2") + "°";
                                AngleText1.transform.position = Angle1.GetPosition(5);
                            }


                        }
                        //Draw angles for measurements with 2 projections
                        else if(projections == 2)
                        {
                            Vector3 AngleDirection = FlatNormal1 - FlatNormal0;
                            Angle0.SetPosition(0, FlatNormal0 + FlatLineProjection0.GetPosition(0));
                            for (int i = 1; i < 10; i++)
                            {
                                Angle0.SetPosition(i, ((FlatNormal0 + (AngleDirection / 10) * i).normalized / 2) + FlatLineProjection0.GetPosition(0));
                            }
                            Angle0.SetPosition(10, FlatNormal1 + FlatLineProjection0.GetPosition(0));
                            AngleText0.text = flatToFlat.ToString("F2") + "°";
                            AngleText0.transform.position = Angle0.GetPosition(5);
                        }

                    }
                }
            }
            
            #endregion

        }

        else
        {
            if (myCam.GetComponent<CameraController>() != null)
            {
                if (!myCam.GetComponent<CameraController>().enabled)
                {
                    myCam.GetComponent<CameraController>().enabled = true;
                    myCam.transform.parent = transform.Find("CameraOrbit");
                    myCam.transform.localPosition = new Vector3(0, 5000, 0);
                    myCam.transform.localEulerAngles = new Vector3(90, 0, 0);
                    myCam.GetComponent<Camera>().orthographic = true;
                    myCam.GetComponent<Camera>().farClipPlane = 999999;
                }
            }
        }

        #region Keyboard Input

        if (Input.GetMouseButtonDown(1) || Input.GetKeyDown("joystick button 1"))
        {
            if (isMeasuringDistance)
            {
                isMeasuringDistance1stPoint = false;
                isMeasuringDistance2ndPoint = false;
                isMeasuringDistance = false;
                isIndicating = false;
                GameObject measurement = measureDistancePrefabs[measureDistancePrefabs.Count - 1];
                measureDistancePrefabs.Remove(measurement);
                Destroy(measurement);
            }
            else if (isMeasuringAngle)
            {
                isMeasuringAngle1stPoint = false;
                isMeasuringAngle2ndPoint = false;
                isMeasuringAngle3rdPoint = false;
                isMeasuringAngle4thPoint = false;
                isMeasuringAngle = false;
                isIndicating = false;
                GameObject measurement = measureAnglePrefabs[measureAnglePrefabs.Count - 1];
                measureAnglePrefabs.Remove(measurement);
                Destroy(measurement);
            }
            else if (hasMeasuredAngle)
            {
                hasMeasuredAngle = false;
                isMeasuringAngle = false;
            }
            else if (selectedObject != null)
            {
                paintColours(true);
                selectedObject = null;
            }

            int lay = LayerMask.NameToLayer("Detail Object");
            int lay2 = LayerMask.NameToLayer("Detail Object ROV");
            int layermask1 = (1 << lay) | (1 << lay2);

            RaycastHit hit;
            Ray ray = new Ray(transform.position, myCam.transform.forward);
            if (Physics.Raycast(ray, out hit, 2500.0f, layermask1))
            {
                if(hit.transform.name == "HypotenuseText" || hit.transform.name == "VerticalText" || hit.transform.name == "HorizontalText")
                {
                    measureDistancePrefabs.Remove(hit.transform.parent.gameObject);
                    Destroy(hit.transform.parent.gameObject);
                }
                if(hit.transform.name == "Angle0" || hit.transform.name == "Angle1" || hit.transform.name == "Angle2")
                {
                    measureAnglePrefabs.Remove(hit.transform.parent.gameObject);
                    Destroy(hit.transform.parent.gameObject);
                }
            }
        }

        float zoom = Input.GetAxis("Mouse ScrollWheel");
        if (!isOrthoMode)
        {
            if (zoom < 0)
            {
                myCam.fieldOfView += 3;
                if (myCam.fieldOfView > 60)
                {
                    myCam.fieldOfView = 60;
                }
            }
            else if (zoom > 0)
            {
                myCam.fieldOfView -= 3;
                if (myCam.fieldOfView < 1)
                {
                    myCam.fieldOfView = 1;
                }
            }
        }

        if (Input.GetKey("joystick button 5"))
        {
            myCam.fieldOfView -= .3f;
            if (myCam.fieldOfView < 1)
            {
                myCam.fieldOfView = 1;
            }
        }
        else if (Input.GetKey("joystick button 4"))
        {
            myCam.fieldOfView += .3f;
            if (myCam.fieldOfView > 60)
            {
                myCam.fieldOfView = 60;
            }
        }

        #region Animation

        int targetIndex = -1;
        if (Input.GetKeyDown(KeyCode.Alpha1)) { targetIndex = 0; }
        else if (Input.GetKeyDown(KeyCode.Alpha2)) { targetIndex = 1; }
        else if (Input.GetKeyDown(KeyCode.Alpha3)) { targetIndex = 2; }
        else if (Input.GetKeyDown(KeyCode.Alpha4)) { targetIndex = 3; }
        else if (Input.GetKeyDown(KeyCode.Alpha5)) { targetIndex = 4; }

        if (targetIndex >= 0)
        {
            if (selectedObject != null)
            {
                Animation anim = selectedObject.GetComponentInChildren<Animation>();
                int c = 0;
                foreach (AnimationState state in anim)
                {
                    if (c == targetIndex)
                    {
                        if (anim.IsPlaying(state.clip.name))
                        {
                            if (state.speed > 0)
                            {
                                state.speed = 0;
                            }
                            else
                            {
                                state.speed = 1;
                            }

                        }
                        else
                        {
                            anim.Play(state.clip.name);
                        }
                    }
                    c++;
                }
            }
        }

        #endregion

        #region Animation For Xbox

        if (Input.GetAxis("Xbox_Dpad_Horizontal") != 0)
        {
            if (!isDpadDown)
            {
                isDpadClicked = true;
            }
            else
            {
                isDpadClicked = false;
            }
            isDpadDown = true;
        }
        else
        {
            isDpadDown = false;
            isDpadClicked = false;
        }

        if (isDpadClicked && selectedObject != null)
        {
            if (Input.GetAxis("Xbox_Dpad_Horizontal") > 0)
            {
                animIndex++;
                Animation anim = selectedObject.GetComponentInChildren<Animation>();
                int c = 0;
                foreach (AnimationState state in anim)
                {
                    c++;
                }

                if (animIndex > (c - 1))
                {
                    animIndex = 0;
                }

                c = 0;
                foreach (AnimationState state in anim)
                {
                    if (c == animIndex)
                    {
                        if (anim.IsPlaying(state.clip.name))
                        {
                            if (state.speed > 0)
                            {
                                state.speed = 0;
                            }
                            else
                            {
                                state.speed = 1;
                            }

                        }
                        else
                        {
                            anim.Play(state.clip.name);
                        }
                    }
                    c++;
                }
            }
            else
            {
                Animation anim = selectedObject.GetComponentInChildren<Animation>();
                int c = 0;
                foreach (AnimationState state in anim)
                {
                    if (c == animIndex)
                    {
                        if (anim.IsPlaying(state.clip.name))
                        {
                            if (state.speed > 0)
                            {
                                state.speed = 0;
                            }
                            else
                            {
                                state.speed = 1;
                            }

                        }
                        else
                        {
                            anim.Play(state.clip.name);
                        }
                    }
                    c++;
                }
            }
        }

        #endregion

        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown("joystick button 7"))
        {
            prevXboxKnobPos = Vector3.zero;
            SetPauseMenu(true);
        }

        if ((Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown("joystick button 8")))
        {
            MinMaxLeftPanel();
        }

        if ((Input.GetKeyDown(KeyCode.C) || Input.GetKeyDown("joystick button 9")))
        {
            MinMaxRightPanel();
        }

        if (Input.GetKeyDown(KeyCode.U))
        {
            //myMaster.GetComponent<MasterController>().Undo();
        }

        if (Input.GetKey(KeyCode.P))
        {
            changeSceneTimer += Time.deltaTime;

            if (changeSceneTimer >= 4)
            {
                if (SceneManager.GetActiveScene().name.Equals("BFN_Scene"))
                {
                    SceneManager.LoadScene("PC_Scene");
                }
                else if (SceneManager.GetActiveScene().name.Equals("PC_Scene"))
                {
                    SceneManager.LoadScene("LIQ_Scene");
                }
                else if (SceneManager.GetActiveScene().name.Equals("LIQ_Scene"))
                {
                    SceneManager.LoadScene("BFN_Scene");
                }
            }
        }
        else
        {
            changeSceneTimer = 0;
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            isOrthoMode = !isOrthoMode;
            
            if(!isOrthoMode && LayoutDrawing != null)
            {
                //Debug.Log("k2");
                if (LayoutDrawing.GetComponent<SpriteRenderer>().color.a > 0)
                {
                    LayoutDrawing.GetComponent<DrawingController>().isFadingOut = true;
                    LayoutDrawing.GetComponent<DrawingController>().isFadingIn = false;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            StartMeasureAngle();
        }
        #endregion
    }

    void UpdatePauseMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown("joystick button 7") || Input.GetKeyDown("joystick button 1"))
        {
            inPauseMenu = false;
            PauseMenu.SetActive(false);
        }

        if (/*Input.GetKeyDown(KeyCode.R) ||*/ Input.GetKeyDown("joystick button 6"))
        {
            inPauseMenu = false;
            PauseMenu.SetActive(false);
        }

        if (isUsingController)
        {

            PauseMenu.transform.Find("XboxKnob").gameObject.SetActive(true);
            ControlXboxKnob(PauseMenu);

            PointerEventData pdata = new PointerEventData(EventSystem.current);
            List<RaycastResult> results = new List<RaycastResult>();

            pdata.position = PauseMenu.transform.Find("XboxKnob").transform.GetComponent<RectTransform>().position;
            EventSystem.current.RaycastAll(pdata, results);

            int count = 0;
            int count2 = 0;
            for (int i = 0; i < results.Count; i++)
            {
                if (results[i].gameObject.tag.ToString().Equals("Button"))
                {
                    count++;
                    selectedButton = results[i].gameObject.transform.GetComponent<Button>();
                    if (selectedButton.transform.GetComponent<Image>())
                    {
                        //selectedButton.transform.GetComponent<Image>().color = new Color(1, 1, 0);
                    }
                }

                if (results[i].gameObject.tag.ToString().Equals("ScrollBar"))
                {
                    count2++;
                    selectedScrollbar = results[i].gameObject;
                }
            }

            if (count == 0)
            {
                selectedButton = null;
            }


            if (Input.GetKeyDown("joystick button 0") && selectedButton != null)
            {
                selectedButton.onClick.Invoke();
            }

        }
        else
        {
            PauseMenu.transform.Find("XboxKnob").gameObject.SetActive(false);
        }
    }

    void UpdateOptionsMenu()
    {
        OptionsMenu.transform.Find("SensitivityText").GetComponent<Text>().text = "Camera Sensitivity : " + cameraSensitivity;
        OptionsMenu.transform.Find("QualityText").GetComponent<Text>().text = "Quality : " + QualitySettings.names[qualityIndex];

        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown("joystick button 7") || Input.GetKeyDown("joystick button 1"))
        {
            SetOptionsMenu(false);
            SetPauseMenu(true);
        }

        if (/*Input.GetKeyDown(KeyCode.R) ||*/ Input.GetKeyDown("joystick button 6"))
        {
            SetOptionsMenu(false);
        }

        if (isUsingController)
        {
            OptionsMenu.transform.Find("XboxKnob").gameObject.SetActive(true);
            ControlXboxKnob(OptionsMenu);

            PointerEventData pdata = new PointerEventData(EventSystem.current);
            List<RaycastResult> results = new List<RaycastResult>();

            pdata.position = OptionsMenu.transform.Find("XboxKnob").transform.GetComponent<RectTransform>().position;
            EventSystem.current.RaycastAll(pdata, results);

            int count = 0;
            int count2 = 0;
            for (int i = 0; i < results.Count; i++)
            {
                if (results[i].gameObject.tag.ToString().Equals("Button"))
                {
                    count++;
                    selectedButton = results[i].gameObject.transform.GetComponent<Button>();
                    if (selectedButton.transform.GetComponent<Image>())
                    {
                        //selectedButton.transform.GetComponent<Image>().color = new Color(1, 1, 0);
                    }
                }

                if (results[i].gameObject.tag.ToString().Equals("ScrollBar"))
                {
                    count2++;
                    selectedScrollbar = results[i].gameObject;
                }
            }

            if (count == 0)
            {
                selectedButton = null;
            }


            if (Input.GetKeyDown("joystick button 0") && selectedButton != null)
            {
                selectedButton.onClick.Invoke();
            }

        }
        else
        {
            OptionsMenu.transform.Find("XboxKnob").gameObject.SetActive(false);
        }
    }

    void UpdateControlsMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown("joystick button 7") || Input.GetKeyDown("joystick button 1"))
        {
            SetControlsMenu(false);
            SetPauseMenu(true);
        }

        if (/*Input.GetKeyDown(KeyCode.R) ||*/ Input.GetKeyDown("joystick button 6"))
        {
            SetControlsMenu(false);
        }

        if (isUsingController)
        {
            ControlsMenu.transform.Find("XboxImg").gameObject.SetActive(true);
            ControlsMenu.transform.Find("KeyboardImg").gameObject.SetActive(false);
            ControlsMenu.transform.Find("XboxKnob").gameObject.SetActive(true);
            ControlXboxKnob(ControlsMenu);

            PointerEventData pdata = new PointerEventData(EventSystem.current);
            List<RaycastResult> results = new List<RaycastResult>();

            pdata.position = ControlsMenu.transform.Find("XboxKnob").transform.GetComponent<RectTransform>().position;
            EventSystem.current.RaycastAll(pdata, results);

            int count = 0;
            int count2 = 0;
            for (int i = 0; i < results.Count; i++)
            {
                if (results[i].gameObject.tag.ToString().Equals("Button"))
                {
                    count++;
                    selectedButton = results[i].gameObject.transform.GetComponent<Button>();
                    if (selectedButton.transform.GetComponent<Image>())
                    {
                        //selectedButton.transform.GetComponent<Image>().color = new Color(1, 1, 0);
                    }
                }

                if (results[i].gameObject.tag.ToString().Equals("ScrollBar"))
                {
                    count2++;
                    selectedScrollbar = results[i].gameObject;
                }
            }

            if (count == 0)
            {
                selectedButton = null;
            }


            if (Input.GetKeyDown("joystick button 0") && selectedButton != null)
            {
                selectedButton.onClick.Invoke();
            }

        }
        else
        {
            ControlsMenu.transform.Find("XboxImg").gameObject.SetActive(false);
            ControlsMenu.transform.Find("KeyboardImg").gameObject.SetActive(true);
            ControlsMenu.transform.Find("XboxKnob").gameObject.SetActive(false);
        }
    }

    void UpdateCursorMode()
    {
        if (isUsingController)
        {
            if (!isScrollingVert && !isScrollingHoriz)
            {

                HUD.transform.Find("XboxKnob").gameObject.SetActive(true);
                ControlXboxKnob(HUD);

                PointerEventData pdata = new PointerEventData(EventSystem.current);
                List<RaycastResult> results = new List<RaycastResult>();

                pdata.position = HUD.transform.Find("XboxKnob").transform.GetComponent<RectTransform>().position;
                EventSystem.current.RaycastAll(pdata, results);

                int count = 0;
                int count2 = 0;
                for (int i = 0; i < results.Count; i++)
                {
                    if (results[i].gameObject.tag.ToString().Equals("Button"))
                    {
                        count++;
                        selectedButton = results[i].gameObject.transform.GetComponent<Button>();
                        if (selectedButton.transform.GetComponent<Image>())
                        {
                            selectedButton.transform.GetComponent<Image>().color = new Color(1, 1, 0);
                        }
                    }

                    if (results[i].gameObject.tag.ToString().Equals("ScrollBarVert") || results[i].gameObject.tag.ToString().Equals("ScrollBarHoriz"))
                    {
                        count2++;
                        selectedScrollbar = results[i].gameObject;
                    }
                }

                if (count == 0)
                {
                    selectedButton = null;
                }

                if (count2 == 0)
                {
                    selectedScrollbar = null;
                }

                if (Input.GetKeyDown("joystick button 0"))
                {
                    if (selectedButton != null)
                    {
                        if (selectedButton.transform.GetComponent<Image>())
                        {
                            selectedButton.transform.GetComponent<Image>().color = new Color(1, 1, 1);
                        }
                        selectedButton.onClick.Invoke();
                    }
                    else if (selectedScrollbar != null)
                    {
                        if (selectedScrollbar.transform.tag.Equals("ScrollBarVert"))
                        {
                            isScrollingVert = true;
                            selectedScrollbar.transform.Find("Sliding Area").Find("Handle").GetComponent<Image>().color = Color.yellow;
                        }
                        else
                        {
                            isScrollingHoriz = true;
                            selectedScrollbar.transform.Find("Sliding Area").Find("Handle").GetComponent<Image>().color = Color.blue;
                            //myMaster.GetComponent<MasterController>().PauseSceneAnimation();
                        }
                    }
                }

                //prevXboxKnobPos = HUD.transform.Find("XboxKnob").position;
            }
            else
            {
                if (Input.GetKey("joystick button 0"))
                {
                    HUD.transform.Find("XboxKnob").gameObject.SetActive(false);
                    if (isScrollingVert)
                    {
                        selectedScrollbar.transform.parent.Find("ScrollRect").Find("Panel").GetComponent<RectTransform>().position += new Vector3(0, Input.GetAxis("Xbox_LS_Vertical") * Time.deltaTime * -400, 0);
                    }
                    else
                    {
                        if ((Input.GetAxis("Xbox_RT") > .2f))
                        {
                            selectedScrollbar.GetComponent<Scrollbar>().value += Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime * .8f;
                        }
                        else if ((Input.GetAxis("Xbox_LT") > .2f))
                        {
                            selectedScrollbar.GetComponent<Scrollbar>().value += Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime * .05f;
                        }
                        else
                        {
                            selectedScrollbar.GetComponent<Scrollbar>().value += Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime * .2f;
                        }
                    }
                }
                else
                {
                    if (isScrollingHoriz)
                    {
                        selectedScrollbar.transform.Find("Sliding Area").Find("Handle").GetComponent<Image>().color = Color.yellow;
                    }
                    else
                    {
                        selectedScrollbar.transform.Find("Sliding Area").Find("Handle").GetComponent<Image>().color = Color.white;
                    }
                    isScrollingVert = false;
                    isScrollingHoriz = false;
                    selectedButton = null;
                    selectedScrollbar = null;
                }
            }
        }
        else
        {
            isScrollingVert = false;
            isScrollingHoriz = false;
            selectedButton = null;
            selectedScrollbar = null;
            HUD.transform.Find("XboxKnob").gameObject.SetActive(false);
        }

        if (/*Input.GetKeyDown(KeyCode.R) ||*/ Input.GetKeyDown("joystick button 6"))
        {
            SetControlsMenu(false);
        }

        
    }

    #endregion

    #region UI Menus Methods

    public void SetPauseMenu(bool activate)
    {
        if (activate)
        {
            inPauseMenu = true;
            PauseMenu.SetActive(true);
            PauseMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().localPosition = prevXboxKnobPos;
        }
        else
        {
            inPauseMenu = false;
            PauseMenu.SetActive(false);
        }
    }

    public void SetOptionsMenu(bool activate)
    {
        if (activate)
        {
            inOptionsMenu = true;
            OptionsMenu.SetActive(true);
            OptionsMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().localPosition = prevXboxKnobPos;
        }
        else
        {
            inOptionsMenu = false;
            OptionsMenu.SetActive(false);
        }
    }

    public void SetControlsMenu(bool activate)
    {
        if (activate)
        {
            inControlsMenu = true;
            ControlsMenu.SetActive(true);
            ControlsMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().localPosition = prevXboxKnobPos;
        }
        else
        {
            inControlsMenu = false;
            ControlsMenu.SetActive(false);
        }
    }

    public void ControlXboxKnob(GameObject CurrentMenu)
    {

        float xScale = Screen.width / startingScreenSize.x;
        float yScale = Screen.height / startingScreenSize.y;

        if ((Input.GetAxis("Xbox_RT") > .2f))
        {
            CurrentMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().position += new Vector3(Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime * 1000, Input.GetAxis("Xbox_LS_Vertical") * Time.deltaTime * 800, 0);
        }
        else if ((Input.GetAxis("Xbox_LT") > .2f))
        {
            CurrentMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().position += new Vector3(Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime * 200, Input.GetAxis("Xbox_LS_Vertical") * Time.deltaTime * 200, 0);
        }
        else
        {
            CurrentMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().position += new Vector3(Input.GetAxis("Xbox_LS_Horizontal") * Time.deltaTime * 400, Input.GetAxis("Xbox_LS_Vertical") * Time.deltaTime * 400, 0);
        }

        if (CurrentMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().position.x < 0)
        {
            CurrentMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().position = new Vector3(0, CurrentMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().position.y);
        }

        if (CurrentMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().position.x > Screen.width - (CurrentMenu.transform.Find("XboxKnob").GetChild(0).GetComponent<RectTransform>().sizeDelta.x * xScale))
        {
            CurrentMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().position = new Vector3(Screen.width - (CurrentMenu.transform.Find("XboxKnob").GetChild(0).GetComponent<RectTransform>().sizeDelta.x * xScale), CurrentMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().position.y);
        }

        if (CurrentMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().position.y < (CurrentMenu.transform.Find("XboxKnob").GetChild(0).GetComponent<RectTransform>().sizeDelta.y * yScale))
        {
            CurrentMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().position = new Vector3(CurrentMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().position.x, (CurrentMenu.transform.Find("XboxKnob").GetChild(0).GetComponent<RectTransform>().sizeDelta.y * yScale));
        }

        if (CurrentMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().position.y > Screen.height)
        {
            CurrentMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().position = new Vector3(CurrentMenu.transform.Find("XboxKnob").GetComponent<RectTransform>().position.x, Screen.height);
        }

        if (!CurrentMenu.transform.name.Equals("HUD"))
        {
            prevXboxKnobPos = CurrentMenu.transform.Find("XboxKnob").localPosition;
        }

    }

    #endregion

    public void paintColours(bool defaultColour)
    {
        if (selectedObject != null)
        {
            Renderer[] rends = selectedObject.GetComponentsInChildren<Renderer>();
            for (int ii = 0; ii < rends.Length; ii++)
            {
                if (rends[ii].transform.localScale.x > .04f)
                {
                    if (defaultColour)
                    {
                        if (rends[ii].materials.Length - 1 > 0)
                        {
                            Material[] mats = new Material[rends[ii].materials.Length - 1];

                            for (int x = 0; x < mats.Length; x++)
                            {
                                mats[x] = rends[ii].materials[x];
                            }

                            rends[ii].materials = mats;
                        }
                    }
                    else
                    {

                        Material[] mats = new Material[rends[ii].materials.Length + 1];

                        for (int x = 0; x < mats.Length - 1; x++)
                        {
                            mats[x] = rends[ii].materials[x];
                        }

                        mats[mats.Length - 1] = outlineMat;
                        rends[ii].materials = mats;
                    }
                }
            }
        }
    }


    #region Scene Manipulation Methods

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void StartMeasure()
    {
        isMeasuringDistance = true;
        isMeasuringDistance1stPoint = true;
        isIndicating = true;
        GameObject measurement = Instantiate(measureDistancePrefab);
        measureDistancePrefabs.Add(measurement);
        IndicatorText.text = "Action : Measuring";
        IndicatorMenu.SetActive(true);
        transform.GetComponent<AudioSource>().Play();
    }

    public void StartMeasureAngle()
    {
        isMeasuringAngle = true;
        isMeasuringAngle1stPoint = true;
        isIndicating = true;
        GameObject measurement = Instantiate(measureAnglePrefab);
        measureAnglePrefabs.Add(measurement);
        IndicatorText.text = "Action: Measuring Angle";
        IndicatorMenu.SetActive(true);
        transform.GetComponent<AudioSource>().Play();
    }

    public void StartScreenshot()
    {
        cameraTimer = 0;
        isIndicating = true;
        isIndicatingVisible = true;
        IndicatorText.text = "Picture in : " + (int)(5 - cameraTimer);
        isTakingPicture = true;
        transform.GetComponent<AudioSource>().Play();
    }

    public void ToggleDayNight()
    {
        
        isDay = !isDay;

        if (isDay)
        {
            if (myMaster.GetComponent<MasterController>().isUnderwaterScene)
            {
                //RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Skybox;
                RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Flat;
                RenderSettings.ambientSkyColor = new Color(.4f, .43f, .5f);
                RenderSettings.ambientIntensity = 1f;
                RenderSettings.reflectionIntensity = .5f;
                RenderSettings.fogDensity = .015f;
                RenderSettings.fog = true;
                myCam.GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
                myCam.GetComponent<Camera>().backgroundColor = new Color(19.0f / 255.0f, 28.0f / 255.0f, 54.0f / 255.0f);
                RenderSettings.fogColor = new Color(19.0f / 255.0f, 28.0f / 255.0f, 54.0f / 255.0f);
                RenderSettings.skybox = (Material)Resources.Load("WispySkyboxMat");

                if (ROV != null)
                {
                    ROV.transform.Find("3rdPersonCam").GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
                    ROV.transform.Find("3rdPersonCam").GetComponent<Camera>().backgroundColor = new Color(19.0f / 255.0f, 28.0f / 255.0f, 54.0f / 255.0f);
                }
            }
            else
            {
                RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Skybox;
                RenderSettings.ambientIntensity = 1f;
                RenderSettings.reflectionIntensity = .5f;
                //RenderSettings.fogDensity = .015f;
                RenderSettings.fog = false;
                myCam.GetComponent<Camera>().clearFlags = CameraClearFlags.Skybox;
                RenderSettings.skybox = (Material)Resources.Load("WispySkyboxMat");
            }

            

            if (DayPP_Profile != null)
            {
                DayPP_Profile.SetActive(true);
            }
            if (NightPP_Profile != null)
            {
                NightPP_Profile.SetActive(false);
            }
            if(ROVPP_Profile != null)
            {
                ROVPP_Profile.SetActive(false);
            }

            if (myCam.transform.Find("PlanktonParticles"))
            {
                myCam.transform.Find("PlanktonParticles").gameObject.SetActive(false);
            }

            foreach (GameObject day_light in Day_Lights_List)
            {
                if (day_light != null)
                {
                    day_light.SetActive(true);
                }
                else
                {
                    Debug.LogError("Day Light is null.");
                }
            }

            foreach (GameObject night_light in Night_Lights_List)
            {

                if (night_light != null)
                {
                    night_light.SetActive(false);
                }
                else
                {
                    Debug.LogError("Night Light is null.");
                }
            }
        }
        else
        {
            if (myMaster.GetComponent<MasterController>().isUnderwaterScene)
            {
                RenderSettings.ambientIntensity = 0f;
                RenderSettings.reflectionIntensity = 0f;


                myCam.GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
                RenderSettings.fogDensity = .3f;
                RenderSettings.fog = true;
                RenderSettings.fogColor = Color.black;
                myCam.GetComponent<Camera>().backgroundColor = Color.black;

                if (myCam.transform.Find("PlanktonParticles"))
                {
                    myCam.transform.Find("PlanktonParticles").gameObject.SetActive(true);
                }

                if (ROV != null)
                {
                    ROV.transform.Find("3rdPersonCam").GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
                    ROV.transform.Find("3rdPersonCam").GetComponent<Camera>().backgroundColor = Color.black;
                }
            }
            else
            {
                RenderSettings.ambientIntensity = .3f;
                RenderSettings.reflectionIntensity = 0f;
                RenderSettings.skybox = (Material)Resources.Load("MilkyWay");

                myCam.GetComponent<Camera>().clearFlags = CameraClearFlags.Skybox;
                RenderSettings.fogDensity = .03f;
                RenderSettings.fog = false;
            }

            if (DayPP_Profile != null)
            {
                DayPP_Profile.SetActive(false);
            }
            if (NightPP_Profile != null)
            {
                NightPP_Profile.SetActive(true);
            }
            if (ROVPP_Profile != null)
            {
                ROVPP_Profile.SetActive(false);
            }


            if (ROV != null)
            {
                ROV.transform.Find("3rdPersonCam").GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
            }

            foreach (GameObject day_light in Day_Lights_List)
            {
                if (day_light != null)
                {
                    day_light.SetActive(false);
                }
                else
                {
                    Debug.LogError("Day Light is null.");
                }
            }

            foreach (GameObject night_light in Night_Lights_List)
            {
                if (night_light != null)
                {
                    night_light.SetActive(true);
                }
                else
                {
                    Debug.LogError("Night Light is null.");
                }
            }
        }
        
    }

    public void StartClipping()
    {
        if (transform.GetChild(0).GetComponent<Camera>().nearClipPlane > .5f)
        {
            transform.GetChild(0).GetComponent<Camera>().nearClipPlane = .3f;
        }
        else
        {
            transform.GetChild(0).GetComponent<Camera>().nearClipPlane = 1f;
        }
        transform.GetComponent<AudioSource>().Play();
    }

    public void ToggleThirdPerson()
    {
        SetOptionsMenu(false);
        SetPauseMenu(false);
        inActionMenu = false;
        inOptionsMenu = false;
        inPauseMenu = false;
        inCursorMode = false;
        ButtonPanel.GetComponent<ButtonPanelController>().MinWindow();
        isThirdPerson = !isThirdPerson;

        if (isThirdPerson)
        {
            myCam.gameObject.SetActive(false);
            ROV.transform.Find("3rdPersonCam").gameObject.SetActive(true);


            if (selectedObject == ROV)
            {
                paintColours(true);
                Debug.Log("painted ROV");
            }
            else
            {
                paintColours(true);
                selectedObject = ROV;
            }

            if (!isDay && ROVPP_Profile != null)
            {
                ROVPP_Profile.SetActive(true);
                if(NightPP_Profile != null)
                {
                    NightPP_Profile.SetActive(false);
                }
                if(DayPP_Profile != null)
                {
                    DayPP_Profile.SetActive(false);
                }
            }


            Player_MK_UI.SetActive(false);

            if (ROV.transform.Find("Real_Collision").gameObject.activeSelf)
            {
                ROV.transform.Find("Real_Collision").gameObject.SetActive(false);
                ROV.transform.Find("Detail_Object").gameObject.SetActive(false);
                ROV.layer = 14;
                ROV.transform.GetComponent<Rigidbody>().isKinematic = false;
                ROV.transform.GetComponent<BoxCollider>().isTrigger = false;
            }
            else
            {

            }
        }
        else
        {
            ROV.transform.Find("3rdPersonCam").gameObject.SetActive(false);
            myCam.gameObject.SetActive(true);

            selectedObject = ROV;
            paintColours(false);
            Player_MK_UI.SetActive(true);

            if (isDay)
            {
                if (ROVPP_Profile != null)
                {
                    ROVPP_Profile.SetActive(false);
                }
                if (NightPP_Profile != null)
                {
                    NightPP_Profile.SetActive(false);
                }
                if (DayPP_Profile != null)
                {
                    DayPP_Profile.SetActive(true);
                }
            }
            else
            {
                if (ROVPP_Profile != null)
                {
                    ROVPP_Profile.SetActive(false);
                }
                if (NightPP_Profile != null)
                {
                    NightPP_Profile.SetActive(true);
                }
                if (DayPP_Profile != null)
                {
                    DayPP_Profile.SetActive(false);
                }
            }

            if (!ROV.transform.Find("Lite_Collision").gameObject.activeSelf)
            {
                ROV.transform.Find("Real_Collision").gameObject.SetActive(true);
                ROV.layer = 10;
                ROV.transform.GetComponent<Rigidbody>().isKinematic = true;
                ROV.transform.GetComponent<BoxCollider>().isTrigger = true;
                ROV.transform.Find("Detail_Object").gameObject.SetActive(true);
            }

        }

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;


    }

    #endregion

    #region Player Manipulation Methods

    public void ChangeQualitySetting(bool upQuality)
    {
        if (upQuality)
        {
            if(qualityIndex < 5)
            {
                qualityIndex++;
                QualitySettings.SetQualityLevel(qualityIndex, true);
            }
        }
        else
        {
            if (qualityIndex > 0)
            {
                qualityIndex--;
                QualitySettings.SetQualityLevel(qualityIndex, true);
            }
        }
    }

    public void ChangeCameraSensitivity(bool isAdding)
    {
        if (isAdding)
        {
            cameraSensitivity += 10;
            if (cameraSensitivity >= 200)
            {
                cameraSensitivity = 200;
            }
        }
        else
        {
            cameraSensitivity -= 10;
            if (cameraSensitivity <= 10)
            {
                cameraSensitivity = 10;
            }
        }

        transform.GetComponent<AudioSource>().Play();
    }

    private void isMouseKeyboard()
    {
        // mouse movement
        if (Input.GetAxis("Mouse X") != 0.0f ||
            Input.GetAxis("Mouse Y") != 0.0f)
        {
            isUsingController = false;
        }

    }

    private void isControlerInput()
    {
        // joystick buttons
        if (Input.GetKey(KeyCode.Joystick1Button0) ||
           Input.GetKey(KeyCode.Joystick1Button1) ||
           Input.GetKey(KeyCode.Joystick1Button2) ||
           Input.GetKey(KeyCode.Joystick1Button3) ||
           Input.GetKey(KeyCode.Joystick1Button4) ||
           Input.GetKey(KeyCode.Joystick1Button5) ||
           Input.GetKey(KeyCode.Joystick1Button6) ||
           Input.GetKey(KeyCode.Joystick1Button7) ||
           Input.GetKey(KeyCode.Joystick1Button8) ||
           Input.GetKey(KeyCode.Joystick1Button9) ||
           Input.GetKey(KeyCode.Joystick1Button10) ||
           Input.GetKey(KeyCode.Joystick1Button11) ||
           Input.GetKey(KeyCode.Joystick1Button12) ||
           Input.GetKey(KeyCode.Joystick1Button13) ||
           Input.GetKey(KeyCode.Joystick1Button14) ||
           Input.GetKey(KeyCode.Joystick1Button15) ||
           Input.GetKey(KeyCode.Joystick1Button16) ||
           Input.GetKey(KeyCode.Joystick1Button17) ||
           Input.GetKey(KeyCode.Joystick1Button18) ||
           Input.GetKey(KeyCode.Joystick1Button19))
        {
            isUsingController = true;
        }

        // joystick axis
        if (Input.GetAxis("Xbox_LS_Horizontal") != 0.0f ||
           Input.GetAxis("Xbox_LS_Vertical") != 0.0f ||
           Input.GetAxis("Xbox_RS_Horizontal") != 0.0f ||
           Input.GetAxis("Xbox_RS_Vertical") != 0.0f ||
           Input.GetAxis("Xbox_LT") != 0.0f ||
           Input.GetAxis("Xbox_RT") != 0.0f ||
           Input.GetAxis("Xbox_Dpad_Horizontal") != 0.0f)
        {
            isUsingController = true;
        }

    }

    public void MinMaxLeftPanel()
    {
        if (LeftPanel.activeSelf)
        {
            LeftPanel.SetActive(false);
            //LeftPanel.transform.Find("MinimizeButton").Find("Image").transform.localEulerAngles = new Vector3(0, 0, -90);
            //LeftPanel.transform.GetComponent<RectTransform>().position = new Vector3(5, LeftPanel.transform.GetComponent<RectTransform>().position.y, LeftPanel.transform.GetComponent<RectTransform>().position.z);
        }
        else
        {
            LeftPanel.SetActive(true);
            //LeftPanel.transform.Find("MinimizeButton").Find("Image").transform.localEulerAngles = new Vector3(0, 0, 90);
            //LeftPanel.transform.GetComponent<RectTransform>().position = new Vector3(260, LeftPanel.transform.GetComponent<RectTransform>().position.y, LeftPanel.transform.GetComponent<RectTransform>().position.z);
        }
    }

    public void MinMaxRightPanel()
    {
        if (RightPanel.activeSelf)
        {
            RightPanel.SetActive(false);
            //RightPanel.transform.GetComponent<RectTransform>().localPosition = new Vector3(-227, RightPanel.transform.GetComponent<RectTransform>().localPosition.y, RightPanel.transform.GetComponent<RectTransform>().localPosition.z);
        }
        else
        {
            RightPanel.SetActive(true);
            //RightPanel.transform.GetComponent<RectTransform>().localPosition = new Vector3(227, RightPanel.transform.GetComponent<RectTransform>().localPosition.y, RightPanel.transform.GetComponent<RectTransform>().localPosition.z);
        }
    }

    public void ColorUI(string hex)
    {
        Debug.Log(hex);
        hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
        hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
        byte a = 255;//assume fully visible unless specified in hex
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        //Only use alpha if the string has enough characters
        if (hex.Length == 8)
        {
            a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
        }
        Color newColor = new Color32(r, g, b, a);

        Image[] uiImages = Player_MK_UI.transform.GetComponentsInChildren<Image>();

        foreach (Image img in uiImages)
        {
            if (img.transform.tag.Equals("ColorUI") || img.transform.tag.Equals("InfoBox") || img.transform.tag.Equals("AssetBox"))
            {
                img.color = newColor;
            }
        }

        Text[] uiTexts = Player_MK_UI.transform.GetComponentsInChildren<Text>();

        foreach (Text text in uiTexts)
        {
            if (text.transform.tag.Equals("ColorUI"))
            {
                text.color = newColor;
            }
        }
    }

    #endregion

    #region VPLink Manipulation Methods

    public void CloseDec()
    {/*
        if (selectedObject != null)
        {
            if (selectedObject.GetComponent<SelectableController>().isValve)
            {
                selectedObject.GetComponent<SelectableController>().CloseDec();
                transform.GetComponent<AudioSource>().Play();
            }
        }
        */
    }

    public void CloseFully()
    {
        /*
        if (selectedObject != null)
        {
            transform.GetComponent<AudioSource>().Play();
            selectedObject.GetComponent<SelectableController>().CloseFully();
        }
        */
    }

    public void OpenInc()
    {
        /*
        if (selectedObject != null)
        {
            if (selectedObject.GetComponent<SelectableController>().isValve)
            {
                transform.GetComponent<AudioSource>().Play();
                selectedObject.GetComponent<SelectableController>().OpenInc();
            }
        }
        */
    }

    public void OpenFully()
    {
        /*
        if (selectedObject != null)
        {
            transform.GetComponent<AudioSource>().Play();
            selectedObject.GetComponent<SelectableController>().OpenFully();
        }
        */
    }

    public void OpenAmount(float amount)
    {
        /*
        if (selectedObject != null)
        {
            transform.GetComponent<AudioSource>().Play();
            selectedObject.GetComponent<SelectableController>().OpenAmount(amount);
        }
        */
    }

    public void CloseAmount(float amount)
    {
        /*
        if (selectedObject != null)
        {
            transform.GetComponent<AudioSource>().Play();
            selectedObject.GetComponent<SelectableController>().CloseAmount(amount);
        }
        */
    }

    #endregion

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Equals("Walk_Plane"))
        {
            if (canClimb && Input.GetAxis("Vertical") < -.15f)
            {
                canClimb = false;
                ladderTimeout = 0;
            }
        }
    }

    public void SelectObject(GameObject newObject)
    {
        if (selectedObject != null)
        {
            paintColours(true);
        }
        hasSelected = true;
        hasSelectedTimer = 0;
        selectedObject = newObject;
        outlineMat.SetFloat("_Outline", selectedObject.transform.GetComponent<SelectableController>().outlineWidth);
        paintColours(false);
        //GamePad.SetVibration(0, .5f, .5f);
        vibrateTimer = 0;
        vibrateTime = .05f;
        transform.GetComponent<AudioSource>().Play();

        if (selectedObject.GetComponent<SelectableController>() != null)
        {
            foreach (Transform child in ObjectDataBox.transform.Find("ScrollRect").Find("Panel"))
            {
                Destroy(child.gameObject);
            }
            foreach (string infoValue in selectedObject.GetComponent<SelectableController>().infoValues)
            {
                GameObject newText = Instantiate(Resources.Load("InfoText", typeof(GameObject))) as GameObject;
                newText.GetComponent<Text>().text = infoValue;
                newText.transform.SetParent(ObjectDataBox.transform.Find("ScrollRect").Find("Panel").transform);
                newText.transform.localScale = new Vector3(1, 1, 1);

                GameObject seperator = Instantiate(Resources.Load("SeperatorImage", typeof(GameObject))) as GameObject;
                seperator.transform.SetParent(ObjectDataBox.transform.Find("ScrollRect").Find("Panel").transform);
            }
        }
    }

}