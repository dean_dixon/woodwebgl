﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeMaker : MonoBehaviour {

    public float pipeRadius;

    public int pipeSegmentCount;

    public GameObject dotPrefab;

    public int numPoints;

    private Mesh mesh;
    private Vector3[] vertices;
    private int[] triangles;

    List<GameObject> linePointObjects = new List<GameObject>();

    private void Awake()  
    {
        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        mesh.name = "Pipe";

        /*
        Vector3[] randomPoints = new Vector3[numPoints];
        randomPoints[0] = Vector3.zero;

        for(int x= 0; x < numPoints; x++)
        {
            if(x > 0)
            {
                Vector3 dir = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
                float length = Random.Range(1, 21);

                randomPoints[x] = randomPoints[x - 1] + dir * length;
            }
            
        }

        transform.GetComponent<LineRenderer>().positionCount = numPoints;

        Vector3[] newLinePositions = Curver.MakeSmoothCurve(randomPoints, 1);

        transform.GetComponent<LineRenderer>().SetPositions(newLinePositions);

        drawRings();

        SetVertices();
        SetTriangles();
        mesh.RecalculateNormals();
        */
        
    }

    private void SetVertices()
    {
        vertices = new Vector3[pipeSegmentCount * transform.GetComponent<LineRenderer>().positionCount];

        Debug.Log("Num Vertices = " + vertices.Length);

        for(int x = 0; x < linePointObjects.Count; x++)
        {
            for(int i = 0; i < pipeSegmentCount; i++)
            {
                vertices[(x * pipeSegmentCount) + i] = linePointObjects[x].transform.GetChild(i).position;
            }
        }

        mesh.vertices = vertices;
    }

    private void SetTriangles()
    {
        triangles = new int[pipeSegmentCount * (transform.GetComponent<LineRenderer>().positionCount-1) * 6 + (pipeSegmentCount-2)*6];

        //Debug.Log("Num tris = " + triangles.Length);

        int p = 1;
        for(int x = 0; x< (pipeSegmentCount-2)*3; x+=3)
        {
            triangles[x] = 0;
            triangles[x + 1] = p;
            triangles[x + 2] = p + 1;

            p++;
        }

        p = (pipeSegmentCount * transform.GetComponent<LineRenderer>().positionCount) - pipeSegmentCount;
        for (int x = 0; x < (pipeSegmentCount - 2)*3; x += 3)
        {
            triangles[x + ((pipeSegmentCount-2)*3) ] = (pipeSegmentCount * transform.GetComponent<LineRenderer>().positionCount) - pipeSegmentCount;
            triangles[x + ((pipeSegmentCount - 2) * 3) + 1] = p + 2;
            triangles[x + ((pipeSegmentCount - 2) * 3) + 2] = p + 1;

            p++;
        }

        for (int t = ((pipeSegmentCount - 2) * 6), i = 0; t < triangles.Length; t += 6, i++)
        {
            if ((i + 1) % pipeSegmentCount == 0)
            {
                triangles[t] = triangles[t + 5] = i;
                triangles[t + 1] = i + pipeSegmentCount;
                triangles[t + 2] = triangles[t + 3] = i + pipeSegmentCount + 1 - pipeSegmentCount;
                triangles[t + 4] = i + 1 - pipeSegmentCount;
            }
            else
            {
                triangles[t] = triangles[t + 5] = i;
                triangles[t + 1] = i + pipeSegmentCount;
                triangles[t + 2] = triangles[t + 3] = i + pipeSegmentCount + 1;
                triangles[t + 4] = i + 1;
            }

            //Debug.Log(t + 6);
            //Debug.Log(i + pipeSegmentCount + 1);
        }



        mesh.triangles = triangles;
    }

    // Use this for initialization
    void Start () {
        //drawRings();
	}

    public void makeMesh()
    {
        drawRings();

        SetVertices();
        SetTriangles();
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        transform.GetComponent<MeshRenderer>().enabled = true;

        foreach(GameObject g in linePointObjects)
        {
            Destroy(g);
        }

        linePointObjects.Clear();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private Vector3 GetPointOnTorus(float u, float v)
    {
        Vector3 p;
        float r = (pipeRadius * Mathf.Cos(v));
        p.x = r * Mathf.Sin(u);
        p.y = r * Mathf.Cos(u);
        p.z = pipeRadius * Mathf.Sin(v);
        return p;
    }

    void drawRings()
    {
        float vStep = (2f * Mathf.PI) / pipeSegmentCount;

        float colorStep = 1.0f / (float)pipeSegmentCount;

        //Debug.Log(colorStep);

        for (int n = 0; n < transform.GetComponent<LineRenderer>().positionCount; n++)
        {
            GameObject newPoint = new GameObject("linePoint");
            newPoint.transform.position = transform.GetComponent<LineRenderer>().GetPosition(n);
            float r = 0;
            float g = 0;
            float b = 0;

            for (int v = 0; v < pipeSegmentCount; v++)
            {
                Vector3 point = GetPointOnTorus(0, v * vStep) + transform.GetComponent<LineRenderer>().GetPosition(n);
                GameObject newVertex = Instantiate(dotPrefab, point, Quaternion.identity);
                newVertex.transform.SetParent(newPoint.transform);

                float p = (float)pipeSegmentCount;
                float z = (float)v;
                if (z / p < .166f)
                {
                    r += colorStep * 6;
                    g = 0;
                    b = 0;
                }
                else if (z / p < .333f)
                {
                    r = 1;
                    g += colorStep * 6;
                    b = 0;
                }
                else if (z / p < .5f)
                {
                    r -= colorStep * 6;
                    g = 1;
                    b = 0;
                }
                else if (z / p < .666f)
                {
                    r = 0;
                    g = 1;
                    b += colorStep * 6;
                }
                else if (z / p < .833f)
                {
                    r = 0;
                    g -= colorStep * 6;
                    b = 1;
                }
                else
                {
                    r = 0;
                    g = 0;
                    b -= colorStep * 6;
                }

                newVertex.transform.GetComponent<MeshRenderer>().material.color = new Color(r, g, b);
            }

            if(n == 0)
            {
                newPoint.transform.LookAt(transform.GetComponent<LineRenderer>().GetPosition(n + 1));
            }
            else if(n < transform.GetComponent<LineRenderer>().positionCount-1)
            {
                Vector3 dir = transform.GetComponent<LineRenderer>().GetPosition(n + 1) - transform.GetComponent<LineRenderer>().GetPosition(n - 1);
                newPoint.transform.forward = dir.normalized;
            }
            else
            {
                Vector3 dir = transform.GetComponent<LineRenderer>().GetPosition(n) - transform.GetComponent<LineRenderer>().GetPosition(n - 1);
                newPoint.transform.forward = dir.normalized;
            }

            newPoint.transform.Rotate(Vector3.up, 90);

            linePointObjects.Add(newPoint);
        }
    }
}
