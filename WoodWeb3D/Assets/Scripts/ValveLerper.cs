﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ValveLerper : MonoBehaviour {

    public bool isLever;
    public bool isX;
    public bool isY;
    public bool isZ;
    public bool turnNegative;
    private Quaternion openOrientation, closedOrientation, targetOrientation;
    private Vector3 rotateAxis;
    public float currentAngle;

    private bool isTurning;

    private float targetAngle;  // should be initialized and set in StartLerp
    
    private float currentValvePosition;  // in units the valve cares about (0-1 or 0-100)
    [HideInInspector]
    public float angularRate;
    private float positionRate;
    private float wayThere; // Seconds we have been turning, lerping stops when this time reaches TurnTime
    private const float TurnTime = 3.0f;
    private Lerping savedL;
    public delegate void Lerping( float theCurrentValue );

    public GameObject myPump;

    void Awake()
    {
        openOrientation = transform.rotation;
        if (isX)
        {
            rotateAxis = Vector3.left;
        }
        else if (isY)
        {
            rotateAxis = Vector3.up;
        }
        else if (isZ)
        {
            rotateAxis = Vector3.forward;
        }

        if(isLever)
        {
          closedOrientation = openOrientation * Quaternion.AngleAxis(turnNegative ? -90 : 90, rotateAxis);
        }
        else
        {
            if(turnNegative)
            {
                rotateAxis *= -1;
            }
        }

        angularRate = 90 / TurnTime;
    }

    // Use this for initialization
    void Start()
    {
        VPDatabase vpd;
        vpd = FindObjectOfType<VPDatabase>();
        savedL = null;
    }
	
    public void InitializeValvePosition(float position)
    {
        if (isLever)
        {
            if(position == 0)
            {
                transform.rotation = closedOrientation;
                currentValvePosition = 0;
            }
            else
            {
                transform.rotation = openOrientation;
                currentValvePosition = 1;
            }
            
            wayThere = 0.0f;
            targetAngle = currentAngle;
            
        }
        else
        {
            currentValvePosition = position;
            currentAngle = (position * (5 / 100f)) * 360f;
            wayThere = 0.0f;
            targetAngle = currentAngle;
        }
    }

	// Update is called once per frame
	void Update () {

        if (isTurning)
        {
            float angularChange = angularRate * Time.deltaTime;
            wayThere += Time.deltaTime;
            

            if (isLever)
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, targetOrientation, 30*Time.deltaTime);

                if (wayThere >= TurnTime || targetOrientation == transform.rotation)
                {
                    isTurning = false;
                    wayThere = 0.0f;
                    currentAngle = targetAngle;
                }
            }
            else
            {
                Quaternion amtToRotate = Quaternion.AngleAxis(angularChange, rotateAxis);
                transform.rotation *= amtToRotate;

                currentValvePosition += positionRate * Time.deltaTime;

                if (savedL != null)
                {
                    savedL(currentValvePosition);
                }

                if (wayThere >= TurnTime)
                {
                    isTurning = false;
                    wayThere = 0.0f;
                    currentAngle = targetAngle;
                }
            }
        }
        else
        {

        }
	}

    public void StartLerp(float endPoint, Lerping l)
    {
        
        if (isLever)
        {
            targetOrientation = endPoint == 0 ? closedOrientation : openOrientation;
            isTurning = true;
            wayThere = 0.0f;
            savedL = null;
            if(targetOrientation != transform.rotation)
            {
                angularRate = 1;
            }
            else
            {
                angularRate = 0;
            }
        }
        else
        {
            isTurning = true;
            targetAngle = (endPoint * (5 / 100f)) * 360f;
            angularRate = (targetAngle - currentAngle) / TurnTime; // 3 seconds per move
            positionRate = (endPoint - currentValvePosition) / TurnTime; // 3 seconds per move
            wayThere = 0.0f;
            savedL = l;
        }
    }
}
