﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WorldMasterController : MonoBehaviour {

    public string GISToken;
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void setToken(InputField input)
    {
        GISToken = input.text;
    }

    public void loadNextScene()
    {
        SceneManager.LoadScene("PlayScene");
    }
}
