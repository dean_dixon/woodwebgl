﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using System;

public class VPButton : MonoBehaviour {

    /*
    public bool isDigital;
    public float setToValue;
    public float addToValue;

    // flowrate tag
    public string equipmentName;


    private VPDatabase vpd;

    string tagName;
    [HideInInspector]
    public string value;

    private void Awake()
    {
        StreamReader reader;

        string defaultPath = "Assets/Resources/VPValves.txt";
        string path = "C:/ProgramData/CapeSoftware/VPLink3/VR/VPValves.txt";
        string configPath = "C:/ProgramData/CapeSoftware/VPLink3/VR/VR_Config.txt";
        string tagFileName = "";
        string line;

        bool exists = System.IO.File.Exists(configPath);
        if (exists)
        {
            reader = new StreamReader(configPath);
            while ((line = reader.ReadLine()) != null)
            {
                //line = line.Replace("\t", string.Empty);
                string[] fields = line.Split(',');
                //Debug.Log("Line=" + line + "; looking for " + equipmentName);

                char[] chars = fields[0].ToCharArray();
                if (!chars[0].Equals('#'))
                {
                    tagFileName = fields[0];
                }
            }

            path = "C:/ProgramData/CapeSoftware/VPLink3/VR/" + tagFileName;
        }

        exists = System.IO.File.Exists(path);

        if (exists)
        {
            reader = new StreamReader(path);
        }
        else
        {
            reader = new StreamReader(defaultPath);
        }
        while ((line = reader.ReadLine()) != null)
        {
            //line = line.Replace("\t", string.Empty);
            string[] fields = line.Split(',');
            //Debug.Log("Line=" + line + "; looking for " + equipmentName);

            char[] chars = fields[0].ToCharArray();
            if ((!chars[0].Equals('#')) && (fields[0].Length > 0))
            {
                if (String.Equals(fields[0], equipmentName, StringComparison.CurrentCultureIgnoreCase))
                {
                    //Debug.Log("Fields[0]=" + fields[0]);
                    tagName = fields[1];
                    break;
                }
                //objScript.isValve = true;
            }
        }

        reader.Close();
    }
    // Use this for initialization
    void Start()
    {
        vpd = FindObjectOfType<VPDatabase>();
        if (vpd == null)
        {
            Debug.LogError("Cannot find VP Link database in the VerMovement object");
        }
        else
        {
            if (tagName.Length > 0)
            {
                value = vpd.PVEStr(tagName, "VPLinkTag ???");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (vpd != null)
        {
            if (tagName.Length > 0)
            {
                value = vpd.PVEStr(tagName, value);
            }
        }
    }

    void setVPValue(float newValue)
    {
        if (vpd != null)
        {
            if (tagName.Length > 0)
            {
                if (isDigital)
                {
                    vpd.setPVE(tagName, newValue);
                }
                else
                {
                    vpd.setPV(tagName, vpd.PV(tagName, 5) + newValue);
                }
            }
        }
    }

    public void pressButton()
    {
        setVPValue(setToValue);
        if (isDigital)
        {
            setVPValue(setToValue);
        }
        else
        {
            setVPValue(addToValue);
        }
    }

    public void reloadConfig()
    {
        StreamReader reader;

        string defaultPath = "Assets/Resources/VPValves.txt";
        string path = "C:/ProgramData/CapeSoftware/VPLink3/VR/VPValves.txt";
        string configPath = "C:/ProgramData/CapeSoftware/VPLink3/VR/VR_Config.txt";
        string tagFileName = "";
        string line;

        bool exists = System.IO.File.Exists(configPath);
        if (exists)
        {
            reader = new StreamReader(configPath);
            while ((line = reader.ReadLine()) != null)
            {
                //line = line.Replace("\t", string.Empty);
                string[] fields = line.Split(',');
                //Debug.Log("Line=" + line + "; looking for " + equipmentName);

                char[] chars = fields[0].ToCharArray();
                if (!chars[0].Equals('#'))
                {
                    tagFileName = fields[0];
                }
            }

            path = "C:/ProgramData/CapeSoftware/VPLink3/VR/" + tagFileName;
        }

        exists = System.IO.File.Exists(path);

        if (exists)
        {
            reader = new StreamReader(path);
        }
        else
        {
            reader = new StreamReader(defaultPath);
        }
        while ((line = reader.ReadLine()) != null)
        {
            //line = line.Replace("\t", string.Empty);
            string[] fields = line.Split(',');
            //Debug.Log("Line=" + line + "; looking for " + equipmentName);

            char[] chars = fields[0].ToCharArray();
            if ((!chars[0].Equals('#')) && (fields[0].Length > 0))
            {
                if (String.Equals(fields[0], equipmentName, StringComparison.CurrentCultureIgnoreCase))
                {
                    //Debug.Log("Fields[0]=" + fields[0]);
                    tagName = fields[1];
                    break;
                }
                //objScript.isValve = true;
            }
        }

        reader.Close();
    }
    */
}
