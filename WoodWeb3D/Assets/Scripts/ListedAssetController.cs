﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListedAssetController : MonoBehaviour {

    [HideInInspector]
    public PlayerController myPlayerScript;
    [HideInInspector]
    public MasterController myMasterScript;
    [HideInInspector]
    public GameObject myAsset;


	void Start () {
        
    }

	void Update () {

	}
    

    public void GoToMyAsset()
    {
        myPlayerScript.transform.position = myAsset.transform.position - (myPlayerScript.myCam.transform.forward.normalized) * 5;
    }
}
