﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawingController : MonoBehaviour {

    [HideInInspector]
    public bool isFadingIn, isFadingOut;

    private SpriteRenderer myRenderer;

    private void Awake()
    {
        myRenderer = transform.GetComponent<SpriteRenderer>();
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if(isFadingIn)
        {
            myRenderer.color = new Color(myRenderer.color.r, myRenderer.color.g, myRenderer.color.b, myRenderer.color.a + .05f);

            if(myRenderer.color.a >= 1.0f)
            {
                myRenderer.color = new Color(myRenderer.color.r, myRenderer.color.g, myRenderer.color.b, 1);
                isFadingIn = false;
                Debug.Log("done fading in");
            }
        }

        if (isFadingOut)
        {
            myRenderer.color = new Color(myRenderer.color.r, myRenderer.color.g, myRenderer.color.b, myRenderer.color.a - .05f);

            if (myRenderer.color.a <= 0.0f)
            {
                myRenderer.color = new Color(myRenderer.color.r, myRenderer.color.g, myRenderer.color.b, 0);
                isFadingOut = false;
                Debug.Log("done fading out");
            }
        }
    }
}
